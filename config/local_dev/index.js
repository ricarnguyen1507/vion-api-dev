const path = require('path')

const addr = process.env.ADDR
const port = process.env.PORT

const domain = "localhost"

const tmp_dir = path.join(rootDir, 'upload')

const public_dir = path.join(rootDir, 'public')

const image_dir = public_dir
const video_dir = public_dir

const import_dir = path.join(public_dir, 'import')
const export_dir = path.join(public_dir, 'export')

const image_url = `http://localhost:${port}/image`
const video_url = `http://localhost:${port}/video`

module.exports = {
  domain,
  fastify_options: {
    logger: false,
    http2: false
  },
  static_routes: [
    {
      prefix: '/cdn',
      root: public_dir
    },
    {
      prefix: '/login',
      root: path.join(rootDir, 'public', 'login'),
      decorateReply: false // cái thứ 2 trở lên bắt buộc phải có
    },
    {
      prefix: '/import',
      root: import_dir,
      decorateReply: false
    },
    {
      prefix: '/export',
      root: export_dir,
      decorateReply: false
    }
  ],
  proxy_routes: [
    {
      prefix: '/',
      upstream: 'http://host.docker.internal:3000',
      websocket: true,
      http2: false
    }
  ],
  apiPrefix: '/api',
  // alpha_grpc: ["192.168.1.3:9080"],
  alpha_grpc: ["alpha:9080"],
  // alpha_grpc: require('./alpha'),
  tracking: {
    url: `https://tracking.${domain}`,
    key: "eB1BG5pI0hRe0t5S"
  },
  tmp_dir,
  image_dir,
  image_url,
  video_dir,
  video_url,
  import_dir,
  export_dir,
  addr,
  port
}