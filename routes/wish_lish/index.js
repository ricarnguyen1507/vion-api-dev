"use strict"
const db = require("#/services/tvcdb")

function mutate(request) {
  return db.mutate(db.addTypeSet("WishType", request.body)).then(result => db.getUids(result))
}

function getAll() {
  return db.query(`{ result(func: type(WishType), orderasc: wish_name){uid,expand(_all_)} }`)
}

module.exports = [
  ['get', '/wish-type/lish', getAll],
  ['post', '/wish-type', mutate]
]