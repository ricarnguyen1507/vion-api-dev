"use strict"

function splitChildVouchers(schemaVoucher) {
  const mutateData = []
  if (schemaVoucher.uid && schemaVoucher?.list_voucher_code?.length > 0) {
    const codes = schemaVoucher.list_voucher_code.split(',')

    const child_voucher = schemaVoucher
    delete child_voucher.list_voucher_code //vì có field này nó sẽ thành schema

    while(codes.length > 0) {
      let cuting = []
      if (codes.length > 1000) {
        cuting = codes.splice(0, 1000)
      } else {
        cuting = codes
      }


    }
    codes.forEach(c => {
      child_voucher.voucher_code = c
    })
    mutateData.push(child_voucher)
    return mutateData
  }
}

module.exports = { splitChildVouchers }