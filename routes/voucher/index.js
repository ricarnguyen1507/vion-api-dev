"use strict"

const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

const tableFields = `
  uid
  is_deleted
  collection_name
  collection_image
  collection_icon
  background_image
  collection_type
  display_order
  sort_order
  layout_type
  parent  @filter(not eq(is_deleted, true) AND not eq(display_status, 1)){
    uid
    expand(_all_)
  }
  children: ~parent @facets(orderasc: display_order) @filter(not eq(is_deleted, true) AND not eq(display_status, 1)) {
    uid
    collection_name
  }
  products: ~product.collection @facets(orderasc: display_order)  @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
    uid
    product_name
  }
  highlight.products @facets(orderasc: display_order) {
    uid
    expand(_all_)
  }
  highlight.negative_products @facets(orderasc: display_order) {
    uid
    expand(_all_)
  }
  highlight.collections @facets(orderasc: display_order) {
    uid
    expand(_all_)
  }
  voucher.customers (orderasc: display_order) {
    uid
    expand(_all_)
  }
  ~highlight.collections {
    uid
  }
  voucher.conditions @facets(orderasc: display_order) {
    uid
    section_value
    section_ref
    section_name
  }
  operator
  type_highlight
  color_title
  color_price
  color_discount
  highlight_name
  image_highlight
  display_status
  reference_type
  is_temporary
  target_type
  condition_type
  condition_value
  voucher_type
  voucher_code
  voucher_value
  max_applied_value
  voucher_label
  start_at
  stop_at
  redeem
  limit_redeem
  is_internal
  auto_apply
`

async function setDelete ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const { result: count_products } = await db.query(`{
    result(func: uid(${uid})) {
      parent {
        uid
      }
      ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
        ppc as uid
      }
      childrent: ~parent {
        ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
          cpc as uid
        }
      }
    }

    count_products(func: uid(cpc, ppc)) {
      uid
    }
  }`)

  if (count_products.length > 0) { //is parent
    return { statusCode: 400, message: "Không được xoá ngành hàng có sản phẩm đang active" }
  }

  await db.upsert({
    query: `{
      var(func: uid(${uid})) {
        parent as uid
        highlight.products {
          php as uid
        }
        ~product.collection {
          ppc as uid
        }
        ~parent {
          children as uid
          highlight.products {
            chp as uid
          }
          ~product.collection {
            cpc as uid
          }
        }
      }
    }`,
    cond: "@if( eq(len(ppc), 0) AND eq(len(cpc), 0) )",
    set: [{
      uid: "uid(parent)",
      is_deleted: true
    }, {
      uid: "uid(children)",
      is_deleted: true
    }],
    del: [
      {
        uid: "uid(parent)",
        'highlight.products': 'uid(php)'
      }, {
        uid: "uid(children)",
        'highlight.products': 'uid(chp)'
      },
      {
        uid: "uid(ppc)",
        "product.collection": "uid(parent)"
      },
      {
        uid: "uid(cpc)",
        "product.collection": "uid(children)"
      },
    ]
  })
  return { statusCode: 200 }
}

async function getAll ({ query }) {
  const sort_filed = `, orderasc: display_order, first: ${query.number}, offset: ${query.page * query.number}`
  const { t, is_temp, reference_type, is_parent } = query
  let { filter = '' } = query
  filter += reference_type ? `AND NOT eq(reference_type, ${reference_type})` : ''
  filter += is_temp && is_temp === "true" ? ` AND eq(is_temporary, true)` : is_temp && is_temp === "false" ? ` AND NOT eq(is_temporary, true)` : ''
  filter += t ? ` AND eq(collection_type, ${t})` : ''
  filter += (typeof is_parent === "undefined") ? '' : is_parent === "true" ? `AND NOT has(parent)` : 'AND NOT has(~parent)'
  const queryStr = `{
    vouchers as summary(func: type(Collection)) @filter(NOT eq(is_deleted, true)${filter}){
      totalCount: count(uid)
    }
    result(func: uid(vouchers) ${sort_filed})  {
      ${tableFields}
    }
  }`
  if (t && t == 300) {
    log.debug(queryStr)
  }

  const { result, summary } = await db.query(queryStr)

  return {
    result: db.parseFacetArray(result),
    summary
  }
}

async function handleForm (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'collection_image')
    }
  }
  // const { del, set, childSort } = fields

  // if(del) {
  //   const delData = JSON.parse(del)
  //   await db.mutate({ del: delData })
  // }
  // if (childSort) {
  //   const sortData = JSON.parse(childSort)
  //   await db.mutate({set: [...sortData]})
  // }
  // if(set) {
  //   const setData = JSON.parse(set)
  //   const uid = await db.mutate({
  //     set: {
  //       uid: "_:new_collection",
  //       "dgraph.type": "Collection",
  //       "collection_type": set.collection_type || 0,
  //       ...setData
  //     }
  //   }).then(res => setData.uid || res.getUidsMap().get("new_collection"))
  //   if(Object.keys(files).length > 0) {
  //     const fields_files = saveMediaFiles(files, imageFolder)
  //     db.mutate({
  //       set: {
  //         uid,
  //         ...fields_files
  //       }
  //     })
  //   }

  //   /**
  //    * Create Schema children
  //    * */
  //   if (!setData?.uid && setData?.list_voucher_code?.length > 0) {
  //     let codes = setData.list_voucher_code.split(',')
  //     let child_voucher = setData
  //     let count = 1

  //     child_voucher.uid = `_:new_voucher_${(new Date()).getTime() + count++}`
  //     child_voucher.voucher_schema = {uid}
  //     child_voucher.display_status = setData.display_status || 0
  //     child_voucher.collection_type = set.collection_type || 300
  //     child_voucher['dgraph.type'] = 'Collection'
  //     delete child_voucher.list_voucher_code //vì có field này nó sẽ thành schema

  //     while(codes.length > 0) {
  //       let childSessionMutate = []
  //       let cuting = []

  //       if (codes.length > 1) {
  //         cuting = codes.splice(0, 1)
  //       } else {
  //         cuting = codes
  //         codes = []
  //       }

  //       cuting.forEach(c => {
  //         child_voucher.voucher_code = c
  //         childSessionMutate.push(child_voucher) //mỗi lần mutate 1000 records
  //       })

  //       await db.mutate({
  //         set: childSessionMutate
  //       })
  //     }
  //   }

  //   return {
  //     statusCode: 200,
  //     new_collection: await db.query(`{
  //       result(func:uid(${uid})) {
  //         ${tableFields}
  //       }
  //     }`)
  //   }
  // }
  return { statusCode: 200 }
}

function getProducts ({ params }) {
  const { uid } = params
  if (!params || !uid) {
    throw { statusCode: 400, message: "Hello guys =)))" }
  }

  return db.query(`{
    var(func:uid(${uid})) @filter(type(Collection)) {
      products as ~product.collection @filter(not eq(is_deleted, true)) {
        uid
      }
    }
    result(func:uid(products)) @filter(type(Product)) {
      uid
      product_name
    }
  }`)
}

function checkVoucherExist ({ body }) {
  const { voucher_code, start_at } = body
  if (!body) {
    throw { statusCode: 400, message: "Hello guys =)))" }
  }

  return db.query(voucher_code !== '' ? `{
    result(func:type(Collection)) @filter(type(Collection) AND eq(voucher_code, ${voucher_code}) AND eq(display_status, 2) AND ge(stop_at, ${start_at}) AND NOT eq(is_deleted, true)) {
      uid
      collection_name
    }
  }` :
    `{
    result(func:type(Collection)) @filter(type(Collection) AND eq(display_status, 2) AND ge(stop_at, ${start_at}) AND NOT eq(is_deleted, true)) {
      uid
      collection_name
    }
  }`)
}

function reOrdering ({ body }) {
  return db.mutates(body)
}

module.exports = [
  ['get', '/list/voucher', getAll],
  ['delete', '/voucher/:uid', setDelete],
  ['post', '/voucher', handleForm],
  ['post', '/voucher/reordering', reOrdering],
  ['get', '/voucher/list/product/:uid', getProducts],
  ['post', '/voucher/check-voucher', checkVoucherExist]
]
