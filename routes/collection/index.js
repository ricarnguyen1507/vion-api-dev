"use strict"

const db = require("#/services/tvcdb")
const { saveMediaFiles, saveMediaMD5 } = require("#/modules/uploadFileHandle")

const imageFolder = "collection_images"
const optionFields = `
uid
collection_name
collection_type
`
const tableFields = `
  uid
  is_deleted
  collection_name
  collection_image
  collection_icon
  background_image
  collection_type
  display_order
  display_name
  sort_order
  layout_type
  parent @filter(not eq(is_deleted, true) AND not eq(display_status, 1)){
    uid
    collection_name
  }
  children: ~parent @facets(orderasc: display_order) @filter(not eq(is_deleted, true) AND not eq(display_status, 1)) {
    uid
    collection_name
  }
  products: ~product.collection @facets(orderasc: display_order)  @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
    uid
    product_name
  }
  highlight.products @facets(orderasc: display_order) {
    uid
    product_name
  }
  highlight.collections @facets(orderasc: display_order) {
    uid
    collection_name
  }
  voucher.customers (orderasc: display_order) {
    uid
    expand(_all_)
  }
  ~highlight.collections {
    uid
    collection_name
  }
  type_highlight
  color_title
  color_price
  color_discount
  highlight_name
  image_highlight
  display_status
  reference_type
  is_temporary
  target_type
  condition_type
  condition_value
  voucher_type
  voucher_code
  voucher_value
  max_applied_value
  voucher_label
  start_at
  stop_at
  redeem
  is_internal
  brand_shop_id
  config_type
  collection.and_cond @facets(orderasc: display_order){
    uid
    section_value
    section_name
    section_ref
    section_ref_value
    section_type
  }
  collection.or_cond @facets(orderasc: display_order){
    uid
    section_value
    section_name
    section_ref
    section_ref_value
    section_type
  }
  collection.neg_prod_cond @facets(orderasc: display_order){
    uid
    product_name
  }
`

async function setDelete ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const { count_products } = await db.query(`{
    result(func: uid(${uid})) {
      parent {
        uid
      }
      ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
        ppc as uid
      }
      childrent: ~parent {
        ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
          cpc as uid
        }
      }
    }

    count_products(func: uid(cpc, ppc)) {
      uid
    }
  }`)

  if (count_products.length > 0) { //is parent
    return { statusCode: 400, message: "Không được xoá ngành hàng có sản phẩm đang active" }
  }

  await db.upsert({
    query: `{
      var(func: uid(${uid})) {
        parent as uid
        highlight.products {
          php as uid
        }
        ~product.collection {
          ppc as uid
        }
        ~parent {
          children as uid
          highlight.products {
            chp as uid
          }
          ~product.collection {
            cpc as uid
          }
        }
      }
    }`,
    cond: "@if( eq(len(ppc), 0) AND eq(len(cpc), 0) )",
    set: [{
      uid: "uid(parent)",
      is_deleted: true
    }, {
      uid: "uid(children)",
      is_deleted: true
    }],
    del: [
      {
        uid: "uid(parent)",
        'highlight.products': 'uid(php)'
      }, {
        uid: "uid(children)",
        'highlight.products': 'uid(chp)'
      },
      {
        uid: "uid(ppc)",
        "product.collection": "uid(parent)"
      },
      {
        uid: "uid(cpc)",
        "product.collection": "uid(children)"
      },
    ]
  })
  return { statusCode: 200 }
}

async function getAll ({ query }) {
  let sort_filed = ', orderasc: display_order'
  if(query?.number != undefined && query?.page != undefined) {
    sort_filed += `, first: ${query.number}, offset: ${query.page * query.number}`
  }
  const { t, is_temp, reference_type, is_parent, option_fields = false, brand_shop_uid = false } = query
  let { filter = '' } = query
  if (t !== undefined && t == 300) {
    sort_filed = ''
  }
  filter += reference_type ? `AND NOT eq(reference_type, ${reference_type})` : ''
  filter += is_temp && is_temp === "true" ? ` AND eq(is_temporary, true)` : is_temp && is_temp === "false" ? ` AND NOT eq(is_temporary, true)` : ''
  filter += t !== undefined ? ` AND eq(collection_type, ${t})` : ''
  filter += (typeof is_parent === "undefined") ? '' : is_parent === "true" ? ` AND (NOT has(parent) AND eq(is_parent, true))` : ' AND (NOT has(~parent) AND NOT eq(is_parent, true))'
  let queryStr = `{
    collections as summary(func: type(Collection)) @filter(NOT eq(is_deleted, true) AND NOT has(~brand_shop.collection)${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(collections) ${sort_filed}){
      ${option_fields ? optionFields : tableFields}
    }
  }`

  /**Đoạn này để filter cho chọn brandShop trong hybrid (trước mắt là vậy) */
  if (brand_shop_uid && db.isUid(brand_shop_uid)) {
    queryStr = `{
      var(func: uid(${brand_shop_uid})) @filter(NOT eq(is_deleted, true) AND eq(display_status, 2)) {
        brand_shop.collection @filter(NOT eq(is_deleted, true) AND has(~brand_shop.collection)${filter}) {
          c as uid
        }
      }
      result(func: uid(c) ${sort_filed}) {
        ${option_fields ? optionFields : tableFields}
      }
    }`
  }
  if (t == 0) {
    log.debug(queryStr)
  }
  const { result, summary } = await db.query(queryStr)
  return {
    result: db.parseFacetArray(result),
    summary
  }
}

// async function handleForm (request) {
//   if(!request.isMultipart()) {
//     throw new Error('multipart request only')
//   }
//   const { fields, files } = request.body

//   const { del, set, childSort } = fields

//   if(del) {
//     const delData = JSON.parse(del)
//     await db.mutate({ del: delData })
//   }
//   if (childSort) {
//     const sortData = JSON.parse(childSort)
//     await db.mutate({set: [...sortData]})
//   }
//   if(set) {
//     const setData = JSON.parse(set)
//     const uid = await db.mutate({
//       set: {
//         uid: "_:new_collection",
//         "dgraph.type": "Collection",
//         "collection_type": set.collection_type || 0,
//         ...setData
//       }
//     }).then(res => setData.uid || res.getUidsMap().get("new_collection"))
//     if(Object.keys(files).length > 0) {
//       const fields_files = saveMediaFiles(files, imageFolder)
//       db.mutate({
//         set: {
//           uid,
//           ...fields_files
//         }
//       })
//     }

//     return {
//       statusCode: 200,
//       new_collection: await db.query(`{
//         result(func:uid(${uid})) {
//           ${tableFields}
//         }
//       }`)
//     }
//   }
//   return { statusCode: 200 }
// }
async function handleNewForm (request) {
  const { fields, files } = request.body
  const uid = request.params.uid
  if(fields?.childCollection && !uid.startsWith('_:')) {
    const { set, del } = JSON.parse(fields.childCollection)
    if (set?.length == del?.length && (set?.length > 0 || del?.length > 0)) {
      for(const obj of set) {
        fields.set += `<${obj.uid}> <parent> <${uid}> (display_order=${obj.display_order}) .\r\n`
      }
    } else if(set?.length < del?.length) {
      for(let i = 0; i < set?.length; i++) {
        fields.set += `<${set[i].uid}> <parent> <${uid}> (display_order=${i}) .\r\n`
      }
      const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
      for(const obj of removeItem) {
        fields.del += `<${obj.uid}> <parent> <${uid}> .\r\n`
      }
    }
    delete fields.childCollection
  }
  if(fields?.childProduct && !uid.startsWith('_:')) {
    const { set, del } = JSON.parse(fields.childProduct)
    if (set?.length == del?.length && (set?.length > 0 || del?.length > 0)) {
      for(const obj of set) {
        fields.set += `<${obj.uid}> <product.collection> <${uid}> (display_order=${obj.display_order}) .\r\n`
      }
    } else if(set?.length < del?.length) {
      for(let i = 0; i < set?.length; i++) {
        fields.set += `<${set[i].uid}> <product.collection> <${uid}> (display_order=${i}) .\r\n`
      }
      const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
      for(const obj of removeItem) {
        fields.del += `<${obj.uid}> <product.collection> <${uid}> .\r\n`
      }
    }
    delete fields.childProduct
  }
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'collection')
    }
  }
  return { statusCode: 200 }
}
async function handleForm (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields, files } = request.body

  const { del, set, childSort } = fields

  if(del) {
    const delData = JSON.parse(del)
    await db.mutate({ del: delData })
  }
  if (childSort) {
    const sortData = JSON.parse(childSort)
    await db.mutate({ set: [...sortData] })
  }
  if(set) {
    const setData = JSON.parse(set)
    const uid = await db.mutate({
      set: {
        uid: "_:new_collection",
        "dgraph.type": "Collection",
        "collection_type": set.collection_type || 0,
        ...setData
      }
    }).then(res => setData.uid || res.getUidsMap().get("new_collection"))
    if(Object.keys(files).length > 0) {
      const fields_files = saveMediaFiles(files, imageFolder)
      db.mutate({
        set: {
          uid,
          ...fields_files
        }
      })
    }

    return {
      statusCode: 200,
      new_collection: await db.query(`{
        result(func:uid(${uid})) {
          ${tableFields}
        }
      }`)
    }
  }
  return { statusCode: 200 }
}
function getProducts ({ params }) {
  const { uid } = params
  if (!params || !uid) {
    throw { statusCode: 400, message: "Hello guys =)))" }
  }

  return db.query(`{
    var(func:uid(${uid})) @filter(type(Collection)) {
      products as ~product.collection @filter(not eq(is_deleted, true)) {
        uid
      }
    }
    result(func:uid(products)) @filter(type(Product)) {
      uid
      product_name
    }
  }`)
}

function checkVoucherExist ({ body }) {
  const { voucher_code, start_at } = body
  if (!body) {
    throw { statusCode: 400, message: "Hello guys =)))" }
  }

  return db.query(voucher_code !== '' ? `{
    result(func:type(Collection)) @filter(type(Collection) AND eq(voucher_code, ${voucher_code}) AND eq(display_status, 2) AND ge(stop_at, ${start_at}) AND NOT eq(is_deleted, true)) {
      uid
      collection_name
    }
  }` :
    `{
    result(func:type(Collection)) @filter(type(Collection) AND eq(display_status, 2) AND ge(stop_at, ${start_at}) AND NOT eq(is_deleted, true)) {
      uid
      collection_name
    }
  }`)
}

function reOrdering ({ body }) {
  return db.mutates(body)
}

async function searchCollectionChild ({ body }) {
  const { fulltext_search } = body
  return await db.query(`{
    result(func: alloftext(collection_name, "${fulltext_search}") , first: 50, offset: 0) @filter(type(Collection) AND eq(collection_type, 0) AND has(parent) AND NOT eq(is_parent, true) AND NOT has(~brand_shop.collection) AND NOT eq(is_deleted, true)) {
      uid
      collection_name
    }
  }`)
}

module.exports = [
  ['get', '/list/collection', getAll],
  ['delete', '/collection/:uid', setDelete],
  ['post', '/collectionNewForm/:uid', handleNewForm],
  ['post', '/collection', handleForm],
  ['post', '/collection/reordering', reOrdering],
  ['get', '/collection/list/product/:uid', getProducts],
  ['post', '/collection/check-voucher', checkVoucherExist],
  ['post', '/collection_child/search', searchCollectionChild]
]
