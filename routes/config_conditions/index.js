const db = require("#/services/tvcdb")
const _ = require('lodash')

function getQuery ({ section_value, section_ref, section_ref_value }, product_uids) {
  if (!section_value || !section_ref) {
    return ''
  }
  if ((section_ref && section_ref !== "") && section_value === 'belong_brandshop') {
    return `
      var(func: uid(${section_ref})) @filter(type(BrandShop) AND eq(display_status, 2)) {
        brand_shop.product @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
          brand_p as uid
        }
      }
      belong_brandshop(func: uid(brand_p)) @filter(type(Product)) {
        uid
      }
    `
  } else if ((section_ref && section_ref !== "") && section_value === 'belong_nh1') {
    return `
    var(func: uid(${section_ref})) @filter(type(Collection) AND eq(collection_type,0) AND NOT eq(is_temporary, true) AND has(~parent)) {
      child: ~parent {
        ~product.collection @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
          nh1_p as uid
        }
      }
    }
    belong_nh1(func: uid(nh1_p)) @filter(type(Product)) {
      uid
    }
    `
  } else if ((section_ref && section_ref !== "") && section_value === 'belong_nh2') {
    return `
    var(func: uid(${section_ref})) @filter(type(Collection) AND eq(collection_type,0) AND NOT eq(is_temporary, true) AND NOT has(~parent)) {
      ~product.collection @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
        nh2_p as uid
      }
    }
    belong_nh2(func: uid(nh2_p)) @filter(type(Product)) {
      uid
    }
    `
  } else if ((section_ref && section_ref !== "") && section_value === 'belong_supplier') {
    return `
    var(func: uid(${section_ref})) @filter(type(Partner)) {
      ~product.partner @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
        sup_p as uid
      }
    }
    belong_supplier(func: uid(sup_p)) @filter(type(Product)) {
      uid
    }
    `
  } else if ((section_ref && section_ref !== "") && section_value === 'belong_brand') {
    return `
    var(func: uid(${section_ref})) @filter(type(Brand)) {
      ~product.brand @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
        bra_p as uid
      }
    }
    belong_brand(func: uid(bra_p)) @filter(type(Product)) {
      uid
    }
    `
  } else if ((section_ref && section_ref !== "") && (section_ref_value && section_ref_value !== "") && section_value === 'has_price_with_vat') {
    let compare_func = "eq"
    let filter_operat = ""
    if (section_ref === "0xLE(value)") {
      compare_func = "le"
    } else if (section_ref === "0xGE(value)") {
      compare_func = "ge"
    } else if (section_ref === "0xEQ(value)") {
      filter_operat = "eq"
    }
    if (product_uids && product_uids.length > 0) {
      return `
      var(func: uid(${product_uids.join(',')})) @filter(type(Product)) {
        product.pricing (first: -1) @filter(${String(compare_func)}(price_with_vat, ${String(section_ref_value)})) {
          ~product.pricing @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
            price_p as uid
          }
        }
      }
      has_price_with_vat(func: uid(price_p)) {
        uid
      }
      `
    } else {
      return `
      var(func: type(Product)) @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
        product.pricing (first: -1) @filter(${String(compare_func)}(price_with_vat, ${String(section_ref_value)})) {
          ~product.pricing @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
            price_p as uid
          }
        }
      }
      has_price_with_vat(func: uid(price_p)) {
        uid
      }
      `
    }

  } else if ((section_ref && section_ref !== "") && (section_ref_value && section_ref_value !== "") && section_value === 'has_product_tags') {
    let compare_func = "shipping"
    if (section_ref === "0xNameTag") {
      compare_func = "name_tag"
    } else if (section_ref === "0xAssess") {
      compare_func = "assess"
    } else if (section_ref === "0xPromotion") {
      compare_func = "promotion"
    } else if (section_ref === "0xShipping") {
      compare_func = "shipping"
    }

    if (product_uids && product_uids.length > 0) {
      return `
      var(func: type(Tag)) @filter(eq(tag_category, ${String(compare_func)})) {
        ~product.tag @filter(uid(${product_uids.join(',')}) AND NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
          tag_p as uid
        }
      }
      has_product_tags(func: uid(tag_p)) {
        uid
      }
      `
    } else {
      return `
      var(func: type(Product)) @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
        product.tag @filter(eq(tag_category, ${String(compare_func)})) {
          ~product.tag @filter(NOT eq(is_deleted,true) AND eq(display_status, 2) AND NOT eq(is_temporary, true)) {
            tag_p as uid
          }
        }
      }
      has_product_tags(func: uid(tag_p)) {
        uid
      }
      `
    }

  } else {
    return ``
  }

}

/**
 *
 * @param { and_cond, or_cond, neg_prod_cond } param
 * @author C.A
 * Chức năng chính, là query kết quả các block and
 * kiểm tra đối chiếu với block or nếu có thì lấy tập hợp của 2 kết quả có cùng điều kiện
 *
 * Cuối cùng là tìm giao hợp của tất cả các block đã được đối chiếu, nếu có sản phẩm loại trừ thì filter bỏ ra.
 *
 * Return lại list uids.join(',')
 * để client tự parse về định dạng có thể lưu được từ phía client mới bắn lên api theo flow cũ từ trước đến giờ cập nhật lại.
 */
async function getConfigCondProducts ({ body }) {
  const { and_cond, or_cond, neg_prod_cond } = body
  if (!and_cond || !or_cond) {
    throw Error("f**k, ")
  }

  /**Start Block AND*/
  const normal_and_result = {}
  if (and_cond.length) {
    let and_query = ''

    /** Chạy từng câu query 1, rồi gắn kết quả vào, để tránh quá tải */
    for(const ac of and_cond) {
      and_query = getQuery(ac)
      if (and_query !== "") {
        const result = await db.query(`{${and_query}}`)
        if (result?.[ac.section_value] && result?.[ac.section_value].length > 0) {
          /**
           * Giải quyết trường hợp chung 1 block có nhiều hơn 1 lần xuất hiện điều kiện này.
           * Với block Bắt buộc thoả tất cả điều kiện thì lấy tập giao (intersection) của các kết quả
           */
          if (!normal_and_result[ac.section_value]) {
            normal_and_result[ac.section_value] = result[ac.section_value]
          } else {
            normal_and_result[ac.section_value] = getIntersection({
              old_result: normal_and_result[ac.section_value],
              new_result: result[ac.section_value]
            }, 'intersection')?.map(r => ({ uid: r })) || []
          }

        }
      }
    }
  }

  /**Start Block OR*/
  const normal_or_result = {}
  if (or_cond.length) {
    let or_query = ''
    /** Chạy từng câu query 1, rồi gắn kết quả vào, để tránh quá tải */
    for(const ac of or_cond) {
      or_query = getQuery(ac)
      if (or_query !== "") {
        const result = await db.query(`{${or_query}}`)
        if (result?.[ac.section_value] && result?.[ac.section_value].length > 0) {
          /**
           * Giải quyết trường hợp chung 1 block có nhiều hơn 1 lần xuất hiện điều kiện này.
           * Với block Bắt buộc thoả tất cả điều kiện thì lấy tập hợp (union) của các kết quả
           */
          if (!normal_or_result[ac.section_value]) {
            normal_or_result[ac.section_value] = result[ac.section_value]
          } else {
            normal_or_result[ac.section_value] = getIntersection({
              old_result: normal_or_result[ac.section_value],
              new_result: result[ac.section_value]
            }, 'union')?.map(r => ({ uid: r })) || []
          }
        }
      }
    }
    /** Lấy ra danh sách các uid của sản phẩm là một tập hợp của tất cả các array kết quả. [0x13,0x14,0x13fe34] */
  }
  /**
  * End Block OR */

  /** Bắt đầu kết hợp các block điều kiện */
  const summaryConds = {}
  let gatherThan = {}, lessThan = {}
  if (Object.keys(normal_and_result).length > Object.keys(normal_or_result).length) {
    gatherThan = normal_and_result
    lessThan = normal_or_result
  } else {
    gatherThan = normal_or_result
    lessThan = normal_and_result
  }
  for(const nar in gatherThan) {
    if (gatherThan[nar] && lessThan[nar]) {
      /**
       * Trường hợp cả 2 loại điều kiện cùng tồn tại, thì lấy tập hợp (union) của cả 2 lại.
       */
      summaryConds[nar] = getIntersection({
        nar: gatherThan[nar],
        nor: lessThan[nar],
      }, 'union')?.map(r => ({ uid: r })) || []
    } else if (gatherThan[nar] && !lessThan[nar]) {
      summaryConds[nar] = gatherThan[nar]
    } else if (!gatherThan[nar] && lessThan[nar]) {
      summaryConds[nar] = lessThan[nar]
    }
  }

  const finalResult = getIntersection(summaryConds, 'intersection')

  if (neg_prod_cond?.length > 0) {
    const filterNegProd = finalResult.filter(a => !neg_prod_cond.includes(a))
    return {
      uids: filterNegProd,
      total: filterNegProd.length
    }
  }
  return {
    uids: finalResult,
    total: finalResult.length
  }
}

/**
 *
 * @param {Object} object_from_query
 * @return uids.join(',')
 */
function getIntersection (object_from_query, type = 'intersection') {
  const get_array = []
  for(const i in object_from_query) {
    get_array.push(object_from_query[i].map(ai => ai.uid))
  }
  if (type === 'intersection') {
    return _.intersection(...get_array)
  } else {
    return _.union(...get_array)
  }
}

module.exports = [
  ['post', '/config-condition/list/products', getConfigCondProducts]
]