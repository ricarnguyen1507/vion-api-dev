"use strict"
const db = require("#/services/tvcdb")

function mutate(request) {
  return db.mutate(db.addTypeSet("Distributor", request.body)).then(result => db.getUids(result))
}
function getAll() {
  return db.query(`{ result(func: type(Distributor)){uid,expand(_all_)} }`)
}
function loadMore(request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Distributor), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid(request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Distributor)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/distributor', getAll],
  ['get', '/list/distributor/:offset/:number', loadMore],
  ['get', '/distributor/:uid', getByUid],
  ['post', '/distributor', mutate]
]