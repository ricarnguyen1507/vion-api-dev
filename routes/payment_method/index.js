"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")
// const images_dir = path.join(image_dir, "images")

async function mutate (request) {
  const { fields, files } = request.body

  await db.mutate(fields)

  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'payment_methods')
    }
  }
  return { statusCode: 200 }
}
async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    paymentMethods as summary(func: type(PaymentMethod)) @filter(${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(paymentMethods)${paginate}){
      uid
      display_status
      payment_name
      payment_code
      description
      image_highlight
      image_background
    }
    }`)
  return { result, summary }
}
// function loadMore(request) {
//   const { offset, number } = request.params
//   return db.query(`query result($number: string, offset: string) {
//     result(func: type(PaymentMethod), first: $number, offset: $offset){uid,expand(_all_)}
//   }`, {
//     $number: number || 20,
//     $offset: offset || 0
//   })
// }
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(PaymentMethod)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/payment_method', getAll],
  // ['get', '/list/payment_method/:offset/:number', loadMore],
  ['get', '/payment_method/:uid', getByUid],
  ['post', '/payment_method', mutate]
]