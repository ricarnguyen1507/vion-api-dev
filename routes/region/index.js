"use strict"

const db = require("#/services/tvcdb")

async function setDelete ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  await db.mutate({
    del: { uid }
  })
  return { statusCode: 200 }
}
function parseDataWard (province) {
  const provinceParse = []
  if(province.length) {
    for(const obj of province) {
      provinceParse.push(obj)
      if(obj?.areas?.length) {
        for(const ward of obj.areas) {
          provinceParse.push(ward)
        }
      }
    }
  }
  return provinceParse
}
const region_column = `uid, id, geo_region, name, name_url, ghn_id, vn_all_province, location_support, is_province, region_code`
const region_fragments = `
${region_column}
areas {
  ${region_column}
  ~areas {
    ${region_column}
  }
}
~areas {
  ${region_column}
}
`
function getAllProvince () {
  return db.query(`{
    result(func: type(Region), orderasc: geo_region) @filter(eq(is_province,true)) {
      uid
      name
      is_province
      geo_region
    }
  }`)
}
async function getTable ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Region)", ...primaryFilters].join(', ') : "type(Region)"
  const str2 = filters.length ? filters : 'eq(vn_all_province,true) OR eq(is_province, true)'

  const { summary, result } = await db.query(`query result($number: string, $offset: string) {
    regions as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    result(func: uid(regions), first: $number, offset: $offset) {
      ${region_fragments}
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  const data = parseDataWard(result)
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
// function saveFile(orgFileName, prefix) {
//   if(!orgFileName) return ''
//   const fileName = `${prefix}${getFileExt(orgFileName)}`
//   const filePath = path.join(image_dir, imageFolder, fileName)
//   fs.move(orgFileName, filePath, { overwrite: true })
//   return `${imageFolder}/${fileName}`
// }
async function handleForm (request) {
  const uidProvince = request.params.uidProvince ?? ''
  let uid = request.params.uid ?? ''
  let provinceSet = {}
  const res = await db.mutate(request.body.fields)
  if(uidProvince != '' && uid.startsWith('_:')) {
    uid = res.getUidsMap().get("new_region")
    provinceSet = {
      uid: uidProvince,
      areas: {
        uid: uid
      }
    }
  }
  await db.mutate({ set: provinceSet })
  return { statusCode: 200 }
}
// async function handleForm ({ body }) {
//   if (!body) {
//     throw Error("Invalid UID")
//   }

//   let provinceSet = {}
//   let provinceDel = {}

//   const dataSet = body?.set
//   const dataDel = body?.del
//   const setProvince = dataSet['~areas']
//   const delProvince = dataDel['~areas']
//   delete dataSet['~areas']
//   delete dataDel['~areas']

//   const uid = await db.mutate({
//     set: {
//       "uid": "_:new_region",
//       "dgraph.type": "Region",
//       ...dataSet
//     },
//   }).then(res => dataSet.uid || res.getUidsMap().get("new_region"))

//   if (!dataSet.vn_all_province && setProvince) { // khi chọn tỉnh cho quận huyện, thì set tỉnh đó có thêm quận huyện
//     provinceSet = {
//       uid: setProvince.uid,
//       areas: {
//         uid: uid
//       }
//     }
//   }
//   if (delProvince) { // khi bỏ chọn tỉnh cho quận huyện, thì del quận huyện ra khỏi tỉnh đó
//     provinceDel = {
//       uid: delProvince.uid,
//       areas: {
//         uid: uid
//       }
//     }
//   }

//   await db.mutates([
//     { del: dataDel },
//     { del: provinceDel },
//     { set: provinceSet },
//   ])

//   return await db.query(`{
//     result(func: type(Region), orderasc: geo_region) {
//       ${region_fragments}
//     }
//   }`)
// }
function getAreas () {
  return db.query(`{
    result(func:type(Region), orderdesc: geo_region) @filter(has(areas) OR eq(vn_all_province, true))  {
      uid
      expand(_all_) {
        uid
        expand(_all_)
      }
    }
  }`)
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Region)) {
      uid
      name
    }
  }`, { $uid })
}

module.exports = [
  ['post', '/list/region', getTable],
  ['get', '/list/region', getAllProvince],
  ['delete', '/region/:uid', setDelete],
  ['post', '/region/:uid/:uidProvince', handleForm],
  ['get', '/list/areas', getAreas],
  ['get', '/region/:uid', getByUid]
]