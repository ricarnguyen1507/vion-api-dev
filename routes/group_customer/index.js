"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'group_customer_image')
    }
  }
  return { statusCode: 200 }
}
async function getAll ({ query }) {
  const { number = -1, page = 0 } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    groupCustomers as summary(func: type(GroupCustomer)) @filter(not eq(is_deleted, true)){
      totalCount: count(uid)
    }
    result(func: uid(groupCustomers), orderasc: display_order${paginate})  {
      uid
      expand(_all_) {
        uid
        expand(_all_)
      }
    }
  }`)
  return { result: db.parseFacetArray(result), summary }
}

async function getOptions () {
  return await db.query(`{
    result(func: type(GroupCustomer)) {
      uid
      group_customer_name
    }
  }`)
}

async function getlist ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(GroupCustomer)", ...primaryFilters].join(', ') : "type(GroupCustomer)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    groupCustomers as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(groupCustomers), first: $number, offset: $offset) {
      uid
      expand(_all_) {
        uid
        expand(_all_)
      }
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })

  return {
    totalCount: summary[0]?.total || 0,
    data: db.parseFacetArray(data)
  }
}

module.exports = [
  ['get', '/list/group_customer', getAll],
  ['post', '/group_customer/list', getlist],
  ['get', '/group_customer/get-option', getOptions],
  ['post', '/group_customer', mutate]
]