"use strict"

const db = require("#/services/tvcdb")
const { saveMediaImage } = require("#/modules/uploadFileHandle")

async function getAll () {
  return await db.query(`{
    result(func: type(SplashScreen)) {
      uid
      expand(_all_)
    }
  }`)
}

async function handleForm (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields, files } = request.body

  await db.mutate(fields)

  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaImage(file, 'splashscreen_images')
    }
  }
  return { statusCode: 200 }
}

module.exports = [
  ['get', '/splash_screen', getAll],
  ['post', '/splash_screen', handleForm]
]