"use strict"

const db = require("#/services/tvcdb")
const session = require("#/modules/userSession")

const tableFields = `
  uid
  widget_name
  display_name
  display_status
  widget.products @facets(orderasc: display_order) @filter(eq(display_status, 2) AND NOT eq(is_deleted,true)) {
    uid
    product_name
    sku_id
    display_order
  }
`
async function getDataTable ({ query }) {
  const { number = -1, page = 0 } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    widgets as summary(func: type(Widget)) {
      totalCount: count(uid)
    }
    result(func: uid(widgets)${paginate}) {
      ${tableFields}
    }
    }`)
  return { result, summary }
}

async function mutateWidget (request) {
  const uid = request?.params?.uid

  /* Lấy data origin cho tracking */
  let dataOrigin = null
  if (uid && db.isUid(uid)) {
    const { origin } = await db.query(`{
      origin(func: uid(${uid})) {
        ${tableFields}
      }
    }`)
    dataOrigin = origin?.[0] || null
  }
  /* ============================= */

  const res = await db.mutate(request.body.fields)

  let addAction = true, getUid = ''
  if(db.isUid(uid)) {
    addAction = false
  }

  /* Lấy data sau khi edit cho tracking */
  let dataEdit = null
  getUid = uid.startsWith('_:') ? res.getUidsMap().get('new_widget') : uid
  const { edit } = await db.query(`{
    edit(func: uid(${getUid})) {
      ${tableFields}
    }
  }`)
  dataEdit = edit?.[0] || null
  /* ================================== */

  const userLogged = await session.getUser(request)
  process.emit("track", {
    pages: 'widget',
    dataOrigin,
    dataEdit,
    actionType: addAction ? 'ADD' : 'EDIT',
    userData: userLogged
  })

  return {
    statusCode: 200
  }
}

async function getlist ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Widget)", ...primaryFilters].join(', ') : "type(Widget)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    widget as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(widget), first: $number, offset: $offset) {
      ${tableFields}
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })

  return {
    totalCount: summary[0]?.total || 0,
    data: db.parseFacetArray(data)
  }
}

module.exports = [
  ['get', '/list/widget', getDataTable],
  ["post", "/widget/list", getlist],
  ['post', '/widget/:uid', mutateWidget]
]
