"use strict"

const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

const tableFields = `
  uid
  launcher_name
  display_status
  type_values @facets(orderasc: display_order){
    uid
    type_name
    display_name
    launcher.brandShop {
      uid
      brand_shop_name
    }
    launcher.product {
      uid
      product_name
      sku_id
    }
    launcher.collection_temp {
      uid
    }
    launcher.livestream {
      uid
    }
    launcher.collection {
      uid
    }
    launcher.landing_page {
      uid
    }
    launcher.promotions {
      uid
      display_name_detail
    }
    image
    tracking_utm{
      uid
      tracking_name
    }
  }
  launcher_customer_type
  launcher.Customer @facets(orderasc: display_order){
    uid
    phone_number
    customer_name
  }
  launcher.GroupCustomer @facets(orderasc: display_order){
    uid
    group_customer_name
  }
  launcher.ott{
    uid
    ott_name
  }
`

/* async function setDelete ({params: {uid}}) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const {count_products} = await db.query(`{
    result(func: uid(${uid})) {
      parent {
        uid
      }
      ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
        ppc as uid
      }
      childrent: ~parent {
        ~product.collection @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
          cpc as uid
        }
      }
    }

    count_products(func: uid(cpc, ppc)) {
      uid
    }
  }`)

  if (count_products.length > 0) { //is parent
    return {statusCode: 400, message: "Không được xoá ngành hàng có sản phẩm đang active"}
  }

  await db.upsert({
    query: `{
      var(func: uid(${uid})) {
        parent as uid
        highlight.products {
          php as uid
        }
        ~product.collection {
          ppc as uid
        }
        ~parent {
          children as uid
          highlight.products {
            chp as uid
          }
          ~product.collection {
            cpc as uid
          }
        }
      }
    }`,
    cond: "@if( eq(len(ppc), 0) AND eq(len(cpc), 0) )",
    set: [{
      uid: "uid(parent)",
      is_deleted: true
    }, {
      uid: "uid(children)",
      is_deleted: true
    }],
    del: [
      {
        uid: "uid(parent)",
        'highlight.products': 'uid(php)'
      }, {
        uid: "uid(children)",
        'highlight.products': 'uid(chp)'
      },
      {
        uid: "uid(ppc)",
        "product.collection": "uid(parent)"
      },
      {
        uid: "uid(cpc)",
        "product.collection": "uid(children)"
      },
    ]
  })
  return { statusCode: 200 }
} */

async function getAll ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Launcher)", ...primaryFilters].join(', ') : "type(Launcher)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    launchers as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(launchers), first: $number, offset: $offset) {
      ${tableFields}
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data: db.parseFacetArray(data)
  }
}

async function mutateLauncher (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'launcher')
    }
  }
  return { statusCode: 200 }
}
module.exports = [
  ['post', '/list/launcher', getAll],
  ['post', '/launcher', mutateLauncher]
]
