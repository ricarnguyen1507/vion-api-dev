"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body)
  return { statusCode: 200 }
}
async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    groupPayments as summary(func: type(GroupPayment)) @filter(${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(groupPayments)${paginate}) {
      uid
      display_status
      group_payment_name
      payment_methods{
          uid
          payment_name
      }
    }
    }`)
  return { result, summary }
}
// function loadMore(request) {
//   const { offset, number } = request.params
//   return db.query(`query result($number: string, offset: string) {
//     result(func: type(GroupPayment), first: $number, offset: $offset){uid,expand(_all_)}
//   }`, {
//     $number: number || 20,
//     $offset: offset || 0
//   })
// }
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(GroupPayment)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/group_payment', getAll],
  // ['get', '/list/group_payment/:offset/:number', loadMore],
  ['get', '/group_payment/:uid', getByUid],
  ['post', '/group_payment', mutate]
]