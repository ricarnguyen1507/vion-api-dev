const {
  exportProductCSV,
  exportErrorProduct
} = require("./export-product")

const { getUser } = require("#/modules/userSession")

function productExportHandler (request, reply) {
  log.info("Start export product")
  const fileName = `product_${Date.now()}.csv`
  exportProductCSV(fileName)
    .then(() => getUser(request))
    .then(user => {
      log.info("End export product")
      process.emit('send_notify_mail', {
        fromName: "Export product notify",
        toName: user.user_name || user.email,
        toAddr: user.email,
        subject: "Exported products",
        content: `<a href="https://cms.${config.domain}/export/${fileName}">Download ${fileName}</a>`
      })
    })
    .catch(err => {
      log.error("Export product", err)
    })
  reply.type("text/html").send(`<a href="/export/${fileName}">Download ${fileName}</a>`)
}

// FUCNTION EXPORT DATA ERROR FROM EXCEL IMPORT FILE
function productErrorExport ({ query }, reply) {
  log.info("-- Start export error product")
  const rows = JSON.parse(query?.export)
  reply.headers({
    "Content-Type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "Content-Disposition": "attachment; filename=product_error_export.xlsx"
  })
  exportErrorProduct(rows, reply.raw)
    .catch(err => {
      reply.send(err)
    })
    .then(() => {
      log.info("-- End export error product")
    })
}
// END FUCNTION EXPORT DATA ERROR FROM EXCEL IMPORT FILE

module.exports = [
  ['get', '/product/product_export.xlsx', productExportHandler],
  ['get', '/product/import_error_export.xlsx', productErrorExport]
]