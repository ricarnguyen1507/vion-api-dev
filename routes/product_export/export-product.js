const fs = require("fs")
const excel = require("exceljs")
const db = require("#/services/tvcdb")

const productXlsFieldNames = [
  "dateCreated",
  "uid",
  "sku_id",
  "ghn_code",
  "product_name",
  "display_name",
  "display_name_detail",
  "display_bill_name",
  "barcode",
  "partner_uid",
  "partner_name",
  "manufacturer_uid",
  "manufacturer_name",
  "brand_uid",
  "brand_name",
  "collection_uid",
  "collection_name",
  "original",
  "unit",
  "sub_unit",
  "sub_unit_quantity",
  "packaging_unit",
  "packaging_unit_quantity",
  "short_desc",
  "keywords",
  "UOM_uid",
  "UOM_name",
  "uom_quantity",
  "long",
  "width",
  "height",
  "cost_price_without_vat",
  "cost_price_with_vat",
  "listed_price_without_vat",
  "listed_price_with_vat",
  "price_without_vat",
  "price_with_vat",
  "front_margin",
  "return_terms",
  "payment_terms",
  "promotion_desc",
  "promotion_detail",
  "instock",
  "stock",
  "is_disabled_cod",
  "warranty_policy",
  "shelf_life",
  "region_uid",
  "name",
  "short_description_1",
  "display_name_1",
  "short_description_2",
  "display_name_2",
  "short_description_3",
  "display_name_3",
  "short_description_4",
  "display_name_4",
  "description_html",
  "product_tag_1",
  "product_tag_name_1",
  "product_tag_2",
  "product_tag_name_2",
  "product_tag_3",
  "product_tag_name_3",
  "product_tag_4",
  "product_tag_name_4",
  "display_status",
  "brand_shop_name",
  "brand_shop_uid",
  "brand_shop_collection_name",
  "brand_shop_collection_uid",
  "specs_type_uid_1",
  "spec_type_value_1",
  "specs_type_uid_2",
  "spec_type_value_2",
]

function buildRowData (fields, data, defaultValue) {
  const entries = fields.map(field => [field, data[field] ?? defaultValue])
  return Object.fromEntries(entries)
}

function parseRow (fields, rowData) {
  // PREPARE DEFAULT DATA FOR ROW DATA

  if (rowData?.['product.areas']) {
    rowData.name = rowData?.['product.areas'].map(a => a.name).join(",")
    rowData['product.areas'] = rowData?.['product.areas'].map(a => a.uid).join(",")
  }
  // END PREPARE REGION DATA

  // PREPARE COOLECTION DATA
  if (rowData['product.collection']) {
    /**try to fix this */
    const collectionListTemp = rowData['product.collection']
    rowData['collection_name'] = collectionListTemp?.map(a => a.collection_name).join(",")
    rowData['product.collection'] = collectionListTemp?.map(a => a.uid).join(",")

  }
  // PARSE NODE EDGE FOR PRODUCT
  rowData['manufacturer_name'] = rowData['product.manufacturer']?.manufacturer_name
  rowData['product.manufacturer'] = rowData['product.manufacturer']?.uid

  rowData['brand_name'] = rowData['product.brand']?.brand_name
  rowData['product.brand'] = rowData['product.brand']?.uid

  rowData['UOM_name'] = rowData['product.uom']?.UOM_name
  rowData['product.uom'] = rowData['product.uom']?.uid

  rowData['partner_name'] = rowData['product.partner']?.partner_name
  rowData['product.partner'] = rowData['product.partner']?.uid

  if(Array.isArray(rowData['product.pricing'])) {
    rowData['product.pricing'] = rowData['product.pricing'][0]
  }

  // SET PRICING PRODUCT DATA
  rowData.cost_price_without_vat = rowData['product.pricing']?.cost_price_without_vat
  rowData.cost_price_with_vat = rowData['product.pricing']?.cost_price_with_vat
  rowData.listed_price_without_vat = rowData['product.pricing']?.listed_price_without_vat
  rowData.listed_price_with_vat = rowData['product.pricing']?.listed_price_with_vat
  rowData.price_without_vat = rowData['product.pricing']?.price_without_vat
  rowData.price_with_vat = rowData['product.pricing']?.price_with_vat
  rowData.front_margin = rowData['product.pricing']?.front_margin
  // END SET PRICING PRODUCT DATA

  // SET SHORT DESCRIPTION PRODUCT DATA
  rowData.display_name_1 = rowData?.short_description_1?.display_name
  rowData.short_description_1 = rowData?.short_description_1?.uid

  rowData.display_name_2 = rowData?.short_description_2?.display_name
  rowData.short_description_2 = rowData?.short_description_2?.uid

  rowData.display_name_3 = rowData?.short_description_3?.display_name
  rowData.short_description_3 = rowData?.short_description_3?.uid

  rowData.display_name_4 = rowData?.short_description_4?.display_name
  rowData.short_description_4 = rowData?.short_description_4?.uid

  // END SET SHORT DESCRIPTION PRODUCT DATA

  // SET PRODUCT TAG
  if (rowData['product.tag']) {
    for (const objTag of rowData['product.tag']) {
      if (objTag.tag_category == "assess") {
        rowData.product_tag_1 = objTag.uid
        rowData.product_tag_name_1 = objTag.tag_title
      } else if (objTag.tag_category == "promotion") {
        rowData.product_tag_2 = objTag.uid
        rowData.product_tag_name_2 = objTag.tag_title
      } else if (objTag.tag_category == "shipping") {
        rowData.product_tag_3 = objTag.uid
        rowData.product_tag_name_3 = objTag.tag_title
      } else if (objTag.tag_category == "name_tag") {
        rowData.product_tag_4 = objTag.uid
        rowData.product_tag_name_4 = objTag.tag_title
      }
    }
  }

  // END SET PRODUCT TAG

  // PARSE DISPLAY STATUS PRODUCT
  if (rowData.display_status == 0) {
    rowData.display_status = "PENDING"
  } else if (rowData.display_status == 1) {
    rowData.display_status = "REJECTED"
  } else if (rowData.display_status == 2) {
    rowData.display_status = "APPROVED"
  } else if (rowData.display_status == 3) {
    rowData.display_status = "IMPORT"
  }
  // END PARSE DISPLAY STATUS PRODUCT

  if(rowData?.brand_shop?.length) {
    rowData.brand_shop_name = rowData?.brand_shop[0].brand_shop_name
    rowData.brand_shop_uid = rowData?.brand_shop[0].uid
    const brand_shop_collection = rowData?.brand_shop[0]?.brand_shop_collection ?? []
    if(brand_shop_collection?.length) {
      const collection_product = brand_shop_collection?.filter(col => col?.["highlight.products"]?.find(prod => prod?.uid === rowData.uid)) ?? []
      if(collection_product.length) {
        rowData.brand_shop_collection_name = collection_product[0]?.collection_name
        rowData.brand_shop_collection_uid = collection_product[0]?.uid
      }
    }
  }
  if(rowData?.specs_sections?.length && rowData?.specs_sections?.length <= 2) {
    rowData.specs_type_uid_1 = rowData?.specs_sections[0]?.section_value
    rowData.spec_type_value_1 = rowData?.specs_sections[0]?.section_ref_value
    rowData.specs_type_uid_2 = rowData?.specs_sections[1]?.section_value
    rowData.spec_type_value_2 = rowData?.specs_sections[1]?.section_ref_value
  }

  let str = JSON.stringify(rowData);
  str = str.replace(/"product.areas":/g, "\"region_uid\":");
  str = str.replace(/"product.manufacturer":/g, "\"manufacturer_uid\":");
  str = str.replace(/"product.brand":/g, "\"brand_uid\":");
  str = str.replace(/"product.partner":/g, "\"partner_uid\":");
  str = str.replace(/"product.collection":/g, "\"collection_uid\":");
  str = str.replace(/"product.uom":/g, "\"UOM_uid\":");

  rowData = JSON.parse(str);

  // PREPARE ROW DATA TO EXPORT
  return buildRowData(fields, rowData, '')
}

function exportProductStream (fields, replyStream) {
  replyStream.chunkedEncoding = true
  replyStream.setHeader("Transfer-Encoding", "chunked")
  replyStream.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
  replyStream.setHeader("Content-Disposition", "attachment; filename=Product_Export.xlsx")
  // stream.setDefaultEncoding("utf8")
  var workbook = new excel.stream.xlsx.WorkbookWriter({ stream: replyStream })
  var worksheet = workbook.addWorksheet()
  worksheet.columns = fields.map(dataField => ({ header: dataField, key: dataField, width: 10 }))
  return {
    addRows (rows) {
      for(const row of rows) {
        worksheet.addRow(parseRow(fields, row)).commit()
      }
    },
    commit () {
      return workbook.commit()
    }
  }
}

async function getProducts (offset, limit) {
  const query = `{
    productID as products(func: type(Product), first: ${limit}, offset: ${offset}) @filter(not eq(is_deleted, true)){
      uid
      sku_id
      ghn_code
      product_name
      display_name
      display_name_detail
      barcode
      product.manufacturer{
        uid
        manufacturer_name
      }
      product.brand{
        uid
        brand_name
      }
      product.partner{
        uid
        partner_name
      }
      product.collection (first: 1) @filter(has(parent) AND eq(collection_type, 0) AND not eq(is_temporary, true) AND not eq(is_deleted, true)){
        uid
        collection_name
      }
      original
      unit
      sub_unit
      sub_unit_quantity
      packaging_unit
      packaging_unit_quantity
      short_desc
      keywords
      product.uom{
        uid
        UOM_name
      }
      uom_quantity
      long
      width
      height
      product.pricing (first: -1) {
        cost_price_without_vat
        cost_price_with_vat
        listed_price_without_vat
        listed_price_with_vat
        price_without_vat
        price_with_vat
        front_margin
      }
      store
      return_terms
      payment_term
      promotion_desc
      promotion_detail
      instock
      stock
      is_disabled_cod
      display_bill_name
      warranty_policy
      shelf_life
      product.areas{
        uid
        name
      }
      short_description_1{
        uid
        display_name
      }
      short_description_2
      {
        uid
        display_name
      }
      short_description_3{
        uid
        display_name
      }
      short_description_4{
        uid
        display_name
      }
      description_html
      product.tag{
        uid
        tag_category
        tag_title
      }
      display_status
      brand_shop : ~brand_shop.product{
        uid
        brand_shop_name
        brand_shop_collection : brand_shop.collection @filter(NOT eq(is_deleted, true) AND (eq(collection_type, 400)) AND NOT eq(is_temporary, true)AND NOT has(~parent)){
          uid
          collection_name
          highlight.products @filter(uid(productID)){
            uid
            product_name
          }
        }
      }
      specs_sections: product.specs_sections{
        section_ref_value
        section_value
        uid
      }
    }
  }`
  return (await db.query(query)).products
}

/* async function exportProduct (stream) {
  // COUNT ALL PRODUCT
  let {result} = await db.query(`{
    result(func: type(Product)) @filter(not eq(is_deleted, true)) {
        count(uid)
    }
  }`)

  const total = result[0]?.count

  if(!total) {
    throw { statusCode: 404, message: "no products to export"}
  }

  const exportStream = exportProductStream(productXlsFieldNames, stream)

  const limit = 250
  let offset = 0

  while (offset < total) {
    const products = await getProducts(offset, limit)
    exportStream.addRows(products)
    offset += limit
  }
  await exportStream.commit()
} */

async function exportProductCSV (fileName) {
  const { r1, r2 } = await db.query(`{
    r1(func: type(Product)) {
      count(uid)
    }
    r2(func: type(Product)) @filter(not eq(is_deleted, true)) {
      count(uid)
    }
  }`)

  const total = r1[0].count
  const available = r2[0].count

  if(available > 0) {
    console.log("-- Export", available, "product")
    const limit = 100
    let offset = 0
    const ws = fs.createWriteStream(`/tmp/${fileName}`)
    ws.write('\uFEFF' + productXlsFieldNames.join(",") + "\n")
    while (offset <= total) {
      const products = await getProducts(offset, limit)
      for(const row of products) {
        const csvRow = Object.values(parseRow(productXlsFieldNames, row)).map(cell => {
          if (typeof cell === 'string') {
            cell = cell.trim()
            if(cell.length > 0) {
              if(cell.replace(/ /g, '').match(/[\s,"]/)) {
                cell = cell.replace(/"/g, '""')
              }
              return '"' + cell + '"'
            }
          }
          return cell;
        }).join(',')
        ws.write(csvRow)
        ws.write("\n")
      }
      offset += limit
    }
    ws.end()
    fs.copyFileSync(`/tmp/${fileName}`, `/public/export/${fileName}`)
    fs.rmSync(`/tmp/${fileName}`)
  } else {
    throw new Error("nothing to export")
  }
}

async function exportProduct (fileName) {
  const { result } = await db.query(`{
    result(func: type(Product)) @filter(not eq(is_deleted, true)) {
        count(uid)
    }
  }`)
  const total = result[0]?.count
  if(!total) {
    throw { statusCode: 404, message: "no products to export" }
  }

  const stream = fs.createWriteStream(`/tmp/${fileName}`)
  var workbook = new excel.stream.xlsx.WorkbookWriter({ stream })
  var worksheet = workbook.addWorksheet()
  worksheet.columns = productXlsFieldNames.map(dataField => ({ header: dataField, key: dataField, width: 10 }))

  const limit = 100
  let offset = 0

  while (offset < total) {
    const products = await getProducts(offset, limit)
    for(const row of products) {
      worksheet.addRow(parseRow(productXlsFieldNames, row)).commit()
    }
    offset += limit
  }
  await workbook.commit()
  fs.copyFileSync(`/tmp/${fileName}`, `/public/export/${fileName}`)
  fs.rmSync(`/tmp/${fileName}`)
}

async function exportErrorProduct (rows, stream) {

  const total = rows.length

  if(!total) {
    throw { statusCode: 404, message: "no data to export" }
  }

  const exportStream = exportProductStream([
    "dateCreated",
    "stt",
    "field_name",
    "error_value"
  ], stream)

  rows[0].dateCreated = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })

  let offset = 0
  const limit = 250
  while (offset < total) {
    exportStream.addRows(await getProducts(offset, limit))
    offset += limit
  }
  await exportStream.commit()
}

module.exports = {
  exportProductCSV,
  exportProduct,
  exportErrorProduct
}