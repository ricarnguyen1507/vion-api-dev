"use strict"
const db = require("#/services/tvcdb")
const { saveMediaFiles } = require("#/modules/uploadFileHandle")

const imageFolder = "notify_images"

const axios = require("axios").create({
  // baseURL: "http://42.118.166.43:32529/notification/push/",
  baseURL: "https://nis.fptplay.net/notification/push",
  headers: {
    secureKey: "to4Hq6eL(80~h8?k5Qdqkj|{KSABc!",
  },
})
async function getByType ({ query }) {
  const { number = -1, page = 0, $type } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  if ($type === "users" || $type === "platforms" || $type === "room") {
    const { result, summary } = await db.query(
      `query result($type: string) {
        notifies as summary(func: eq(type_notify, $type)) @filter(type(Notify) and not eq(is_deleted,true)){
          totalCount: count(uid)
        }
        result(func: uid(notifies)${paginate})  {
          uid
          expand(_all_) {
            uid
            expand(_all_)
          }
        }
      }`,
      { $type }
    )
    return { result, summary }
  } else {
    throw new Error("Làm gì dấy ?")
  }
}


// Up dữ liệu chung => thay đổi ở cái type
async function mutateNotify (request, reply) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const type_notify = request.params.type
  if (type_notify === "users" || type_notify === "platforms" || type_notify === "room") {
    const { fields, files } = request.body
    const { del, set } = fields
    if (del) {
      const delData = JSON.parse(del)
      await db.mutate({ del: delData })
    }
    if (set) {
      const setData = JSON.parse(set)
      const uid = await db
        .mutate({
          set: {
            uid: "_:new_notify",
            "dgraph.type": "Notify",
            type_notify: type_notify,
            ...setData,
          },
        })
        .then((res) => setData.uid || res.getUidsMap().get("new_notify"))
      let images = {}
      if (Object.keys(files).length > 0) {
        images = saveMediaFiles(files, imageFolder)
        await db.mutate({
          set: {
            uid,
            ...images,
          },
        })
      }
      return {
        statusCode: 200,
        new_notify: uid,
        images: images
      }
    }
    return { statusCode: 200 }
  } else {
    reply.send("Làm gì dấy ? ")
  }
}

// Gửi notify qua FPT_play
async function sendNotify (request) {
  const type = request.params.type
  const $uid = request.body.uid

  const image_url = (uri) => uri ? `${config.image_url}/${uri}` : ""

  if ($uid && (type === "users" || type === "platforms" || type === "room")) {
    const row = await db.query(
      `query result($uid: string) {
            result(func: uid($uid)) @filter(type(Notify)){
              uid
              expand(_all_)
            }
        }`,
      { $uid }
    ).then(res => res.result[0])

    const postData = {
      app: "fplay",
      ...(type === 'room' && { room_id: "tvshopping", room_type: "tvshopping" }),
      type: "tvshopping",
      ...(type === 'users' && { users: row.notify_phone_customer.split(",") }),
      type_id: row.type_id,
      message: {
        body: row.notify_body || "",
        inbox_content: "",
        title: row.notify_title || "",
        url: "com.bda.shoppingtv",
        image: image_url(row.notify_image),
        inbox_type: "None",
        preview_info: {
          preview_title: row.preview_title || "",
          preview_img: image_url(row.preview_img),
          preview_description: row.preview_description || "",
          icon: image_url(row.preview_icon),
        },
      },
      start_at: new Date(row.start_at).toISOString(),
      stop_at: new Date(row.stop_at).toISOString(),
      platforms: row.platform_notify.split(","),
    }
    log.debug(`--- send_notify: ${type} ---`)
    log.debug(JSON.stringify(postData))
    const response = await axios.post(`/${type}`, postData).catch(error => error.response)
    return response.data
  }
  throw new Error("Lam gi o day")
}

// Xóa nút hiển thị
async function setDisplay ({ params: { uid } }) {
  if (uid) {
    await db.mutate({ set: { uid, is_display: true } })
  }
  return { statusCode: 200 }
}
// Xóa theo uid
async function setDelete ({ params: { uid } }) {
  if (uid) {
    await db.mutate({ set: { uid, is_deleted: true } })
  }
  return { statusCode: 200 }
}

async function getByUid (request) {
  return db.query(
    `query result($uid: string) {
      result(func: uid($uid)) @filter(type(Notify) and not eq(is_deleted,true)){
        uid
        expand(_all_)
      }
    }`,
    { $uid: request.params.uid }
  )
}
module.exports = [
  ["get", "/list/notify/:type", getByType],
  ["get", "/notify/:uid", getByUid],
  ["post", "/notify/:type", mutateNotify],
  ["post", "/notify/send/:type", sendNotify],
  ["delete", "/notify/:uid", setDelete],
  ["delete", "/notify/display/:uid", setDisplay],
]
