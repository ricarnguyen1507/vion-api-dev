// const sendMail = require("#/services/notify_mailer")

const { exportOrder } = require("./export-order")

async function orderExportHandler (request, reply) {
  log.info("Start export order")
  const fileName = `order_${Date.now()}.xlsx`

  const xls = await exportOrder(request)
  reply.headers({
    "Content-Type": "application/octet-stream; ; charset=utf-8",
    "Content-Disposition": `attachment; filename=${fileName}`
  })
  const res = reply.raw
  res.sent = true
  res.end(xls, 'binary')
}

module.exports = [
  ['get', '/order/order_export.xlsx', orderExportHandler],
]