const excel = require("exceljs")
const db = require("#/services/tvcdb")
const orderFragment =
`
created_at
uid
order_id
notes
transaction_id
total_pay
pay_status
order_status
pay_gateway
address_des
customer_name
phone_number
applied_value: voucher_value
order.customer {
  customer_name
  phone_number
}
province {
  uid
  name
}
district {
  uid
  name
}
sub_orders {
  uid
  order_id
  partner_id
  order_status
  notes
  shipping_code
  reason
  order.partner {
    uid
    partner_name
    shipping_method
  }
  shipping_partner_value
  date_delivery_success
  order_items {
    uid
    product_name
    quantity
    sell_price
    price_without_vat
    cost_price
    cost_price_without_vat
    discount
    promotion_desc
    promotion_detail
    product: order.product {
      sku_id
      barcode
      promotion_detail
      promotion_desc
      payment_terms
      return_terms
      unit
      areas {
        uid
        name
      }
    }
  }
}
ott : user.ott {
  uid
  ott_name
}
voucher_usage: ~voucher_usage.order @normalize {
  uid
  voucher_uid: voucher_uid
  voucher_usage.voucher {
    voucher_usage_code: voucher_code
    voucher_value: voucher_value
    voucher_type: voucher_type
  }
}
`
const orderXlsFieldNames = [
  "Thời gian/ngày tháng export",
  "Thời gian KH đặt hàng",
  "Mã đơn hàng",
  "Mã đơn hàng con",
  "Tên KH",
  "SĐT KH",
  "Người nhận",
  "SĐT nhận",
  "Địa chỉ nhận",
  "TP/Tỉnh",
  "Quận/Huyện",
  "Tên sản phẩm",
  "Mã SKUs",
  "Barcode",
  "Số lượng",
  "Đơn vị tính",
  "Mô tả CTKM",
  "Chương trình khuyến mãi",
  "Giá vốn (No VAT)",
  "Giá vốn (Có VAT)",
  "Giá bán (No VAT)",
  "Giá bán (Có VAT)",
  "Thành tiền",
  "VoucherID",
  "Loại Voucher",
  "Giá trị Voucher",
  "Tổng tiền đã giảm",
  "Tổng tiền đơn hàng con",
  "Hình thức thanh toán",
  "Trạng thái thanh toán",
  "Tình trạng đơn hàng tổng",
  "Tình trạng đơn hàng",
  "Khu vực",
  "Lý do",
  "Mã vận đơn",
  "PTGH",
  "ĐVVC",
  "Ghi chú đơn hàng con",
  "Ngày giao hàng thành công",
  "Nhà Cung cấp",
  "Transaction ID",
  "Ghi chú đơn hàng cha",
  "Tổng tiền chưa giảm",
  "OTT"
]

function buildRowData (fields, data, defaultValue) {
  const entries = fields.map(field => [field, data[field] ?? defaultValue])
  return Object.fromEntries(entries)
}
function parseOrderStatus (status) {
  if(status) {
    if(status == 1) {
      return 'Đang xác nhận'
    } else if(status == 2) {
      return 'Đã xác nhận'
    } else if(status == 3) {
      return 'Đang giao'
    } else if(status == 4) {
      return 'Đã giao'
    } else if(status == 5) {
      return 'Yêu cầu huỷ đơn'
    } else if(status == 6) {
      return 'Đã hủy'
    } else if(status == 7) {
      return 'Đang hoàn trả đơn hàng'
    } else if(status == 8) {
      return 'Đã hoàn trả đơn hàng'
    } else if(status == 9) {
      return 'Đã hoàn tiền'
    } else if(status == 10) {
      return 'Đối tác đang xử lý'
    } else if(status == 11) {
      return 'Đối tác đã nhận cọc'
    }
  }
}
function parseTotalPay (sub_order) {
  let total = 0
  for(const item of sub_order.order_items) {
    const totalItemPrice = parseInt(item.quantity * item.sell_price)
    total += totalItemPrice
  }
  return total
}
const getDate = (timestamp = null) => {
  const date = timestamp ? new Date(timestamp).toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }) : new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  const resDate = date//`${day}/${month}/${year} ${hour}:${min}`
  return resDate
}
async function parseRow (fields, row, sub_order, item, reasons) {
  // PREPARE DEFAULT DATA FOR ROW DATA
  const rowData = {}
  rowData["Thời gian/ngày tháng export"] = getDate()
  rowData["Thời gian KH đặt hàng"] = getDate(row?.created_at) ?? ''
  rowData["Mã đơn hàng"] = row?.order_id ?? ''
  rowData["Mã đơn hàng con"] = sub_order?.order_id ?? ''
  rowData["Tên KH"] = row['order.customer']?.customer_name ?? ''
  rowData["SĐT KH"] = row['order.customer']?.phone_number ?? ''
  rowData["Người nhận"] = row?.customer_name ?? ''
  rowData["SĐT nhận"] = row?.phone_number ?? ''
  rowData["Địa chỉ nhận"] = row?.address_des ?? ''
  rowData["TP/Tỉnh"] = row?.province?.name ?? ''
  rowData["Quận/Huyện"] = row?.district?.name ?? ''
  rowData["Tên sản phẩm"] = item?.product_name ?? ''
  rowData["Mã SKUs"] = item?.product?.sku_id ?? ''
  rowData["Barcode"] = item?.product?.barcode ?? ''
  rowData["Số lượng"] = item?.quantity ?? ''
  rowData["Đơn vị tính"] = item?.product?.unit ?? ''
  rowData["Mô tả CTKM"] = item?.promotion_detail ?? ''
  rowData["Chương trình khuyến mãi"] = item?.promotion_desc ?? ''
  rowData["Giá vốn (No VAT)"] = item?.cost_price_without_vat ?? ''
  rowData["Giá vốn (Có VAT)"] = item?.cost_price ?? ''
  rowData["Giá bán (No VAT)"] = item?.price_without_vat ?? ''
  rowData["Giá bán (Có VAT)"] = item?.sell_price ?? ''
  rowData["Thành tiền"] = (item?.sell_price * item.quantity) ?? ''
  rowData["VoucherID"] = row?.voucher_usage?.length ? row?.voucher_usage[0].voucher_uid : ''

  if(row?.voucher_usage?.length) {
    if(row?.voucher_usage[0].voucher_type == 0) {
      rowData["Loại Voucher"] = `Giảm giá trực tiếp - Giá trị(${row?.voucher_usage[0].voucher_value})`
    } else if(row?.voucher_usage[0].voucher_type == 1) {
      rowData["Loại Voucher"] = `Giảm theo % - Giá trị(${row?.voucher_usage[0].voucher_value})`
    } else if(row?.voucher_usage[0].voucher_type == 2) {
      rowData["Loại Voucher"] = 'Freeship'
    }
  }else{
    rowData["Loại Voucher"] = ''
  }
  rowData["Giá trị Voucher"] = row?.voucher_usage?.length ? row?.voucher_usage[0].voucher_value : ''
  rowData["Tổng tiền đã giảm"] = row?.total_pay ?? ''
  rowData["Tổng tiền đơn hàng con"] = await parseTotalPay(sub_order)
  rowData["Hình thức thanh toán"] = row?.pay_gateway ?? ''
  if(row?.pay_status == 1) {
    rowData["Trạng thái thanh toán"] = 'Success'
  } else if(row?.pay_status == 2) {
    rowData["Trạng thái thanh toán"] = 'Failed'
  } else if(row?.pay_status == 3) {
    rowData["Trạng thái thanh toán"] = 'Pending'
  } else {
    rowData["Trạng thái thanh toán"] = ''
  }

  rowData["Tình trạng đơn hàng tổng"] = row?.order_status ? parseOrderStatus(row.order_status) : ''
  rowData["Tình trạng đơn hàng"] = sub_order?.order_status ? parseOrderStatus(sub_order.order_status) : ''
  /**get areas data */
  const areaListTemp = item?.product?.areas
  rowData["Khu vực"] = areaListTemp?.map(a => a.name).join(",")
  if(sub_order?.reason && reasons.length) {
    for(const obj of reasons) {
      if(obj.reason_value == sub_order?.reason) {
        rowData["Lý do"] = obj.reason_name
      }
    }
  } else{
    rowData["Lý do"] = ''
  }
  rowData["Mã vận đơn"] = sub_order?.shipping_code ?? ''
  if(sub_order["order.partner"]?.shipping_method == 0) {
    rowData["PTGH"] = 'FORWARDING'
  } else if (sub_order["order.partner"]?.shipping_method == 1) {
    rowData["PTGH"] = 'STOCKING'
  } else if(sub_order["order.partner"]?.shipping_method == 2) {
    rowData["PTGH"] = 'STOCK_AT_SUP'
  } else {
    rowData["PTGH"] = ''
  }
  rowData["ĐVVC"] = sub_order?.shipping_partner_value ?? ''
  rowData["Ghi chú đơn hàng con"] = sub_order?.notes ?? ''
  rowData["Ngày giao hàng thành công"] = sub_order?.date_delivery_success ? getDate(sub_order?.date_delivery_success) : ''
  rowData["Nhà Cung cấp"] = sub_order["order.partner"]?.partner_name ?? ''
  rowData["Transaction ID"] = row?.transaction_id ?? ''
  rowData["Ghi chú đơn hàng cha"] = row?.notes ?? ''
  rowData["Tổng tiền chưa giảm"] = row?.applied_value + row?.total_pay ?? ''
  rowData["OTT"] = row?.ott?.ott_name ?? ''

  /**end get areas data */
  return buildRowData(fields, rowData, '')
}

async function getOrders (filter) {
  const query = `{
    orders as summary(func: type(PrimaryOrder)) @filter(not eq(order_status, 0) AND ${filter}) {
      totalCount: count(uid)
    }
    data(func: uid(orders), orderdesc: created_at) {
      ${orderFragment}
    }
    status(func:type(Status)) {
      status_value
      status_name
      status_code
    }
    shipping_partners(func: type(ShippingPartner)) {
      uid
      shipping_partner_name
      shipping_partner_value
    }
    reasons(func: type(Reason)) {
      uid
      reason_name
      reason_value
    }
    partner(func:type(Partner)) {
      partner_name
      id
    }
  }`
  const ObjOrder = await db.query(query)
  return ObjOrder
}

async function exportOrder (request) {
  let { filter = '' } = request.query
  const { selectedDateFrom = '', selectedDateTo = '' } = request.query

  if (selectedDateFrom) {
    filter += `ge(created_at, ${selectedDateFrom})`
  }

  if (selectedDateTo) {
    filter += ` AND le(created_at, ${selectedDateTo})`
  }

  const { result } = await db.query(`{
    result(func: type(PrimaryOrder)) @filter(${filter}) {
      count(uid)
    }
  }`)
  const total = result[0]?.count
  if(!total) {
    throw { statusCode: 404, message: "no products to export" }
  }
  var workbook = new excel.Workbook();
  var worksheet = workbook.addWorksheet()
  worksheet.columns = orderXlsFieldNames.map(dataField => ({ header: dataField, key: dataField, width: 10 }))
  const orders = await getOrders(filter)
  if(orders?.data?.length) {
    parseOrderItem(orders.data)
    for(const row of orders.data) {
      if(row?.sub_orders?.length) {
        for(const sub_order of row.sub_orders) {
          if(sub_order?.order_items?.length) {
            for(const item of sub_order.order_items) {
              worksheet.addRow(await parseRow(orderXlsFieldNames, row, sub_order, item, orders.reasons)).commit()
            }
          }
        }
      }
    }
  }
  return workbook.xlsx.writeBuffer()
}
function parseOrderItem (data) {
  const date_submit = new Date('8/18/2021, 2:30:25 PM')
  if(data?.length) {
    for(const obj of data) {
      if(obj?.created_at <= date_submit.valueOf()) {
        if(obj?.sub_orders?.length) {
          for(const sub of obj.sub_orders) {
            if(sub?.order_items?.length) {
              sub?.order_items.map(item => item.promotion_detail = item?.product?.promotion_detail)
            }
          }
        }
      }
    }
  }
}

module.exports = {
  exportOrder
}