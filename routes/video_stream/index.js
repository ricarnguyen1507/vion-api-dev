"use strict"
const fs = require("fs")
const path = require("path")
const db = require("#/services/tvcdb")
const { image_dir } = config
const { saveMediaFiles } = require("#/modules/uploadFileHandle")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

const imageFolder = "stream_media"

const videoStreamFields = `
uid
stream.name
stream.display_name
stream.start_time
stream.end_time
stream.image_thumb
stream.image_fullscreen
stream.products {
  uid
  stream_product.product {
    uid
    product_name
  }
  time
}
stream.view_count
stream.view_virtual
stream.is_highlight
stream.channel {
  uid
  channel.name
  channel.link
}
stream.video_transcode
display_status
is_portrait
`

function getAll () {
  return db.query(`{ result(func: type(VideoStream)){
    ${videoStreamFields}
  }}`)
}
async function getOption ({ query: { brand_shop_uid } }) {
  const option_fields = `
  uid
  stream.name
  stream.display_name`
  if (brand_shop_uid && db.isUid(brand_shop_uid)) {
    return await db.query(`{
      var(func: uid(${brand_shop_uid})) @filter(has(brand_shop.video_stream)) {
        brand_shop.video_stream @filter(eq(display_status, 2)) {
          vs as uid
        }
      }
      result(func: uid(vs)) @filter(eq(display_status, 2)) {
        ${option_fields}
      }
    }`)
  }
  return await db.query(`{ result(func: type(VideoStream)) @filter(NOT has(~brand_shop.video_stream)) {
    ${option_fields}
  }}`)
}

async function streamFilter ({ body }) {
  const { number = 10, page = 0, func = '', filter = '' } = body
  const funcStr = func !== '' ? `
      streams as summary(${func}) @filter(type(VideoStream) AND NOT has (~brand_shop.video_stream) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }` : `
      streams as summary(func: type(VideoStream)) @filter(NOT has (~brand_shop.video_stream) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }`
  const { data, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    data(func: uid(streams), first: $number, offset: $offset)  {
      ${videoStreamFields}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { data, summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page } }
}

function getUid (uid) {
  return db.query(`{
    result(func: uid(${uid})) @filter(type(VideoStream))
    {
      ${videoStreamFields}
    }
  }`)
}

function getStreamUId (request) {
  const uid = request?.params?.uid ?? '';
  return getUid(uid);
}
async function streamChangeStatus ({ body }) {
  const { uid, status } = body ?? {}
  try{
    await db.upsert({
      query: `{
        var(func: uid(${uid})) @filter(type(VideoStream)) {
          c as uid
        }
      }`,
      cond: "@if(gt(len(c), 0))",
      set: {
        uid: "uid(c)",
        display_status: status
      }
    });
    return {
      statusCode: 200,
      data: getUid(uid)
    }
  }catch(err) {
    return {
      statusCode: 500,
      data: getUid(uid),
      error: err
    }
  }
}

function isLive (rowData) {
  // 0 --> Đang live
  // -1 ---> Chưa live
  // 1 ---> Đã live
  const time_start = new Date(rowData['stream.start_time']);
  const time_end = new Date(rowData['stream.end_time']);
  const now = new Date();
  return (now >= time_start && now <= time_end) && false
}

async function mutate (request) {
  if (!request.isMultipart()) {
    throw new Error("multipart request only")
  }
  const { fields, files } = request.body
  const { set, del, filesDel } = fields
  if(set && JSON.parse(set)) {
    const setData = JSON.parse(set)
    const { result } = await getUid(setData?.uid ?? '');
    if(result && result.length) {
      if(isLive(result[0])) {
        throw new Error("Nội dung đang live. không thể chỉnh sửa")
      }
    }
  }
  if (del) {
    const delData = JSON.parse(del);
    if(Object.keys(delData).length) {
      if(delData['stream.video_transcode'] == null) {
        delete delData['stream.video_transcode']
      }
      if(delData['stream.end_time'] == null) {
        delete delData['stream.end_time']
      }
      const temp = Object.keys(delData).length > 1 ? [delData] : [];
      if(delData['stream.products']) {
        delData['stream.products'].forEach(item => {
          temp.push({ uid: item.uid })
        })
      }
      await db.mutate({ del: temp })
    }
  }

  if(filesDel && JSON.parse(filesDel)) {
    const old_files = JSON.parse(filesDel);
    for(const key in old_files) {
      try {
        fs.unlinkSync(path.join(image_dir, old_files[key]))
      } catch(err) {
        log.error(err)
      }
    }
  }

  if (set) {
    const setData = JSON.parse(set)
    if(setData && setData['stream.products']) {
      setData['stream.products'].map(item => {
        item["dgraph.type"] = "StreamProduct"
      })
    }
    let uid;
    if(Object.keys(setData).length) {
      uid = await db
        .mutate({
          set: {
            uid: "_:new_video_stream",
            "dgraph.type": "VideoStream",
            ...setData
          }
        })
        .then((res) => setData.uid || res.getUidsMap().get("new_video_stream"))
    }

    if(Object.keys(files).length > 0 && uid) {
      const fields_files = saveMediaFiles(files, imageFolder)
      db.mutate({
        set: {
          uid,
          ...fields_files
        }
      })
    }

    return getAll()
  }
  return { statusCode: 200 }
}

/* ===============================  Phần update sử dụng Node =================================== */
async function getlist_node ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(VideoStream)", ...primaryFilters].join(', ') : "type(VideoStream)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    livestream as summary(func: ${str1}) @filter(${str2} AND NOT has (~brand_shop.video_stream)) {
      total: count(uid)
    }
    data(func: uid(livestream), first: $number, offset: $offset) {
      ${videoStreamFields}
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function mutate_node (request) {
  const { fields, files } = request.body

  if(request.isMultipart()) {
    await db.mutate(fields)
    if(Object.keys(files).length > 0) {
      for(const file of Object.entries(files)) {
        saveMediaMD5(file, imageFolder)
      }
    }
  }
  else{
    const { set } = fields
    if(typeof set?.uid === 'string' && set?.uid && db.isUid(set?.uid)) {
      await db.mutate({ set })
    }
  }
  return { statusCode: 200 }
}
module.exports = [
  ["get", "/video-stream", getAll],
  ["get", "/video-stream-options", getOption],
  ["get", "/video-stream/:uid", getStreamUId],
  ["post", "/video-stream", mutate],
  ["post", "/video-stream-filter", streamFilter],
  ["post", "/video-stream/change-status", streamChangeStatus],
  ["post", "/video-stream/list", getlist_node],
  ["post", "/video-stream/edit", mutate_node],
]
