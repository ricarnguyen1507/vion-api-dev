const excel = require("exceljs")
const db = require("#/services/tvcdb")
const incomeFragment =
`
uid
income_type
income_name
income.section{
  uid
  section.product
  quantity
}
income.pricing{
  uid
  cost_price_with_vat
}
income.user {
  uid
  user_name
}
created_at
notes
`
const orderXlsFieldNames = [
  "Thời gian/ngày tháng export",
  "Thời gian tạo đơn",
  "Mã đơn hàng",
  "Tên Người tạo",
  "Tên sản phẩm",
  "Giá vốn (No VAT)",
  "Giá bán (Có VAT)",
  "Thành tiền",
  "Ghi chú"
]

function buildRowData (fields, data, defaultValue) {
  const entries = fields.map(field => [field, data[field] ?? defaultValue])
  return Object.fromEntries(entries)
}

function parseTotalPay (sub_order) {
  let total = 0
  for(const item of sub_order.order_items) {
    const totalItemPrice = parseInt(item.quantity * item.sell_price)
    total += totalItemPrice
  }
  return total
}
const getDate = (timestamp = null) => {
  const date = timestamp ? new Date(timestamp).toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }) : new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  const resDate = date//`${day}/${month}/${year} ${hour}:${min}`
  return resDate
}
async function parseRow (fields, row, section,prodInfor) {
  // PREPARE DEFAULT DATA FOR ROW DATA
  const rowData = {}
  rowData["Thời gian/ngày tháng export"] = getDate()
  rowData["Thời gian tạo đơn"] = getDate(row?.created_at) ?? ''
  rowData["Mã đơn hàng"] = row?.income_name ?? ''
  rowData["Tên Người tạo"] = row['income.user']?.user_name ?? ''
  rowData["Tên sản phẩm"] = prodInfor[0]?.product_name ?? ''
  rowData["Giá vốn (No VAT)"] = prodInfor[0]?.cost_price ?? ''
  rowData["Giá bán (Có VAT)"] = parseInt(section?.quantity) * parseInt(prodInfor[0]?.cost_price) ?? ''
  rowData["Thành tiền"] = row?.['income.pricing']?.cost_price_with_vat ?? ''
  rowData["Ghi chú"] = row?.notes  ?? ''
  /**end get areas data */
  return buildRowData(fields, rowData, '')
}

async function getIncomes (filter) {
  const query = `{
    incomes as summary(func: type(Income)) @filter(not eq(order_status, 0) AND ${filter}) {
      totalCount: count(uid)
    }
    data(func: uid(incomes), orderdesc: created_at) {
      ${incomeFragment}
    }
  }`
  const ObjIncome = await db.query(query)
  return ObjIncome
}

async function exportIncome (request) {
  let { filter = '' } = request.query
  const { selectedDateFrom = '', selectedDateTo = '' } = request.query

  if (selectedDateFrom) {
    filter += `ge(created_at, ${selectedDateFrom})`
  }

  if (selectedDateTo) {
    filter += ` AND le(created_at, ${selectedDateTo})`
  }

  const { result } = await db.query(`{
    result(func: type(Income)) @filter(${filter}) {
      count(uid)
    }
  }`)
  const total = result[0]?.count
  if(!total) {
    throw { statusCode: 404, message: "Ngày được chọn ko có hoá đơn thanh toán" }
  }
  var workbook = new excel.Workbook();
  var worksheet = workbook.addWorksheet()
  worksheet.columns = orderXlsFieldNames.map(dataField => ({ header: dataField, key: dataField, width: 10 }))
  const orders = await getIncomes(filter)
  if(orders?.data?.length) {
    for(const row of orders.data) {
      if(row?.['income.section']?.length) {
        for(const section of row['income.section']) {
          if(section['section.product']){
            let {prodInfor} = await db.query(`{
              prodInfor(func: uid(${section['section.product']})) @filter(type(Product)) {
                uid
                product_name
                cost_price
              }
            }`)
            if(prodInfor?.length){
              worksheet.addRow(await parseRow(orderXlsFieldNames, row, section,prodInfor)).commit()
            }
          }
        }
      }
    }
  }
  return workbook.xlsx.writeBuffer()
}

module.exports = {
  exportIncome
}