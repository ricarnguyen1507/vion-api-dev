// const sendMail = require("#/services/notify_mailer")

const { exportIncome } = require("./export-income")

async function incomeExportHandler (request, reply) {
  log.info("Start export order")
  const fileName = `order_${Date.now()}.xlsx`

  const xls = await exportIncome(request)
  reply.headers({
    "Content-Type": "application/octet-stream; ; charset=utf-8",
    "Content-Disposition": `attachment; filename=${fileName}`
  })
  const res = reply.raw
  res.sent = true
  res.end(xls, 'binary')
}

module.exports = [
  ['get', '/income/income_export.xlsx', incomeExportHandler],
]