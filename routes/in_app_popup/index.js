"use strict"
const { saveMediaFiles } = require("#/modules/uploadFileHandle")

const imageFolder = "in_app_popup_images"
const db = require("#/services/tvcdb")

function getAll() {
  return db.query(`{ result(func: type(Popup)){
    uid
    popup_name
    show_by_user
    frequency_number
    frequency_time
    frequency_type
    popup_image
    popup_location
    popup_location_value
    popup_start_time
    popup_end_time
    popup_link_type
    customer_type
    popup_status
    popup_start_limit_time
    popup_end_limit_time
    list_date
    popup.platform {
      uid
      collection_name
    }
    popup.product {
      uid
      product_name
      sku_id
    }
    popup.collection {
      uid
      collection_name
    }
    popup.collection_temp {
      uid
      collection_name
    }
    popup.brandshop {
      uid
      brand_shop_name
    }
    popup.customer {
      uid
      customer_name
      phone_number
    }
    popup.landing_page {
      uid
      landing_name
    }
  }}`)
}

async function mutate(request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields, files } = request.body
  const { set, del } = fields
  if (del) {
    const delData = JSON.parse(del)
    await db.mutate({ del: delData })
  }

  if(set) {
    const setData = JSON.parse(set)
    const uid = await db.mutate({
      set: {
        uid: "_:new_popup",
        "dgraph.type": "Popup",
        ...setData
      }
    }).then(res => setData.uid || res.getUidsMap().get("new_popup"))
    if(Object.keys(files).length > 0) {
      const fields_files = saveMediaFiles(files, imageFolder)
      db.mutate({
        set: {
          uid,
          ...fields_files
        }
      })
    }

    return getAll()
  }
  return { statusCode: 200 }
}
module.exports = [
  ['get', '/in-app-popup', getAll],
  ['post', '/in-app-popup', mutate]
]