"use strict"
const db = require("#/services/tvcdb")

async function getlist ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(MegaMenu)", ...primaryFilters].join(', ') : "type(MegaMenu)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    megamenu as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(megamenu), first: $number, offset: $offset) {
      uid
      expand(_all_){
        uid
        collection_name
      }
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function mutate (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}
module.exports = [
  ["post", "/megamenu/list", getlist],
  ["post", "/megamenu/edit", mutate],
]
