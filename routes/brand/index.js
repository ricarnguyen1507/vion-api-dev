"use strict"
const db = require("#/services/tvcdb")

function getAll () {
  return db.query(`{ result(func: type(Brand)){uid,expand(_all_)} }`)
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Brand), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

async function getDataForTable ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Brand)", ...primaryFilters].join(', ') : "type(Brand)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    brands as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(brands), first: $number, offset: $offset) {
      uid
      expand(_all_)
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function updateBrand (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const mutateData = request.body.fields
  await db.mutate(mutateData)
  return { statusCode: 200 }
}

async function deleteBrand ({ params }) {
  if(params?.uid) {
    const $uid = params.uid
    if(db.isUid($uid)) {
      await db.mutate({ set: { uid: $uid, is_deleted: true } })
    } else {
      throw new Error("WTF")
    }
    return { statusCode: 200 }
  }
}

async function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Brand)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/brand', getAll],
  ['get', '/list/brand/:offset/:number', loadMore],
  ['post', '/list/brand', getDataForTable],
  ['post', '/brand', updateBrand],
  ['delete', '/brand/:uid', deleteBrand],
  ['get', '/brand/:uid', getByUid]
]