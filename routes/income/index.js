"use strict"

const db = require("#/services/tvcdb")

async function handleForm (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields } = request.body

  await db.mutate(fields)

  return { statusCode: 200 }
}

async function getAll () {
  const { result } = await db.query(`{
    result(func: type(Ads)) @filter(not eq(is_deleted, true)) {
      uid
      tracking_utm {
        uid
      }
      ads.group_customer {
        uid
      }
      ads_type
      is_active
      click
      link
      mime_type
      notes
      image
    }
  }`)
  return { result: result.reverse() }
}

async function listIncome ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Income)", ...primaryFilters].join(', ') : "type(Income)"
  const str2 = filters.length ? [...filters].join(" AND ") : ''

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    inc as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(inc), first: $number, offset: $offset) {
      uid
      income_name
      income_type
      income.product{
        uid
        product_name
        cost_price
      }
      income.pricing{
        uid
        cost_price_with_vat
      }
      income.user {
        uid
        user_name
      }
     created_at
     quantity
     notes
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
async function listIncomeV2 ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Income)", ...primaryFilters].join(', ') : "type(Income)"
  const str2 = filters.length ? [...filters].join(" AND ") : ''

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    inc as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(inc), first: $number, offset: $offset) {
      uid
      income_type
      income_name
      income.section{
        uid
        section.product
        quantity
      }
      income.pricing{
        uid
        cost_price_with_vat
      }
      income.user {
        uid
        user_name
      }
     created_at
     notes
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
async function setActive (request) {
  const { uid, is_active } = request.body
  await db.mutate({
    set: { uid, is_active }
  })
  return { statusCode: 200 }
}

async function setDelete (request) {
  const uid = request.params.uid
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  return { statusCode: 200 }
}

module.exports = [
  ['post', '/list/incomes', listIncome],
  ['get', '/income', getAll],
  ['post', '/income', handleForm],
  ['post', '/list/incomes_v2', listIncomeV2]
]