"use strict"
const db = require("#/services/tvcdb")

async function mutate ({ body }) {
  await db.mutate(body.fields)
  return { statusCode: 200 }
}
async function getTable ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Reason)", ...primaryFilters].join(', ') : "type(Reason)"
  const str2 = filters.length ? `@filter(${[...filters].join(" AND ")})` : ''

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    reasons as summary(func: ${str1}) ${str2}{
      total: count(uid)
    }
    data(func: uid(reasons), first: $number, offset: $offset) {
      uid,
      expand(_all_)
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
async function getAll() {
  const { result, summary } = await db.query(`{
    reasons as summary(func: type(Reason)) {
      totalCount: count(uid)
    }
    result(func: uid(reasons)){uid,expand(_all_)}
    }`)
  return { result, summary }
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Reason), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Reason)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['post', '/list/reason', getTable],
  ['get', '/list/reason', getAll],
  ['get', '/list/reason/:offset/:number', loadMore],
  ['get', '/reason/:uid', getByUid],
  ['post', '/reason', mutate]
]