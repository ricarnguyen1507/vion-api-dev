"use strict"
const db = require("#/services/tvcdb")

async function getAll ({ query }) {
  const { number = -1, page = 0 } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    musics as summary(func: type(Music)) {
      totalCount: count(uid)
    }
    result(func: uid(musics), orderasc: song_name${paginate}){uid,expand(_all_)}
  }`)
  return { result, summary }
}
async function findSongId (songId) {
  return db.query(`{ result(func: eq(song_id, "${songId}")){uid} }`);
}
async function mutate (request) {
  const { uid, song_id } = request.body.set;
  if (uid && uid === '_:new_music') {
    const res = await findSongId(song_id);
    if (res.result.length)
      throw new Error("Đã tồn tại id này!")
    else {
      return db.mutate(db.addTypeSet("Music", request.body)).then(result => db.getUids(result))
    }
  } else {
    return db.mutate(db.addTypeSet("Music", request.body)).then(result => db.getUids(result))
  }
}
module.exports = [
  ['get', '/music', getAll],
  ['post', '/music', mutate]
]