"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}
async function getAll ({ body }) {
  const { number = 20, page = 0 } = body
  const { result, summary } = await db.query(`query result($number: int, $offset: int){
      otts as summary(func: type(OTT)){
        totalCount: count(uid)
      }
      result(func: uid(otts), first: $number, offset: $offset){uid,expand(_all_)}
    }`, {
    $number: String(number),
    $offset: String(number * page)
  })
  return { result, summary }
}
async function getList () {
  const { result } = await db.query(`query {
      platforms as summary(func: type(OTT)) {
        totalCount: count(uid)
      }
      result(func: uid(platforms)){uid,expand(_all_)}
    }`,)
  return { result }
}

module.exports = [
  ['get', '/list/ott', getList],
  ['post', '/list/ott', getAll],
  ['post', '/ott', mutate]
]