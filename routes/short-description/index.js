"use strict"
const db = require("#/services/tvcdb")

const tableField = `
  uid
  display_name
  short_des
  short_des_color
  short_des_1
  short_des_1_color
  short_des_2
  short_des_2_color
  short_des_3
  short_des_3_color
  short_des_4
  short_des_4_color
  created_at
  updated_at
  display_status
  is_default
`

async function getFilter({ body }) {
  const { number = 20, page = 0, func = '', filter = '' } = body
  const funcStr = func !== '' ? `
      c as summary(${func}) @filter(type(ShortDescription) ${filter?.length ? 'AND ' + filter : ''}) {
        totalCount: count(uid)
      }` : `
      c as summary(func: type(ShortDescription)) @filter(${filter}) {
        totalCount: count(uid)
      }`
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    result(func: uid(c), first: $number, offset: $offset)  {
      ${tableField}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { result, summary }
}

async function mutate(request) {
  await db.mutate(request.body)
  return {
    statusCode: 200
  }
}

module.exports = [
  ["post", "/short-description-list", getFilter],
  ["post", "/short-description", mutate]
]
