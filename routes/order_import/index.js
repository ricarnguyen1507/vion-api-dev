const db = require("#/services/tvcdb")

async function checkImportUpdateUID (request) {
  const basicFields = JSON.parse(request.body.fields.dataNodeEdge);
  const availableObj = [];
  let limit = 0;
  if (basicFields?.length > 0) {
    while (basicFields.length > 0) {
      limit = (basicFields.length > 200) ? 199 : basicFields.length;
      const tmp = basicFields.splice(0, limit);
      try {
        for(const obj of tmp) {
          const objUid = await db.query(`{
            result(func: uid(${obj?.order_item_uid}))@filter(type(OrderItem)){
              uid
              product_name
            }
          }`)
          if (objUid?.result?.length > 0) {
            availableObj.push(obj)
          }
        }
      }
      catch (err) {
        log.error(err);
      }
    }
  }
  log.info('End check UID', availableObj.length, new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }))
  return availableObj
}
async function importUpdateOrder ({ body }) {
  const txn = db.txn()
  const basicFields = JSON.parse(body.fields.dataUpdated);
  if (basicFields?.length > 0) {
    let chunk
    try {
      while(chunk = basicFields.splice(0, 200), chunk.length > 0) {
        const dataImport = []
        for(const order of chunk) {
          const dataChange = {
            'dgraph.type': "OrderItem",
            uid: order.order_item_uid,
            promotion_detail: order.promotion_detail
          }
          dataImport.push(dataChange)
        }
        await db.mutate({ set: [...dataImport] }, txn)
      }
    } catch (error) {
      log.error(error)
    }

    await txn.commit()
  }
}
module.exports = [
  ['post', '/order/checkUID', checkImportUpdateUID],
  ['post', '/order/import', importUpdateOrder]
]