"use strict"
const db = require("#/services/tvcdb")

function getAll() {
  return db.query(`{
    highlight_cat(func: eq(collection_type, 200)) {
      v as uid
    }
    products(func: type(Product)) @normalize {
      uid: uid
      product_name: product_name
      image_promotion: image_promotion
      image_banner: image_banner
      image_cover: image_cover
      image_highlight: image_highlight
      product.collection @filter(uid(v)) {
        highlight: count(uid)
      }
    }
  }`)
}

function mutate(request) {
  return db.mutate(db.addTypeSet("Product", request.body)).then(() => "OK")
}

module.exports = [
  ['get', '/list/highlight', getAll],
  ['post', '/highlight', mutate]
]