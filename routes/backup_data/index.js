"use strict"
const emitRecoveryDB = require("#/services/backup-data/recovery_db")
const emitDumpDB = require("#/services/backup-data/dump_db")
const updatePartnerToSubOrder = require("#/services/sync-partner/order")

async function startDump ({ params }) {
  if (!params) {
    throw Error("What are you doing here?")
  }
  let { type } = params
  if (!params.type) {
    type = "all"
  }
  const data = await emitDumpDB(type)
  return {
    statusCode: 200,
    data
  }
}
async function startRecovery ({ params }) {
  if (!params) {
    throw Error("What are you doing here?")
  }
  let { type } = params
  if (!params.type) {
    type = "all"
  }
  const data = await emitRecoveryDB(type)
  return {
    statusCode: 200,
    data
  }
}

async function clonePartnerToSubOrder () {
  return await updatePartnerToSubOrder()
}

module.exports = [
  ['get', '/backup_data/:type', startDump],
  ['get', '/recovery_data/:type', startRecovery],
  ['get', '/updatePartnerToSubOrder', clonePartnerToSubOrder],
]