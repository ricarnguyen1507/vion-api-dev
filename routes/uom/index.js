"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}
async function getTable ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(UOM)", ...primaryFilters].join(', ') : "type(UOM)"
  const str2 = filters.length ? `@filter(${[...filters]})` : ''

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    uoms as summary(func: ${str1}) ${str2}{
      total: count(uid)
    }
    data(func: uid(uoms), first: $number, offset: $offset) {
      uid,
      expand(_all_)
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
async function getAll () {
  const { result } = await db.query(`{
    result(func: type(UOM)){
      uid,
      expand(_all_)}
    }`)
  return { result }
}
module.exports = [
  ['post', '/list/uom', getTable],
  ['get', '/list/uom', getAll],
  ['post', '/uom', mutate]
]