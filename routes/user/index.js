"use strict"
const db = require("#/services/tvcdb")
const session = require("#/modules/userSession")

async function updateUser (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const mutateData = request.body.fields
  await db.mutate(mutateData)
  return { statusCode: 200 }
}

async function deleteUser ({ params }) {
  if(params?.uid && db.isUid(params?.uid)) {
    // log.debug("delete user", params.uid)
    await db.mutate({ set: { uid: params.uid, is_deleted: true } })
  } else {
    throw new Error("WTF")
  }
  return { statusCode: 200 }
}

async function listUser ({ body }) {
  // Update lại những user cũ , tự động thêm data khi load table
  await db.upsert({
    query: `{
      var(func: type(RolePortal)) @filter(eq(role_portal_code, "OMNI") AND NOT eq(is_deleted,true)) {
        rp as uid
      }

      var(func: type(CmsUser)) @filter(NOT has(user.role_portal) AND NOT eq(is_deleted,true)) {
        ro as uid
      }
    }`,
    cond: "@if(gt(len(ro), 0))",
    set: [{
      uid: "uid(ro)",
      ['user.role_portal']: {
        uid: "uid(rp)"
      }
    }]
  });
  // Query RolePortal => query user thiếu fields => add thêm default
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(CmsUser)", ...primaryFilters].join(', ') : "type(CmsUser)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    users as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(users), first: $number, offset: $offset, orderasc: function_name) {
      uid
      user_name
      phone_number
      email
      role {
        uid
        role_name
      }
      user.role_portal {
        uid
        expand(_all_)
      }
      user.ott {
        uid
        expand(_all_)
      }
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function getUser ({ params }) {
  if(!db.isUid(params.uid)) {
    throw new Error(`wrong uid(${params.uid})`)
  }
  return await db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(CmsUser)){
      uid
      expand(_all_){
        uid
        expand(_all_)
      }
    }
  }`, { $uid: params.uid })
}

async function checkUser (request) {
  return await session.getUser(request)
}

//#region login
const login_route = {
  schema: {
    body: {
      required: ['$username', '$password'],
      properties: {
        $username: { type: 'string' },
        $password: { type: 'string' }
      }
    }
  },
  async handler(req) {
    const { login: [login] } = await db.query(`query login($username: string, $password: string) {
      login(func: eq(user_name, $username)) @filter(type(CmsUser)) {
        uid
        auth: checkpwd(password, $password)
        phone_number
        role {
          role_name
          role.role_function {
            expand(_all_)
          }
        }
        checkPortal: user.role_portal {
          expand(_all_)
        }
      }
    }`, req.body)

    if(login?.auth === true) {
      const { uid, checkPortal = {}, role, phone_number = "" } = login
        session.setUser(req, {
          uid,
          user_name: req.body.$username,
          role: role.role_name, // Lấy để check quyền dữ liệu cũ - ko bị lỗi với code cũ
          permission: role?.['role.role_function'] || [], // Lưu để check quyền 0 - 1 - 2
          phone_number
        })
        return { statusCode: 200 }
    }

    log.info(`User ${req.body.$username} login failed`)

    return { statusCode: 401 }
  }
}
//#endregion login

async function Logout (request) {
  session.deleteUser(request)
  return 'logged out'
}

//#region changePassword
const check_password_query = `query result($uid: string, $password_old: string){
  result(func: uid($uid))  {
    isAuthenticated: checkpwd(password, $password_old)
  }
}`

async function ChangePassword (request) {
  const { $uid, $password_old, $password_new } = request.body
  if(!($password_old && $password_new && $uid)) {
    throw new Error("What are you doing here?")
  }

  if(await session.getUser(request)) {
    const { result } = await db.query(check_password_query, { $uid, $password_old }) // Check password old
    if(result && result[0]?.isAuthenticated) {
      await db.mutate({ set: { uid: $uid, password: $password_new } })
      return { statusCode: 200 }
    }
    else{
      return { statusCode: 401 }
    }
  }
}

async function getOptions () {
  return await db.query(`{
    result(func: type(CmsUser)) {
      uid
      user_name
    }
  }`)
}
//#endregion changePassword

module.exports = [
  ['get', '/cmsUser/get-option', getOptions],
  ['post', '/list/user', listUser],
  ['get', '/user/:uid', getUser],
  ['delete', '/user/:uid', deleteUser],
  ['post', '/user', updateUser],
  ['post', '/user/login', login_route],
  ['get', '/user/check', checkUser],
  ['get', '/user/logout', Logout],
  ['post', '/user/password/change', ChangePassword]
]