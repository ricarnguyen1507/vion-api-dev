"use strict"
const db = require("#/services/tvcdb")

async function mutate ({ body }) {
  if(body.set) {
    const { address, alt_address } = body.set
    if(address) {
      db.addType("Address", address)
    }
    if(alt_address) {
      alt_address.forEach(addr => {
        db.addType("Address", addr)
      })
    }
    db.addType("Warehouse", body.set)
  }

  const result = await db.mutate(body)

  return db.getUids(result, { statusCode: 200 })
}

async function deleteWarehouse ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  // process.emit('sync', uid, "warehouse")
  return "success"
}

const fields = `
  uid
  expand(_all_) {
    uid
    expand(_all_) {
      uid
      expand(_all_)
    }
  }
`

async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`

  const { result, summary } = await db.query(`{
    warehouses as summary(func: type(Warehouse)) @filter(NOT eq(is_deleted, true)${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(warehouses) ${paginate}){${fields}}
    }`)
  return { result, summary }
}

function loadMore (request) {
  const { $offset = 20, $number = 0 } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Warehouse), first: $number, offset: $offset) {${fields}}
  }`, { $number, $offset })
}

function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Warehouse)) {${fields}}
  }`, { $uid })
}
async function getOptions () {
  const query = `{
    result(func: type(Warehouse)) @filter(not eq(is_deleted, true)) {
        uid
        name
        store_code
      }
    }`

  return db.query(query)
}

module.exports = [
  ['delete', '/warehouse/:uid', deleteWarehouse],
  ['get', '/list/warehouse', getAll],
  ['get', '/list/warehouse/:offset/:number', loadMore],
  ['get', '/warehouse/:uid', getByUid],
  ['post', '/warehouse', mutate],
  ['get', '/list/warehouse-options', getOptions]
]