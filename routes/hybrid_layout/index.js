"use strict"
const db = require("#/services/tvcdb")
const layoutField = require('./layoutField')
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'layout_section')
    }
  }
  return { statusCode: 200 }
}
async function getAll ({ query }) {
  const { brand_shop_uid } = query
  const { result, summary } = await db.query(`query result{
    q(func: uid(${brand_shop_uid})) @filter( type(BrandShop) AND has(~hybrid_layout.brand_shop)) {
      hybrid as summary: ~hybrid_layout.brand_shop{
         totalCount: count(uid)
      }
    }
    result(func: uid(hybrid)) {
      uid
      layout_name
    }
  }`)
  return { result, summary }
}
async function getDataTable ({ body }) {
  const { number, page, filter = '' } = body.params
  const { result, summary } = await db.query(`query result($number: int, $offset: int){
      hybrid as summary(func: type(HybridLayout), orderasc: display_order) @filter(not eq(is_deleted, true) ${filter}) {
        totalCount: count(uid)
      }
      result(func: uid(hybrid), first: $number, offset: $offset) {
        ${layoutField}
      }
    }`, {
    $number: String(number),
    $offset: String(number * page)
  })
  return { result: db.parseFacetArray(result), summary }
}
function getOption () {
  return db.query(`{
    result(func: type(HybridLayout), orderasc: display_order) @filter(NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      layout_name
    }
  }`)
}

async function setDelete ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const { result } = await db.query(`{
    result(func: uid(${uid})) @filter(eq(is_default, true) AND NOT eq(is_deleted, true))  {
      check: count(uid)
    }
  }`)

  if(result?.length) {
    return { statusCode: 400, message: "Không được xoá HybridLayout mặc định" }
  }

  return db.mutate({ set: { uid, is_deleted: true } }).then(() => ({ statusCode: 200 }))
}

module.exports = [
  ['post', '/list/hybrid_layout', getDataTable],
  ['get', '/list/hybrid_layout', getAll],
  ['get', '/list/hybrid_layout_options', getOption],
  ['delete', '/hybrid_layout/:uid', setDelete],
  ['post', '/hybrid_layout', mutate]
]