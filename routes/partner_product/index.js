"use strict"

const db = require("#/services/tvcdb")
const { change_alias } = require("#/modules/db-import-product")
const { saveMediaFile } = require("#/modules/uploadFileHandle")
const prodPartnerFields = require('./productPartnerFileds')

async function mutateProductPartner (request) {
  const txn = db.txn()
  const { fields, files } = request.body
  let newProductUid = request.params.uid;
  let basicFields = JSON.parse(fields.data)
  console.log('basicFields', basicFields)
  if (newProductUid == 0) {
    if (!basicFields.name || !basicFields.sku || !basicFields.price || !basicFields.description || !basicFields.weight || !basicFields.height || !basicFields.width || !basicFields.unit_id || !basicFields.attributes || !basicFields.voucher) {
      throw { statusCode: 400, message: "Lack of Product Information" }
    }
    const res = await db.mutate({
      set: {
        uid: "_:new_product",
        "dgraph.type": "ProductRaw",
      }
    })
    newProductUid = res.getUidsMap().get('new_product')
  } else {
    const { result: [product] } = await db.query(`{
          result(func: uid(${basicFields.uid})) @filter(type(ProductRaw)){
            uid
            dgraph.type
          }
       }`)
    if (!product) {
      throw { statusCode: 400, message: "Product not exist" }
    }
  }
  const avatar = {}
  const pictures = []
  console.log('files', files)
  for(const multipart_field in files) {
    const file = files[multipart_field]
    if (multipart_field.startsWith('pictures')) {
      const [newFilePath] = saveMediaFile(file, multipart_field, `partner/pictures/products/${newProductUid}`)
      const avatars = {};
      avatars.picture_url = newFilePath
      pictures.push(avatars)

    } else if (multipart_field.startsWith('files')) {
      const [newFilePath] = saveMediaFile(file, multipart_field, `partner/certificate/${newProductUid}`)
      console.log('newFilePath', newFilePath)
      basicFields.certificate_file = newFilePath
    } else {
      const [newFilePath] = saveMediaFile(file, multipart_field, `partner/avatar/products/${newProductUid}`)
      avatar.picture_url = newFilePath
      basicFields.avatar = avatar
    }
  }

  const fullTextSearch = [
    basicFields.name, change_alias(basicFields.name),
    basicFields.sku, change_alias(basicFields.sku),
    newProductUid, change_alias(newProductUid),
  ]
  basicFields.pictures = pictures.length > 0 ? pictures : []
  if (basicFields.avatar) {
    db.addType("Avatar", basicFields.avatar);
  }
  if (basicFields.pictures) {
    basicFields.pictures.forEach(ava => {
      db.addType("Avatar", ava);
    })
  }

  if (basicFields.extended_shipping_package) {
    db.addType("ExtendedShipping", basicFields.extended_shipping_package);
  }
  if (basicFields.voucher) {
    basicFields.voucher.start_date = basicFields.voucher.start_date ? Date.parse(basicFields.voucher.start_date) : 0
    basicFields.voucher.end_date = basicFields.voucher.end_date ? Date.parse(basicFields.voucher.end_date) : 0
    basicFields.voucher.is_deleted = false
    db.addType("Voucher", basicFields.voucher);
  }
  if (basicFields.variants) {
    basicFields.variants.forEach(variant => {
      db.addType("Variant", variant);
    })
  }
  basicFields.uid = newProductUid
  if (!basicFields.promotion_from_date) {
    basicFields.promotion_from_date = new Date()
  }
  basicFields.promotion_from_date = basicFields.promotion_from_date ? Date.parse(basicFields.promotion_from_date) : 0
  basicFields.promotion_to_date = basicFields.promotion_to_date ? Date.parse(basicFields.promotion_to_date) : 0
  basicFields.is_deleted = false
  basicFields.fulltext_search = fullTextSearch.join(' ')
  const mutateData = {
    "dgraph.type": "ProductRaw",
    ...basicFields
  }
  console.log('data prepare to add', mutateData)
  await db.mutate({ set: mutateData }, txn)

  // PREPARE DATA ATTRIBUTE FACETS
  if (basicFields.attributes.length > 0) {
    const mutateAttribute = {};
    let strBody = JSON.stringify(basicFields);
    strBody = strBody.replace(/"attributes":/g, "\"attribute_name\":")
    basicFields = JSON.parse(strBody)
    mutateAttribute.attribute_name = [];
    for (let i = 0; i < basicFields.attribute_name.length; i++) {
      if (!basicFields.attribute_name[i].attribute_name && !basicFields.attribute_name.value) {
        throw { statusCode: 400, message: "Lack of Attribute information" }
      }
      // CREATE ATTRIBUTE CODE
      const alias = change_alias(basicFields.attribute_name[i].attribute_name)
      basicFields.attribute_name[i].attribute_code = alias.split(' ').join('_')
      // END CREATE ATTRIBUTE CODE

      mutateAttribute.attribute_name = basicFields.attribute_name[i].attribute_name
      mutateAttribute['attribute_name|attribute_code'] = basicFields.attribute_name[i].attribute_code
      mutateAttribute['attribute_name|value'] = basicFields.attribute_name[i].value
      mutateAttribute.uid = newProductUid
      await db.mutate({ set: mutateAttribute }, txn)
    }
  }
  // END PREPARE DATA ATTRIBUTE FACETS
  await txn.commit()
  return {
    "result": 132465889,
    "success": true,
    "error": {
      "message": "string",
      "details": "string",
      "error_details": [
        {
          "property_name": "string",
          "message": "string",
          "value": {},
          "code": "string"
        }
      ]
    },
    "status_code": 200
  }
}

function setDelete ({ params: { uid } }) {
  if(uid) {
    return db.mutate({ set: { uid, is_deleted: true } }).then(() =>
      // process.emit('sync', uid, 'product')
      ({ statusCode: 200 })
    )
  }
}

// get /list/product/:number/:offset
function loadMore (request) {
  const { offset, number, keywords } = request.params
  const search = keywords === 'all' ? '' : ` and anyofterms(name, "${keywords}")`
  return db.query(`query result($number: string, $offset: string) {
    products as summary(func: type(ProductRaw)) @filter(not eq(is_deleted, true)${search}) {
      totalCount: count(uid)
    }
    data(func: uid(products), orderdesc: created_at, first: $number, offset: $offset) {
      uid
      expand(_all_){
        uid
        expand(_all_){
          uid
          expand(_all_)
        }
      }
    }
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

// get /product/:uid
function getByUid (request) {
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(ProductRaw)){
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_)
        }
      }
    }
  }`, { $uid: request.params.uid })
    .then(({ result: [product = {}] }) => product)
}


async function productFilter ({ body }) {
  const { number = 20, page = 0, filter = '', func = '' } = body
  console.log(body)
  const { data, summary } = await db.query(`query result($number: int, $offset: int) {
    ${func !== ''
    ? `products as summary(${func}) @filter(type(ProductRaw) ${filter}) {
        totalCount: count(uid)
    }`
    :
    `products as summary(func: type(ProductRaw)) @filter(${filter}) {
        totalCount: count(uid)
    }`}
    data(func: uid(products), orderdesc: created_at, first: $number, offset: $offset) @filter(not eq(is_deleted, true)){
        ${prodPartnerFields}
    }
    }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { data: db.parseFacetArray(data), summary }
}
// FUCNTION EXPORT DATA ERROR FROM EXCEL IMPORT FILE


module.exports = [
  ['post', '/list/partner/product', productFilter],
  ['get', '/list/partner/product/:number/:offset/:keywords', loadMore],
  ['get', '/product/partner/:uid', getByUid],
  ['post', '/product/partner/:uid', mutateProductPartner],
  ['delete', '/product/partner/:uid', setDelete]
]