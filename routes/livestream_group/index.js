"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'livestream_group')
    }
  }
  return { statusCode: 200 }
}


const fields = `
  uid
  livestream_group_name
  livestream_group.videostreams @facets(orderasc: display_order) {
    uid
    stream.name
    stream.display_name
  }
  livestream_group.brand_shop {
    uid
    brand_shop_name
  }
  display_status
  layout_type
`
async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    livestream_group as summary(func: type(LivestreamGroup)) @filter(NOT eq(is_deleted, true) ${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(livestream_group)${paginate}){${fields}}
  }`)
  return { result: db.parseFacetArray(result), summary }
}

async function getOption ({ query: { brand_shop_uid } }) {
  let query = `{
    result(func: type(LivestreamGroup)) @filter(NOT has(livestream_group.brand_shop) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      livestream_group_name
    }
  }`
  if (brand_shop_uid && db.isUid(brand_shop_uid)) {
    query = `{
      var(func: uid(${brand_shop_uid})) {
        ~livestream_group.brand_shop @filter(eq(display_status, 2)) {
          u as uid
        }
      }
      result(func: uid(u)) @filter( type(LivestreamGroup) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
        uid
        livestream_group_name
      }
    }`
  }
  return db.query(query)
}

async function getOptionAvailable ({ query: { brand_shop_uid } }) {
  let query = `{
    result(func: type(VideoStream) , first: 100, offset: 0) @filter(NOT has(~brand_shop.video_stream) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      stream.name
    }
  }`
  if (brand_shop_uid && db.isUid(brand_shop_uid)) {
    query = `{
      var(func: uid(${brand_shop_uid})) {
        brand_shop.video_stream @filter(eq(display_status, 2)) {
          u as uid
        }
      }
      result(func: uid(u) , first: 100, offset: 0) @filter(type(VideoStream) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
        uid
        stream.name
      }
    }`
  }
  return db.query(query)
}

module.exports = [
  ['get', '/list/livestream_group', getAll],
  ['post', '/livestream_group', mutate],
  ['get', '/list/livestream_group/options', getOption],
  ['get', '/list/livestream_group_options/available', getOptionAvailable],
]