const {
  WebP,
  MozJPEG,
  Steps,
  FromFile,
  FromStream
} = require('@imazen/imageflow')

const { stat } = require('fs')

const path = require('path')

const { image_dir, public_dir } = config

function get_encoder(name) {
  switch (name) {
    case "png":
    case "webp":
      return new WebP(80)
    default:
      return new MozJPEG(80)
  }
}

function processImage ({ query, url, headers }, reply) {
  const qm = url.indexOf("?")
  const uri = qm === -1 ? url : url.slice(0, qm)
  let img_path = path.join(image_dir, ...uri.split("/"))

  stat(img_path, (err, stats) => {
    if(err) {
      reply.code(404)
      img_path = path.join(public_dir, "static", "404.webp")
    }

    const res = reply.raw

    if(stats) {
      const img_mtime = stats.mtime.toUTCString()
      if ('if-modified-since' in headers && headers['if-modified-since'] === img_mtime) {
        return reply.code(304).send()
      }
      res.setHeader('Cache-Control', 'public, max-age=3600')
      res.setHeader('Last-Modified', img_mtime)
    }

    res.setHeader('Content-Type', 'image')

    const steps = new Steps(new FromFile(img_path))

    if('w' in query || 'h' in query) {
      steps.constrainWithin(query.w, query.h)
    }
    const ext = img_path.slice(img_path.lastIndexOf(".") + 1)
    steps
      .encode(new FromStream(res), get_encoder(ext))
      .execute()
      .then(() => {
        res.end()
      })
      .catch(err => {
        console.log(err)
        reply.code(500).send("Sorry, server got error")
      })
  })
}

const schema = {
  query: {
    properties: {
      w: { type: ['number', 'null'] },
      h: { type: ['number', 'null'] }
    }
  }
}


module.exports = {
  schema,
  method: 'GET',
  url: '/images/*',
  handler: processImage
}