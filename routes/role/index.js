"use strict"
const db = require("#/services/tvcdb")

async function getlist ({ body }) {

  // ==================== Thêm dữ liệu cho RolePortal ====================
  // Check RolePortal - nếu chưa có tạo mới
  await db.upsert({
    query: `{
      ckp as var (func: type(RolePortal)){
        uid
      }
    }`,
    cond: "@if(eq(len(ckp), 0))",
    set: [
      {
        uid: '_:new_portal_omni',
        "dgraph.type": "RolePortal",
        role_portal_name: "Omnishop",
        role_portal_code: "OMNI"
      },
      {
        uid: '_:new_portal_ott',
        "dgraph.type": "RolePortal",
        role_portal_name: "Đối tác OTT",
        role_portal_code: "OTT"
      }
    ]
  });

  // Check role.role_portal nếu > 0 => add default là : OMNI
  await db.upsert({
    query: `{
      var(func: type(RolePortal)) @filter(eq(role_portal_code, "OMNI") AND NOT eq(is_deleted,true)) {
        rp as uid
      }

      var(func: type(Role)) @filter(NOT has(role.role_portal) AND NOT eq(is_deleted,true)) {
        ro as uid
      }
    }`,
    cond: "@if(gt(len(ro), 0))",
    set: [{
      uid: "uid(ro)",
      ['role.role_portal']: {
        uid: "uid(rp)"
      }
    }]
  });
  // ============================================================

  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Role)", ...primaryFilters].join(', ') : "type(Role)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
      role as summary(func: ${str1}) @filter(${str2}) {
        total: count(uid)
      }
      data(func: uid(role), first: $number, offset: $offset) {
        uid
        expand(_all_)(orderasc: function_name){
          uid
          expand(_all_)
        }
      }
    }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function mutate (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}

function getAll() {
  return db.query(`{
    result(func: type(Role)) {
      uid
      role_name
      role.role_portal{
        uid
        expand(_all_)
      }
    }
  }`)
}

function loadMore(request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Role), first: $number, offset: $offset) {
      uid
      role_name
    }
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

function getByUid(request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Role)) {
      uid
      role_name
      role.role_function (orderasc: function_name) @filter(not eq(is_deleted, true)) {
        uid
        expand(_all_)
      }
    }
  }`, { $uid })
}

async function getListRolePortal() {
  return db.query(`{
    result(func: type(RolePortal)) {
      uid
      expand(_all_)
    }
  }`)
}

module.exports = [
  ['get', '/list/role', getAll],
  ['get', '/list/role/:number/:offset', loadMore],
  ['get', '/role/:uid', getByUid],
  ['post', '/role/list', getlist],
  ['post', '/role/edit', mutate],
  ['get', '/role_portal/list', getListRolePortal]
]