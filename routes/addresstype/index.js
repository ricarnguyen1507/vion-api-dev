"use strict"
const db = require("#/services/tvcdb")

function mutate ({ body }) {
  return db.mutate(db.addTypeSet("AddressType", body)).then(result => db.getUids(result))
}
async function getAll ({ query }) {
  const { number = -1, page = 0 } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    adTypes as summary(func: type(AddressType)){
      totalCount: count(uid)
    }
    result(func: uid(adTypes) ${paginate}){
      uid,
      expand(_all_)}
    }`)
  return { result, summary }
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(AddressType), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(AddressType)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/addresstype', getAll],
  ['get', '/list/addresstype/:offset/:number', loadMore],
  ['get', '/addresstype/:uid', getByUid],
  ['post', '/addresstype', mutate]
]