"use strict"

const fs = require('fs')
const path = require('path')
const db = require("#/services/tvcdb")
const session = require("#/modules/userSession")
const { image_dir } = config
const { parsesPricing } = require('./pricing')
const { saveMediaFile } = require("#/modules/uploadFileHandle")
const prodFields = require('./productFileds')
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutateProduct (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  // Tạo thư mục mới với tên random lưu image
  // Lưu image theo tên field
  // Cập nhật product để lấy uid
  // Di chuyển thư mục image tạm qua thư mục images/[uid]
  let uid = request.params.uid
  const { fields, files } = request.body

  const res = await db.mutate(fields)
  let addAction = true
  if (db.isUid(uid)) {
    addAction = false
  }
  uid = uid.startsWith('_:') ? res.getUidsMap().get('new_product') : uid

  await hanleMedia(fields, files, uid)

  return { statusCode: 200, uid }
}
function setDelete ({ params: { uid } }) {
  if(uid) {
    return db.mutate({ set: { uid, is_deleted: true } }).then(() =>
      // process.emit('sync', uid, 'product')
      ({ statusCode: 200 })
    )
  }
}

async function hanleMedia (fields, files, product_uid) {
  const previews = {}
  const videoTrancodes = {}
  if(Object.entries(files).length) {
    for(const multipart_field in files) {
      const file = files[multipart_field]
      if(multipart_field.startsWith('previews')) {
        const [pred, field, puid, oldFile] = multipart_field.split('|')
        if(!previews[puid]) {
          previews[puid] = {}
        }
        if(oldFile) {
          try {
            fs.unlinkSync(path.join(image_dir, oldFile))
          } catch(err) {
            log.error(err)
          }
        }
        const [newFilePath, media_type] = saveMediaFile(file, field, `products/${product_uid}`)
        if(field === 'source') {
          previews[puid]['media_type'] = media_type
        }
        previews[puid][field] = newFilePath
      } else if (multipart_field.startsWith('video_transcodes')) {
        const [pred, field, puid, oldFile] = multipart_field.split('|')
        if(!videoTrancodes[puid]) {
          videoTrancodes[puid] = {}
        }
        if(oldFile) {
          try {
            fs.unlinkSync(path.join(image_dir, oldFile))
          } catch(err) {
            log.error(err)
          }
        }
        const [newFilePath, media_type] = saveMediaFile(file, field, `products/${product_uid}`)
        if(field === 'source') {
          videoTrancodes[puid]['media_type'] = "video"
        }
        videoTrancodes[puid][field] = newFilePath

      } else {
        saveMediaMD5([multipart_field, file], 'products')
      }
    }
  }
  if(Object.keys(fields).length) {
    Object.entries(fields).forEach(([key, val]) => {
      if (key.startsWith('previews|square')) {
        const [pred, field_name, puid] = key.split('|')
        if (previews?.[puid]) {
          previews[puid] = { ...previews[puid], 'square': val }
        } else {
          previews[puid] = {
            uid: puid,
            square: val
          }
        }
      }
      if (key.startsWith('video_transcodes|source')) {
        const expression = /(https?:)/g;
        const valueString = (val !== "" && !val.match(expression)) ? `https://tvcommerce-st.fptplay.net/prod/videos/products/encoded/${val}/index.m3u8` : val
        const [pred, field_name, puid] = key.split('|')
        if (videoTrancodes?.[puid]) {
          videoTrancodes[puid] = { ...videoTrancodes[puid], source: valueString, media_type: 'video' }
        } else {
          videoTrancodes[puid] = {
            uid: puid,
            source: valueString,
            media_type: "video"
          }
        }
      }
    })
  }
  const TypedPreviews = db.addType("Media", Object.entries(previews).map(([k, v]) => ({ uid: k, ...v })))
  const TypedVideoTranscodes = db.addType("Media", Object.entries(videoTrancodes).map(([k, v]) => ({ uid: k, ...v })))
  await db.mutate({ set: {
    "dgraph.type": "Product",
    uid: product_uid,
    previews: TypedPreviews,
    video_transcodes: TypedVideoTranscodes,
  } })
  return { statusCode: 200 }
}
// get /list/product/:number/:offset
function loadMore (request) {
  const { offset, number, keywords } = request.params
  const search = keywords === 'all' ? '' : ` and anyofterms(product_name, "${keywords}")`
  return db.query(`query result($number: string, $offset: string) {
    products as summary(func: type(Product)) @filter(not eq(is_deleted, true)${search}) {
      totalCount: count(uid)
    }
    data(func: uid(products), orderdesc: created_at, first: $number, offset: $offset) {
      uid
      expand(_all_){
        uid
        expand(_all_){
          uid
          expand(_all_)
        }
      }
    }
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

// get /product/:uid
function getByUid (request) {
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Product)){
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_)
        }
      }
    }
  }`, { $uid: request.params.uid })
    .then(({ result: [product = {}] }) => product)
}


async function updateAreas () {
  const products = await db.query(`{
    products(func:type(Product)) @filter(has(area)) {
      uid
      product_name
      area
    }
  }`).then(({ products }) => products)

  products.map(product => {
    const arrArea = product.area.split(',')
    arrArea.map(area => {
      db.query(`{
        search(func:alloftext(name, "${area}")) @filter(type(Region)) {
          uid
          name
        }
      }`).then(({ search }) => {
        if (search && search.length > 0) {
          const mutateData = {
            set: {
              uid: product.uid,
              areas: {
                uid: search[0].uid || null
              }
            }
          }
          db.mutate(mutateData)
        }
      }).catch(err => {
        log.error(err)
      })
    })
  })

  return { statusCode: 200 }
}

async function updateDetail () {
  const { products } = await db.query(`{
    products(func:type(Product)) @filter(has(details)) {
      uid
      details (first: 1) {
        uid
        content
        title
        icon {
          uid
          expand(_all_)
        }
      }
    }
  }`)

  products.map(product => {
    const mutateData = {
      set: {
        uid: product.uid,
        detail: {
          'dgraph.type': 'Detail',
          title: product.details[0].title,
          content: product.details[0].content,
          'detail.icon': {
            uid: product.details[0].icon && product.details[0].icon.uid || null
          }
        }
      }
    }
    db.mutate(mutateData)
      .then(res => {
        log.debug(res)
      })
      .catch(err => {
        log.error(err)
      })
  })
}

async function getOptions (request) {
  if(!request.body) {
    throw new Error('What are you doing here?')
  }
  const { fulltext_search, collection_uid, brand_shop_uid } = request.body
  let query = `query qr($text: string) {
    result(func: alloftext(fulltext_search, $text) , first: 50, offset: 0) @filter(type(Product) AND not eq(is_deleted, true) AND (eq(display_status, 2) OR eq(display_status, 3))) {
      uid
      product_name
      sku_id
      cost_price
    }
  }`
  if (brand_shop_uid) {
    query = `query qr($text: string) {
      var(func: uid(${brand_shop_uid})) {
        brand_shop.product @filter(NOT eq(is_deleted, true) AND eq(display_status, 2) AND NOT eq(is_temporaty, true)) {
          p as uid
        }
      }
      result(func: alloftext(fulltext_search, $text) , first: 50, offset: 0) @filter(uid(p) AND type(Product) AND not eq(is_deleted, true) AND eq(display_status, 2)) {
        uid
        product_name
        sku_id
        cost_price
      }
    }`
  }
  if (collection_uid) {
    query = `query qr($text: string) {
      var(func: uid(${collection_uid})) {
        c as uid
        ~parent {
          cc as uid
        }
      }
      var(func: uid(c,cc)) @filter(type(Collection) AND eq(display_status, 2) AND not eq(is_deleted, true)) {
        ~product.collection {
          p as uid
        }
      }
      result(func: alloftext(fulltext_search, $text) , first: 50, offset: 0) @filter(uid(p) AND type(Product) AND not eq(is_deleted, true) AND eq(display_status, 2)) {
        uid
        product_name
        sku_id
        cost_price
      }
    }`
  }

  return await db.query(query, { $text: fulltext_search })
}

function change_alias (str) {
  return !str ? '' : str.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/đ/g, 'd').replace(/Đ/g, 'D');
}

async function createSearchData () {
  let { result: [{ count }] } = await db.query(`{
      result(func: type(Product)) {
          count(uid)
      }
  }`)

  const limit = 200
  let offset = 0

  if (count > 0) {
    while(count > 0) {
      const products = await db.query(`{
          result(func: type(Product), first: ${limit}, offset: ${offset}) {
            uid
            product_name
            display_name
            display_name_detail
            keywords
            sku_id
            product.pricing (first: -1) {
              uid
              price_with_vat
            }
          }
      }`).then(res => res.result)

      const fullTextSearch = []
      await products.forEach(data => {
        fullTextSearch.push(
          { set: {
            uid: data.uid,
            fulltext_search: [
              data.product_name, change_alias(data.product_name),
              data.display_name, change_alias(data.display_name),
              data.display_name_detail, change_alias(data.display_name_detail),
              data.keywords, change_alias(data.keywords),
              data.sku_id, change_alias(data.sku_id),
              data.uid, change_alias(data.uid),
            ].join(' '),
            'price_with_vat': data?.['product.pricing']?.['price_with_vat'] || 0
          } })
      })
      // console.log('fullTextSearch', fullTextSearch);
      await db.mutates(fullTextSearch)

      if (count > limit) {
        count -= limit
        offset += limit
      } else {
        count -= count
      }
    }

    return {
      statusCode: 200,
      message: `Update successfully`
    }
  } else {
    return {
      statusCode: 200,
      message: `No record found!`
    }
  }
}
async function productFilter ({ body }) {
  const { number = 20, page = 0, is_temporary = null, filter = '', func = '', typeFilter = '', brand_shop_filter = '' } = body
  const queryProd = `${func !== ''
    ? `products as summary(${func}) @filter(type(Product) AND not eq(is_deleted, true)${filter}) {
      totalCount: count(uid)
    }`
    :
    `products as summary(func: type(Product)) @filter(not eq(is_deleted, true)${filter}) {
      totalCount: count(uid)
    }`
  }
  result(func: uid(products), orderdesc: created_at, first: $number, offset: $offset) ${is_temporary !== null ? `@filter(eq(is_temporary, ${is_temporary}))` : `@filter(NOT eq(is_temporary, true))`} {
    ${prodFields}
  }
  `
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${typeFilter == 'brand_shop_name' ?
    `var(${brand_shop_filter}) @filter(type(BrandShop) AND not eq(is_deleted, true)) {
          products as brand_shop.product @filter(not eq(is_deleted, true)${func}${filter})
        }
        summary(func:uid(products)){
          totalCount: count(uid)
        }
        result(func: uid(products), orderdesc: created_at, first: $number, offset: $offset){
          ${prodFields}
      }`
    :
    `${queryProd}`
}
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })

  parsesPricing(result)

  return { result: db.parseFacetArray(result), totalCount: summary?.[0]?.totalCount ?? 0 }
}

// END PREPARE EDGE FOR IMPORT FILE
async function getProductByUids (request) {
  return db.query(`{
    result(func: uid(${request.body.fields.uid})) {
        uid
        product_name
        sku_id
      }
    }`)
}
async function getAll () {
  const queryStr = `{
    result(func: type(Product)) {
      uid
      product_name
    }
  }`
  const res = await db.query(queryStr)
  return res
}


async function copyAreasToAnOther () {
  return {
    statusCode: 200,
    data: ["api nay ko dung nua, cms khong goi vao nua thi xoa"]
  }
}


// END CHECK UID PRODUCT FROM IMPORT FILE AND DATABASE
module.exports = [
  ['post', '/list/product', productFilter],
  ['get', '/list/product/:number/:offset/:keywords', loadMore],
  ['get', '/product/:uid', getByUid],
  ['delete', '/product/:uid', setDelete],
  ['get', '/product/update/areas', updateAreas],
  ['get', '/product/update/detail', updateDetail],
  ['post', '/list/product-option', getOptions],
  ['get', '/product/create-search-data', createSearchData],
  ['post', '/get/productList', getProductByUids],
  ['post', '/product/:uid', mutateProduct],
  ['get', '/list/product', getAll],
  ['get', '/product/copy-areas', copyAreasToAnOther]
]