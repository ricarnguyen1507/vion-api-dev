function convertDateToInt (pricing) {
  if (pricing.from_date) {
    const fromDate = new Date(pricing.from_date)
    pricing.from_date = fromDate.getTime()
  }
  if (pricing.to_date) {
    const toDate = new Date(pricing.to_date)
    pricing.to_date = toDate.getTime()
  }

  return pricing
}
function convertIntToDate (pricing) {
  if (pricing.from_date) {
    const fromDate = new Date(pricing.from_date)
    pricing.from_date = fromDate.getFullYear() + '/' + (fromDate.getMonth() + 1) + '/' + fromDate.getDate()
  }
  if (pricing.to_date) {
    const toDate = new Date(pricing.to_date)
    pricing.to_date = toDate.getFullYear() + '/' + (toDate.getMonth() + 1) + '/' + toDate.getDate()
  }

  return pricing
}

function parsesPricing (products) {
  for(const prod of products) {
    if(Array.isArray(prod['product.pricing'])) {
      prod['product.pricing'] = prod['product.pricing'][0]
    }
  }
}

module.exports = {
  convertDateToInt,
  convertIntToDate,
  parsesPricing
}