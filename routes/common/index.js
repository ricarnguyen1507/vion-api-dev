"use strict"
const db = require("#/services/tvcdb")

function disabled_uid({ params: { uid } }) {
  return db.mutate({ set: { uid, is_deleted: true } }).then(() => "success")
}

function delete_uid({ params: { uid } }) {
  return db.mutate({ del: { uid } }).then(() => "success")
}

module.exports = [
  ['delete', '/disabled/:uid', disabled_uid],
  ['delete', '/delete/:uid', delete_uid]
]