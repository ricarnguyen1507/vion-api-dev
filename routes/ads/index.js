"use strict"

const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function handleForm (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields, files } = request.body

  await db.mutate(fields)

  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'ads_images')
    }
  }
  return { statusCode: 200 }
}

async function getAll () {
  const { result } = await db.query(`{
    result(func: type(Ads)) @filter(not eq(is_deleted, true)) {
      uid
      tracking_utm {
        uid
      }
      ads.group_customer {
        uid
      }
      ads_type
      is_active
      click
      link
      mime_type
      notes
      image
    }
  }`)
  return { result: result.reverse() }
}

async function listAds ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Ads)", ...primaryFilters].join(', ') : "type(Ads)"
  const str2 = filters.length ? [...filters, 'not eq(is_deleted, true)'].join(" AND ") : 'NOT eq(is_deleted, true)'

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    ads as summary(func: ${str1}) @filter(${str2}) {
      total: count(uid)
    }
    data(func: uid(ads), first: $number, offset: $offset) {
      uid
      tracking_utm {
        uid
      }
      ads.group_customer {
        uid
      }
      ads_type
      is_active
      click
      link
      mime_type
      notes
      image
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}

async function setActive (request) {
  const { uid, is_active } = request.body
  await db.mutate({
    set: { uid, is_active }
  })
  return { statusCode: 200 }
}

async function setDelete (request) {
  const uid = request.params.uid
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  return { statusCode: 200 }
}

module.exports = [
  ['post', '/list/ads', listAds],
  ['get', '/ads', getAll],
  ['post', '/ads', handleForm],
  ['put', '/ads/active', setActive],
  ['delete', '/ads/:uid', setDelete]
]