"use strict"
const db = require("#/services/tvcdb")
const session = require("#/modules/userSession")
const orderFragment =
  `created_at
uid
order_id
order_status
pay_status
total_pay
customer_name
phone_number
address_des
pay_gateway
voucher_code
extra_applied_value
applied_value: voucher_value
notes
client_notes
request_delivery_time
date_delivery_success
reason
district {
  uid
  name
}
province {
  uid
  name
}
order.customer {
  customer_name
  phone_number
}
user.ott{
  uid
  ott_name
}
sub_orders {
  uid
  order_id
  partner_id
  order_status
  notes
  shipping_code
  reason
  order.partner {
    uid
    partner_name
    shipping_method
  }
  shipping_partner_value
  date_delivery_success
  order_items {
    uid
    product_name
    quantity
    sell_price
    cost_price
    discount
    promotion_desc
    promotion_detail
    product: order.product {
      sku_id
      promotion_detail
      promotion_desc
      payment_terms
      return_terms
      unit
      product.areas {
        uid
        name
      }
    }
  }
}
voucher_usage: ~voucher_usage.order @normalize {
  uid
  voucher_uid: voucher_uid
  voucher_usage.voucher {
    voucher_usage_code: voucher_code
    voucher_value: voucher_value
    voucher_type: voucher_type
  }
}`
const subOrder = `
uid
order_id
partner_id
order_status
notes
shipping_code
reason
order.partner {
  uid
  partner_name
  shipping_method
}
shipping_partner_value
date_delivery_success
order_items {
  uid
  product_name
  quantity
  sell_price
  cost_price
  discount
  promotion_desc
  promotion_detail
  product: order.product {
    sku_id
    promotion_detail
    promotion_desc
    payment_terms
    return_terms
    unit
    product.areas {
      uid
      name
    }
  }
}
`
function calcPay ({ sell_price = 0, discount = 0, quantity = 0 }) {
  return (1 - discount / 100) * sell_price * quantity
}
function parseSubOrder (data) {
  data['sub_order_total'] = 0
  data.order_partner_name = data['order.partner'].partner_name ?? ''
  data.order_partner_shipping_method = data['order.partner'].shipping_method ?? ''
  data.order_items && data.order_items.map(item => {
    data['order_items_sub_order_total'] += calcPay(item)
    data.order_items_sku_id = data['order_items']?.product?.sku_id ?? ''
    data.order_items_sku_id = data['order_items']?.product?.sku_id ?? ''
    data.order_items_unit = data['order_items']?.product?.unit ?? ''
    data.order_items_return_terms = data['order_items']?.product?.return_terms ?? ''
    data.order_items_promotion_detail = data['order_items']?.promotion_detail ?? ''
    data.order_items_promotion_desc = data['order_items']?.promotion_desc ?? ''
  })
  delete data['order.partner']
  delete data['order_items']
}
async function mutate (request) {
  const { uid, order_status, notes, customer_name, phone_number, address_des, shipping_code, reason, shipping_partner_value, date_delivery_success, is_sub_order, extra_applied_value } = request.body
  const userLogged = await session.getUser(request)
  let dataOrigin = null;
  const { result: [data] } = await db.query(`{
     result(func: uid(${uid})){
      ${is_sub_order == true ? subOrder : orderFragment}
    }
  }`)
  if(is_sub_order == true && Object.keys(data).length) {
    parseSubOrder(data)
  }
  dataOrigin = data

  const { result: [current_o] } = await db.query(`{
    result(func: uid(${uid})) @filter(type(PrimaryOrder)) {
      uid
      order_status
      order_id
      sub_orders{
        uid
    	  date_delivery_success
      }
    }
  }`)

  // TODO: Check nếu data là thuộc PrimaryOrder |=> order_status === 4 ( đã giao ) |=> Update date_delivery_success
  let date_primary //, date_max
  if(current_o && current_o?.sub_orders && current_o?.sub_orders.length > 0) {
    if(order_status && parseInt(order_status) === 4 && current_o.order_status !== 4) { // Đã giao === 4
      let date_max = 0
      current_o.sub_orders.forEach(i => {
        if (i?.date_delivery_success > date_max) {
          date_max = i?.date_delivery_success
        }
      })
      date_primary = date_max !== 0 ? date_max : null
    }
  }

  // Khí order_status (PrimaryOrder) === "Đã hủy" (6) => Update order_status cho order con
  if(current_o && order_status && parseInt(order_status) === 6) {
    await db.upsert({
      query: `{
        ods as result(func: uid(${uid})) @filter(type(PrimaryOrder)) {
          uid
          sub_orders {
            s_ods as uid
          }
        }
      }`,
      cond: "@if(gt(len(ods), 0) AND gt(len(s_ods), 0))",
      set: [
        {
          uid: "uid(ods)",
          order_status,
          reason
        },
        {
          uid: "uid(s_ods)",
          order_status,
          reason
        }
      ]
    })
  }

  return db.mutate({
    set: {
      uid,
      order_status,
      notes,
      customer_name,
      phone_number,
      address_des,
      shipping_code,
      reason,
      shipping_partner_value,
      date_delivery_success: current_o && date_primary ? date_primary : date_delivery_success,
      extra_applied_value
    }
  }).then(async () => {
    // const trackBody = {
    //   type: 'order',
    //   new_status: parseInt(order_status),
    //   old_status: current_o.order_status,
    //   order_id: current_o.order_id,
    //   uid: userLogged.uid,
    //   user_id: '',
    //   user_phone: userLogged.phone_number || ""
    // }
    // tracking(trackBody)
    const { result: [dataEdit] } = await db.query(`{
       result(func: uid(${uid})){
        ${is_sub_order == true ? subOrder : orderFragment}
      }
    }`)
    if(is_sub_order == true && Object.keys(dataEdit).length) {
      parseSubOrder(dataEdit)
    }
    process.emit('track', {
      pages: is_sub_order && (is_sub_order === true || is_sub_order === 'true') ? 'order_sub' : 'order',
      dataOrigin,
      dataEdit,
      actionType: 'EDIT',
      userData: userLogged
    })

    process.emit('sync', uid, 'order')

    return current_o && date_primary ? { statusCode: 200, date_delivery_success: date_primary } : { statusCode: 200 }
  })
}


function getAll () {
  return db.query(`{
    result(func: type(PrimaryOrder), orderdesc: created_at) @filter(not eq(is_deleted, true) and not eq(order_status, 0)) {
      ${orderFragment}
    }
  }`)
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(PrimaryOrder), orderdesc: created_at, first: $number, offset: $offset) @filter(not eq(order_status, 0)) {
      ${orderFragment}
    }
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) {
      ${orderFragment}
  }`, { $uid })
}

async function orderFilter ({ body }) {
  const { number = 20, page = 0, filter = '' } = body
  const { summary, data } = await db.query(`query result($number: int, $offset: int) {
    orders as summary(func: type(PrimaryOrder)) @filter(not eq(is_deleted, true) and not eq(order_status, 0)${filter}) {
      totalCount: count(uid)
    }
    data(func: uid(orders), orderdesc: created_at, first: $number, offset: $offset) {
      ${orderFragment}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })

  data.map(d => {
    d.total_initial_amount = d?.voucher_usage?.[0] ? d.total_pay + d.applied_value : d.total_pay,
    d.total_pay = d?.extra_applied_value ? d.total_pay - d.extra_applied_value : d.total_pay
  })
  calTotalPay(data)
  calInitAmount(data)
  calRevenue(data)
  calOrderValue(data)
  parseSubOrderItem(data)
  parseOrderItemPromotion(data)
  return {
    summary, data
  }

}
function calTotalPay (data) {
  if(data?.length) {
    for(const obj of data) {
      if(obj?.sub_orders?.length) {
        for(const sub of obj.sub_orders) {
          let total_init = 0
          if(sub.order_status == 6 || sub.order_status == 7 || sub.order_status == 8 || sub.order_status == 9) {
            if(sub?.order_items?.length) {
              for(let i = 0; i < sub.order_items.length; i++) {
                total_init += sub.order_items[i]?.sell_price * sub.order_items[i]?.quantity
              }
            }
          }
          obj.total_pay = obj.total_pay - total_init
        }
      }
    }
  }
}
function calInitAmount (data) {
  if(data?.length) {
    for(const obj of data) {
      if(obj?.sub_orders?.length) {
        for(const sub of obj.sub_orders) {
          let total_init = 0
          if(sub.order_status == 6 || sub.order_status == 7 || sub.order_status == 8 || sub.order_status == 9) {
            if(sub?.order_items?.length) {
              for(let i = 0; i < sub.order_items.length; i++) {
                total_init += sub.order_items[i]?.sell_price * sub.order_items[i]?.quantity
              }
            }
          }
          obj.total_initial_amount = obj.total_initial_amount - total_init
        }
      }
    }
  }
}
function calRevenue (data) {
  if(data?.length) {
    for(const obj of data) {
      let total_cost_price = 0
      if(obj?.sub_orders?.length) {
        for(const sub of obj.sub_orders) {
          if(sub.order_status != 6 && sub.order_status != 7 && sub.order_status != 8 && sub.order_status != 9) {
            if(sub?.order_items?.length) {
              for(let i = 0; i < sub.order_items.length; i++) {
                total_cost_price += sub.order_items[i]?.cost_price * sub.order_items[i]?.quantity
              }
            }
          }
        }
      }
      obj.revenue = obj.extra_applied_value ? obj.total_initial_amount - obj.applied_value - obj.extra_applied_value - total_cost_price : obj.total_initial_amount - obj.applied_value - total_cost_price
    }
  }
}
function calOrderValue (data) {
  if(data?.length) {
    for(const obj of data) {
      let total_cost_price = 0
      if(obj?.sub_orders?.length) {
        for(const sub of obj.sub_orders) {
          if(sub?.order_items?.length) {
            for(let i = 0; i < sub.order_items.length; i++) {
              total_cost_price += sub.order_items[i]?.sell_price * sub.order_items[i]?.quantity
            }
          }
        }
      }
      obj.order_value = total_cost_price
    }
  }
}
function parseSubOrderItem (data) {
  if(data?.length) {
    for(const obj of data) {
      const sub_order_temp = []
      if(obj?.sub_orders?.length) {
        for(const sub of obj.sub_orders) {
          if(sub?.order_items?.length && sub?.order_items?.length > 1) {
            for(let i = 0; i < sub.order_items.length; i++) {
              const item = {}
              let sub_tem = {}
              if(i != 0) {
                sub_tem = {
                  order_items: [],
                  uid: sub.uid
                }
                item['uid'] = sub.order_items[i]?.uid
                item['product_name'] = sub.order_items[i]?.product_name ?? ''
                item['product'] = sub.order_items[i]?.product ?? {}
                item['quantity'] = sub.order_items[i]?.quantity ?? ''
                item['return_terms'] = sub.order_items[i]?.return_terms ?? ''
                item['promotion_detail'] = sub.order_items[i]?.promotion_detail ?? ''
                item['promotion_desc'] = sub.order_items[i]?.promotion_desc ?? ''
                item['sell_price'] = sub.order_items[i]?.sell_price ?? ''
                item['cost_price'] = sub.order_items[i]?.cost_price ?? ''
                item['notes'] = obj?.notes ?? ''
                sub_tem['order_items'].push(item)
                sub_order_temp.push(sub_tem)
              }else {
                sub_tem = {
                  order_items: [],
                  order_id: sub?.order_id,
                  order_status: sub?.order_status,
                  ['order.partner']: sub?.['order.partner'],
                  partner_id: sub?.partner_id,
                  date_delivery_success: sub?.date_delivery_success,
                  reason: sub?.reason,
                  shipping_code: sub?.shipping_code,
                  shipping_partner_value: sub?.shipping_partner_value,
                  uid: sub.uid
                }
                sub_tem['order_items'].push(sub?.order_items[i])
                sub_order_temp.push(sub_tem)
              }
            }
          }else{
            sub_order_temp.push(sub)
          }
        }
      }
      obj.sub_orders = sub_order_temp
    }
  }
}
function parseOrderItemPromotion (data) {
  const date_submit = new Date('8/18/2021, 2:30:25 PM')
  if(data?.length) {
    for(const obj of data) {
      if(obj?.created_at <= date_submit.valueOf()) {
        if(obj?.sub_orders?.length) {
          for(const sub of obj.sub_orders) {
            if(sub?.order_items?.length) {
              sub?.order_items.map(item => item.promotion_detail = item?.product?.promotion_detail ?? '')
            }
          }
        }
      }
    }
  }
}

module.exports = [
  ['post', '/list/order', orderFilter],
  ['get', '/list/order', getAll],
  ['get', '/list/order/:number/:offset', loadMore],
  ['get', '/order/:uid', getByUid],
  ['post', '/order', mutate],
  // ['get', '/order/order_export.xlsx', orderExport]
]