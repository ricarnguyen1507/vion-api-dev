"use strict"
const db = require("#/services/tvcdb")
const promotionField = require('./promotionField')
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'promotion')
    }
  }
  return { statusCode: 200 }
}
async function getDataTable ({ body }) {
  const { number, page, filter = '' } = body.params
  const { result, summary } = await db.query(`query result($number: int, $offset: int){
      promotion as summary(func: type(Promotions), orderasc: display_order) @filter( not eq(is_deleted, true) ${filter} ) {
        totalCount: count(uid)
      }
      result(func: uid(promotion), first: $number, offset: $offset) {
        ${promotionField}
      }
    }`, {
    $number: String(number),
    $offset: String(number * page)
  })
  return { result, summary }
}
async function getAll ({ query }) {
  const { brand_shop_uid = false } = query
  let queryStr = `{
    result(func: type(Promotions)) @filter(NOT has(promotion.brand_shop) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      display_name_detail
    }
  }`
  if (brand_shop_uid && db.isUid(brand_shop_uid)) {
    queryStr = `{
      var(func: uid(${brand_shop_uid})) @filter(type(BrandShop)){
        promotions as  ~promotion.brand_shop
      }
      result(func: uid(promotions))@filter(NOT eq(is_deleted, true) AND eq(display_status, 2)){
        uid
        display_name_detail
      }
    }`
  }
  const { result, summary } = await db.query(queryStr)
  return { result, summary }
}

async function getOptions () {
  const queryStr = `{
    result(func: type(Promotions)) @filter(NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      display_name_detail
    }
  }`
  const { result, summary } = await db.query(queryStr)
  return { result, summary }
}
/* async function setDelete ({params: {uid}}) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const {result} = await db.query(`{
    result(func: uid(${uid})) @filter(eq(is_default, true) AND NOT eq(is_deleted, true))  {
      check: count(uid)
    }
  }`)

  if(result?.length) {
    return { statusCode: 400, message: "Không được xoá Promotions mặc định" }
  }

  return db.mutate({ set: { uid, is_deleted: true } }).then(() => ({ statusCode: 200 }))
} */

module.exports = [
  ['post', '/list/promotion', getDataTable],
  ['get', '/list/promotions', getAll],
  ['get', '/list/promotions-options', getOptions],
  // ['delete', '/hybrid_layout/:uid', setDelete],
  ['post', '/promotion', mutate]
]