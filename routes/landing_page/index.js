"use strict"

const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

const tableFields = `
  uid
  landing_name
  image_cover
  display_status
`

async function deleteUid (request) {
  const uid = request.params.uid
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  return { statusCode: 200 }
}

async function getAll () {
  const queryStr = `{
    result(func: type(LandingPage)) @filter(not eq(is_deleted, true)) {
      ${tableFields}
    }
  }`
  const res = await db.query(queryStr)
  return res
}


async function handleFormData (request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'landing')
    }
  }
  return { statusCode: 200 }
}

module.exports = [
  ['get', '/list/landing', getAll],
  ['post', '/landing', handleFormData],
  ['delete', '/landing/:uid', deleteUid]
]
