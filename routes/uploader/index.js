"use strict"

const { saveMediaFile } = require("#/modules/uploadFileHandle")

async function handleFormData(request, uid) {
  const { files } = request.body

  const images = {}

  for(const multipart_field in files) {
    const file = files[multipart_field]
    const [newFilePath] = saveMediaFile(file, multipart_field, `uploader/${uid}`)
    images[multipart_field] = newFilePath
  }

  return images
}

module.exports = [
  ['post', '/uploader/upload-image', handleFormData]
]