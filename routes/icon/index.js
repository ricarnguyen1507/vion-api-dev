"use strict"
const fs = require('fs-extra')
const path = require('path')
const pump = require('pump')
const db = require("#/services/tvcdb")
const { image_dir } = config

const icons_dir = path.join(image_dir, "icons")

function mkdir (dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true })
  }
}

// function rename(fileName, newName) {
//   const ext = fileName.slice(-4)
//   return `${newName}${ext}`
// }

mkdir(icons_dir)

function handleUpload (request, reply) {
  if (!request.isMultipart()) {
    reply.code(400).send(new Error('Request is not multipart'))
    return
  }
  // let time = (new Date()).getTime()

  // const fields = []
  const uids = {}

  function onEnd (err) {
    if(err) {
      reply.send(err)
    } else {
      // Trả lại danh sách map, hashtable uid
      // VD: { icon_1: 0x1, icon_2: 0x2 }
      reply.send(uids)
    }
  }
  function fileHandle (field, file, filename, encoding, mimetype) {
    /* Save file lấy tên theo uid
      1) Tạo data rỗng để lấy uid
      2) Lưu file lấy tên uid
      3) Update lại dữ liệu có tên file theo uid
    */
    if(mimetype.startsWith("image")) {
      db.mutate({ set: { "dgraph.type": "Icon", uid: `_:${field}` } })
        .then(res => {
          const uid = res.getUidsMap().get(field)
          uids[field] = uid
          const ext = filename.slice(-4)
          const saveFileName = `${uid}${ext}`
          pump(file, fs.createWriteStream(path.join(icons_dir, saveFileName)), function (err) {
            if(err) {
              request.log.error(err)
            } else {
              db.mutate({
                set: { uid, source: `icons/${saveFileName}` }
              }).catch(err => request.log.error(err))
            }
          })
        })
        .catch(err => request.log.error(err))
    }
  }
  request.multipart(fileHandle, onEnd)
}

function getAll () {
  return db.query(`{ result(func: type(Icon)){uid,expand(_all_)} }`)
}

function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Icon), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

function deleteIcon (request) {
  const { uid, source } = request.body
  try {
    fs.unlinkSync(path.join(image_dir, source))
  } catch(err) {
    request.log.info(err.message)
  }
  return db.mutate({ del: { uid } }).then(() => ({ statusCode: 200 }))
}

module.exports = [
  ['get', '/list/icon', getAll],
  ['get', '/list/icon/:offset/:number', loadMore],
  ['post', '/del/icon', deleteIcon],
  ['post', '/set/icon', handleUpload]
]