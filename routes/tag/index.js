"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'tag')
    }
  }
  return { statusCode: 200 }
}
async function getDataTable ({ query }) {
  const { number = -1, page = 0, tag_category = '', filter } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const queryStr = tag_category == '' ?
    `tags as summary(func: type(Tag)) @filter(not eq(is_deleted, true)${filter}){
    totalCount: count(uid)
  }` :
    `tags as summary(func: type(Tag)) @filter(eq(tag_category, ${tag_category})${filter}){
    totalCount: count(uid)
  }`
  const { result, summary } = await db.query(`{
      ${queryStr}
      result(func: uid(tags) ${paginate}) {
        uid
        display_status
        tag_category
        tag_title
        tag_type
        image_promotion
        promotion_value @facets(purchase:purchase, extra:extra, percentage: percentage, fee:fee)
        name_tag_value_1
        name_tag_value_2
        shipping_value {
          uid
          name
        }
      }
    }`)
  return { result, summary }
}
async function getAll () {

  return await db.query(`{
    tags as var(func: type(Tag))
    result(func: uid(tags)) {
      uid
      tag_category
      tag_title
      tag_type
    }
  }`)
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Tag), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Tag)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/tag', getDataTable],
  ['get', '/list/all_tag', getAll],
  ['get', '/list/tag/:offset/:number', loadMore],
  ['get', '/tag/:uid', getByUid],
  ['post', '/tag', mutate]
]