"use strict"

const db = require("#/services/tvcdb")

async function specsValueFilter({ params: { specs_type_uid } }) {
  if (!specs_type_uid || !db.isUid(specs_type_uid)) {
    throw new Error("What are you doing here?")
  }

  return await db.query(`{
    result(func: type(SpecsValue))) @filter(uid(${specs_type_uid})) {
      specs_value_name
    }
  }`)
}


module.exports = [
  ['get', '/list/specs-value/:specs_type_uid', specsValueFilter]
]
