// const db = require("#/services/tvcdb")
// const { hrvTypes } = require("#/services/sync-partner/haravan")
// const { fhTypes } = require("#/services/sync-partner/foodhub")

module.exports = function() {
  if(!config.sync_partner) return []

  const syncHandle = require("#/services/sync-partner")
  const syncMissingHandle = require("#/services/sync-partner/sync-missing")

  async function requestSyncHandle({ params: { type, uid } }) {
    if(!type || !uid) {
      throw { statusCode: 400, message: `sync failed: type(${type}), uid: (${uid})` }
    }
    syncHandle(uid, type)
    return { statusCode: 200 }
  }

  async function requestSyncMissingHandle({ params: { type, brand_shop_id } }) {
    if(!type || !brand_shop_id) {
      throw { statusCode: 400, message: `sync missing failed: type(${type}), uid: (${brand_shop_id})` }
    }
    syncMissingHandle(brand_shop_id, type)
    return { statusCode: 200 }
  }
  return [
    ['get', '/sync/:type/:uid', requestSyncHandle],
    ['get', '/sync-missing/:type/:brand_shop_id', requestSyncMissingHandle]
  ]
}