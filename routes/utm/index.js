"use strict"
const db = require("#/services/tvcdb")

function listTrackingUtm() {
  return db.query(`{
    result(func: type(TrackingUtm)) @filter(not eq(is_deleted, true)) {
      uid
      tracking_name
      short_desc: short_desc
      tracking_parameters {
        uid
        parameter_utm {
          uid
        }
        tracking_value {
          uid
        }
      }
    }
  }`)
}

function getTrackingUtmOption() {
  return db.query(`{
    result(func: type(TrackingUtm)) @filter(not eq(is_deleted, true)) {
      uid
      tracking_name
    }
  }`)
}

async function updateTrackingUtm(request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}

function listParameterUtm() {
  return db.query(`{
    result(func: type(ParameterUtm)) {
      uid
      tracking_code
      short_desc
      tracking_values {
        uid
        val_code
        val_name
      }
    }
  }`)
}

async function updateParameterUtm(request) {
  if(!request.isMultipart()) {
    throw new Error('multipart request only')
  }
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}

async function deleteUid(request) {
  const uid = request.params.uid
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  return { statusCode: 200 }
}

module.exports = [
  ['get', '/trackingUtm', listTrackingUtm],
  ['get', '/trackingUtm/get-option', getTrackingUtmOption],
  ['post', '/trackingUtm', updateTrackingUtm],
  ['delete', '/trackingUtm/:uid', deleteUid],
  ['get', '/parameterUtm', listParameterUtm],
  ['post', '/parameterUtm', updateParameterUtm],
  ['delete', '/parameterUtm/:uid', deleteUid]
]