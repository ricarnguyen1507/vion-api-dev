"use strict"
const db = require("#/services/tvcdb")

function mutate(request) {
  return db.mutate(db.addTypeSet("LayoutType", request.body)).then(result => db.getUids(result))
}
function getAll() {
  return db.query(`{ result(func: type(LayoutType), orderasc: type_value){uid,expand(_all_)} }`)
}

module.exports = [
  ['get', '/list/layout_type', getAll],
  ['post', '/layout_type', mutate]
]