"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function specsTypeFilter ({ body }) {
  const { number = 20, page = 0, filter = '' } = body
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    specsType as summary(func: type(SpecsType), orderdesc: created_at, first: $number, offset: $offset) @filter(NOT eq(is_deleted, true) ${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(specsType)) {
      uid
      specs_type_name
      specs_type.values @facets(orderasc: display_order) {
        uid
        specs_value_name
      }
      display_status
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })

  return { result: db.parseFacetArray(result), summary }
}

async function handleForm (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'specs_type')
    }
  }
  return { statusCode: 200 }
}

async function getOptions () {
  const { result } = await db.query(`{
    result(func: type(SpecsType)) @filter(eq(display_status,2)) {
      uid
      specs_type_name
      specs_type.values @facets(orderasc: display_order) {
        uid
        specs_value_name
      }
    }
  }`)
  return {
    result: db.parseFacetArray(result)
  }
}

async function updateSpecsValues ({ params: { product_uid } }) {
  if (!product_uid || !db.isUid(product_uid)) {
    throw new Error("This is wrong way to go.")
  }

  const { specs_section } = await db.query(`{
    var(func: uid(${product_uid})) {
      product.specs_sections {
        ps as uid
      }
    }

    specs_section(func: uid(ps)) {
      uid
      section_name
      section_ref
      section_ref_value
    }
  }`)

  if (specs_section.length > 0) {
    for (let i = 0; i < specs_section.length; i++) {
      const ss = specs_section[i]
      if (ss.section_ref && ss.section_ref_value && ss.section_ref_value !== "") {
        const { find_exist } = await db.query(`{
          find_exist(func: type(SpecsValue)) @filter(eq(specs_value_name, "${String(ss.section_ref_value)}")) {
            uid
            specs_value_name
            specs_type: ~specs_type.values {
              uid
              specs_type_name
            }
          }
        }`)

        let specsTypeData = {}
        if (find_exist?.length === 0) {
          //tạo mới lưu vào
          specsTypeData = {
            uid: ss.section_ref,
            "specs_type.values": {
              uid: "_:new_specs_value",
              "dgraph.type": "SpecsValue",
              specs_value_name: ss.section_ref_value
            }
          }
        } else {
          if (find_exist[0] && find_exist[0].uid) {
            specsTypeData = {
              uid: ss.section_ref,
              "specs_type.values": {
                uid: find_exist[0].uid,
                "dgraph.type": "SpecsValue",
                specs_type_name: ss.section_ref_value
              }
            }
          }
        }

        await db.mutate({ set: specsTypeData })
      }
    }
    return { statusCode: 200, message: "sync successfully" }
  }
  return { statusCode: 200, message: "specs_section empty" }
}


module.exports = [
  ['post', '/list/specs-type', specsTypeFilter],
  ['post', '/specs-type', handleForm],
  ['get', '/list/specs-type-options', getOptions],
  ['get', '/specs-type/update-specs-values/:product_uid', updateSpecsValues],
]
