"use strict"
const db = require("#/services/tvcdb")
const layoutField = require('./layoutField')
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function mutate (request) {
  const { fields, files } = request.body
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'layout_section')
    }
  }
  return { statusCode: 200 }
}
async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    layouts as summary(func: type(Layout)) @filter(not eq(is_deleted, true) and not has(~brand_shop.layout)${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(layouts), orderasc: display_order ${paginate}) @filter(not eq(is_deleted, true) and not has(~brand_shop.layout) ${filter}) {
      ${layoutField}
    }
  }`)
  return { result: db.parseFacetArray(result), summary }
}

async function setDelete ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  const { result } = await db.query(`{
    result(func: uid(${uid})) @filter(eq(is_default, true) AND NOT eq(is_deleted, true))  {
      check: count(uid)
    }
  }`)

  if(result?.length) {
    return { statusCode: 400, message: "Không được xoá Layout mặc định" }
  }

  return db.mutate({ set: { uid, is_deleted: true } }).then(() => ({ statusCode: 200 }))
}

module.exports = [
  ['get', '/list/layout', getAll],
  ['delete', '/layout/:uid', setDelete],
  ['post', '/layout', mutate]
]