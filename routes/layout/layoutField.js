module.exports = `
  uid
  layout_name
  layout.hybrid_layout {
    uid
    layout_name
  }
  display_status
  created_at
  is_deleted
  is_default
  display_order
  target_type
  layout.layout_type
  image_cover
  layout.layout_section @facets(orderasc: display_order) {
    uid
    section_limit
    section_name
    section_ref
    section_type
    section_value
  }
  layout.customers {
    uid
    expand(_all_)
  }
  layout.group_customers{
    uid
    expand(_all_)
  }
  is_laucher_temp
  layout.ott{
    uid
    ott_name
  }
`