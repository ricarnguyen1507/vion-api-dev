function convertDateToInt(pricing) {
  if (pricing.from_date) {
    const fromDate = new Date(pricing.from_date)
    pricing.from_date = fromDate.getTime()
  }
  if (pricing.to_date) {
    const toDate = new Date(pricing.to_date)
    pricing.to_date = toDate.getTime()
  }

  return pricing
}
function convertIntToDate(pricing) {
  if (pricing?.from_date) {
    const fromDate = new Date(pricing.from_date)
    pricing.from_date = fromDate.getFullYear() + '/' + (fromDate.getMonth() + 1) + '/' + fromDate.getDate()
  }
  if (pricing?.to_date) {
    const toDate = new Date(pricing.to_date)
    pricing.to_date = toDate.getFullYear() + '/' + (toDate.getMonth() + 1) + '/' + toDate.getDate()
  }

  return pricing
}

function parsesPricing(products) {
  const parseD = products?.map(d => ({
    ...d,
    'partner.pricing': convertIntToDate(d?.['partner.pricing']) // get first pricing from array logging
    // 'partner.pricing': convertIntToDate(d?.['partner.pricing']?.[0]) // get first pricing from array logging
  }))
  return parseD
}

module.exports = {
  convertDateToInt,
  convertIntToDate,
  parsesPricing
}