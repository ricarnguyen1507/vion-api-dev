"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  return await db.mutate(request.body)
}
const partnerFrag = `
uid
      partner.collection {
        uid
        expand(_all_)
      }
      id
      partner_name
      contract_number
      email
      tax_id
      contact_person
      phone_number
      address_str
      bank_name
      bank_branch_name
      account_name
      account_number
      contract_type
      partner.pricing (first: -1) {
        uid
        back_margin
        back_margin_terms
        from_date
        to_date
      }
      return_terms
      payment_terms
      shipping_method
      shipping_time
      partner.warehouse {
        uid
        expand(_all_)
      }
      required_order_value
`
async function getDataTable ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    partners as summary(func: type(Partner)) @filter(NOT eq(is_deleted, true)${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(partners)${paginate}){${partnerFrag}}
  }`)
  return { result, summary }
}

function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Partner), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}

function getByUid ({ params: { uid } }) {
  return db.query(`{
    result(func: uid(${uid})) {
      uid
      expand(_all_) {
        uid
        expand(_all_)
      }
    }
  }`)
}

module.exports = [
  ['get', '/list/partner', getDataTable],
  ['get', '/list/partner/:offset/:number', loadMore],
  ['get', '/partner/:uid', getByUid],
  ['post', '/partner', mutate]
]