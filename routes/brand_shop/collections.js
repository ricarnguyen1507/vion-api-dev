"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")
const tableFields = `
  uid
  is_deleted
  collection_name
  collection_image
  collection_icon
  background_image
  collection_type
  display_order
  sort_order
  layout_type
  parent  @filter(not eq(is_deleted, true) AND not eq(display_status, 1)){
    uid
    expand(_all_)
  }
  children: ~parent @facets(orderasc: display_order) @filter(not eq(is_deleted, true) AND not eq(display_status, 1)) {
    uid
    collection_name
  }
  highlight.products @facets(orderasc: display_order) {
    uid
    product_name
  }
  highlight.collections @facets(orderasc: display_order) {
    uid
    collection_name
  }
  voucher.customers (orderasc: display_order) {
    uid
    expand(_all_)
  }
  ~highlight.collections {
    uid
    collection_name
  }
  brand_shop: ~brand_shop.collection {
    uid
    brand_shop_name
  }
  type_highlight
  color_title
  color_price
  color_discount
  highlight_name
  image_highlight
  display_status
  reference_type
  is_temporary
  target_type
  condition_type
  condition_value
  voucher_type
  voucher_code
  voucher_value
  max_applied_value
  voucher_label
  start_at
  stop_at
  redeem
  is_internal
  created_at
`

async function getBrandShopHighlightDetail ({ params }) {
  const collection_id = params?.uid;
  const queryStr = `{
    result(func: uid(${collection_id})) @filter(type(Collection) AND has(~brand_shop.collection) AND NOT eq(is_deleted, true)) {
      ${tableFields}
    }
  }`
  const { result: [collection] } = await db.query(queryStr)
  return collection ?? {}
}

async function getAll ({ query }) {
  const { t, is_temp, reference_type, is_parent, brand_shop_uid, number = 300, page = 0, filter_str, typeFilter } = query
  let sort_filed = ', orderasc: display_order, first: $number, offset: $offset'
  if (t && t === 300) {
    sort_filed = ''
  }
  let filter = reference_type ? `AND NOT eq(reference_type, ${reference_type})` : ''
  filter += filter_str && filter_str.length > 0 && typeFilter != "brand_shop_name" ? ` AND ${filter_str}` : ''
  filter += is_temp && is_temp === "true" ? ` AND eq(is_temporary, true)` : is_temp && is_temp === "false" ? ` AND NOT eq(is_temporary, true)` : ''
  filter += t ? ` AND eq(collection_type, ${t})` : ''
  filter += (typeof is_parent === "undefined") ? '' : is_parent === "true" ? `AND NOT has(parent)` : 'AND NOT has(~parent)'
  const queryStr = brand_shop_uid ?
    `var(func: type(Collection)) @filter(has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
      ~brand_shop.collection @filter(uid(${brand_shop_uid})) {
        c as brand_shop.collection
      }
    }
    summary(func:uid(c)) @filter(has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
      total: count(uid)
    }
    result(func: uid(c) ${sort_filed}) @filter(has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
      ${tableFields}
    }` : typeFilter == "brand_shop_name" ?
      `var(func: type(Collection)) @filter(has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
        ~brand_shop.collection @filter(${filter_str}) {
          c as brand_shop.collection
        }
      }
      summary(func:uid(c)) @filter(type(Collection) AND has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
        total: count(uid)
      }
      result(func: uid(c) ${sort_filed}) @filter(type(Collection) AND has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
        ${tableFields}
      }
    `
      :
      `c as summary(func: type(Collection)) @filter(has(~brand_shop.collection) AND NOT eq(is_deleted, true)${filter}) {
    total: count(uid)
    }
    result(func: uid(c) ${sort_filed}) {
      ${tableFields}
    }`
  if (t && t === 300) {
    log.debug(queryStr)
  }
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${queryStr}
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return {
    result: db.parseFacetArray(result),
    summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page } }
}

async function handleForm (request) {
  const brand_shop_uid = request?.params?.brand_shop_uid
  let uid = request.params.uid
  const { fields, files } = request.body
  const res = await db.mutate(fields)
  uid = uid.startsWith('_:') ? res.getUidsMap().get('new_collection') : uid
  if(brand_shop_uid) {
    const brand_shop_product = {
      "dgraph.type": "BrandShop",
      "uid": brand_shop_uid,
      "brand_shop.collection": { uid: uid }
    }
    await db.mutate({ set: brand_shop_product })
  }
  if(Object.keys(files).length > 0) {
    for(const multipart_field in files) {
      const file = files[multipart_field]
      saveMediaMD5([multipart_field, file], 'collection_images')
    }
  }
  return { statusCode: 200 }
}

module.exports = {
  collection_route: [
    ["get", "/brand-shop/collection-detail/:uid", getBrandShopHighlightDetail], //Lấy thông tin chi tiết highlight brand shop
    ['get', '/brand-shop/list/collection', getAll],
    ['post', '/brand-shop/collection/:uid/:brand_shop_uid', handleForm],
  ]
}