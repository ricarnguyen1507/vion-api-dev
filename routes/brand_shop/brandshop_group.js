"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}


const fields = `
  uid
  brandshop_group_name
  brandshop_group.brandshops @facets(orderasc: display_order) {
    uid
    brand_shop_name
  }
  display_status
`
async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
    brandshop_group as summary(func: type(BrandshopGroup)) @filter(NOT eq(is_deleted, true) ${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(brandshop_group)${paginate}){${fields}}
  }`)
  return { result: db.parseFacetArray(result), summary }
}

async function getOption () {
  const query = `{
    result(func: type(BrandshopGroup)) @filter(NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      brandshop_group_name
    }
  }`
  return db.query(query)
}

module.exports = {
  brandshop_group_route: [
    ['get', '/list/brandshop_group', getAll],
    ['post', '/brandshop_group', mutate],
    ['get', '/list/brandshop_group/options', getOption],
  ] }