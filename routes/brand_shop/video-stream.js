"use strict"
const db = require("#/services/tvcdb")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

const streamFrag = `
    uid
    stream.name
    stream.display_name
    stream.start_time
    stream.end_time
    stream.image_thumb
    stream.image_fullscreen
    stream.products {
      uid
      stream_product.product {
        uid
        product_name
      }
      time
    }
    stream.view_count
    stream.view_virtual
    display_status
    stream.is_highlight
    stream.channel {
      uid
      channel.name
      channel.link
    }
    stream.video_transcode
    brand_shop: ~brand_shop.video_stream {
      uid
      brand_shop_name
    }
    is_portrait
`

function getAll () {
  return db.query(`{ result(func: type(VideoStream)) @filter(has(~brand_shop.video_stream)){
    ${streamFrag}
  }}`)
}

async function streamFilter ({ body }) {
  const { number = 20, page = 0, func = '', filter } = body
  const funcStr = func !== '' ? `
      streams as summary(${func}) @filter(type(VideoStream) AND has(~brand_shop.video_stream) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }` : `
      streams as summary(func: type(VideoStream)) @filter(has(~brand_shop.video_stream)  ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }`
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    result(func: uid(streams), first: $number, offset: $offset)  {
      ${streamFrag}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { result, summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page } }
}

function getUid (uid) {
  return db.query(`{ result(func: uid(${uid})) @filter(type(VideoStream) AND has(~brand_shop.video_stream)){
    ${streamFrag}
  }}`)
}

function getStreamUId (request) {
  const uid = request?.params?.uid ?? '';
  return getUid(uid);
}
async function streamChangeStatus ({ body }) {
  const { uid, status } = body ?? {}
  try{
    await db.upsert({
      query: `{
        var(func: uid(${uid})) @filter(type(VideoStream) AND has(~brand_shop.video_stream)) {
          c as uid
        }
      }`,
      cond: "@if(gt(len(c), 0))",
      set: {
        uid: "uid(c)",
        display_status: status
      }
    });
    return {
      statusCode: 200,
      data: getUid(uid)
    }
  }catch(err) {
    return {
      statusCode: 500,
      data: getUid(uid),
      error: err
    }
  }
}

async function mutate (request) {
  const brand_shop_uid = request?.params?.brand_shop_uid
  const uid = request.params.uid
  const { fields, files } = request.body
  // let brand_shop = ''
  if(brand_shop_uid && uid.startsWith('_:')) {
    fields.set += `\r\n<${brand_shop_uid}> <dgraph.type> "BrandShop" .\r\n<${brand_shop_uid}> <brand_shop.video_stream> <_:new_video_stream> .`
  }
  await db.mutate(fields)
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'live_stream')
    }
  }
  return { statusCode: 200 }
}


module.exports = {
  video_stream_route: [
    ["get", "/brand-shop/video-stream", getAll],
    ["get", "/brand-shop/video-stream/:uid", getStreamUId],
    ["post", "/brand-shop/video-stream/:uid/:brand_shop_uid", mutate],
    ["post", "/brand-shop/video-stream-filter", streamFilter],
    ["post", "/brand-shop/video-stream/change-status", streamChangeStatus]
  ]
}