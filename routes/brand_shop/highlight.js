"use strict"
const db = require("#/services/tvcdb")
const { saveMediaFiles } = require("#/modules/uploadFileHandle")

const imageFolder = "brand-shop-highlight"

const tableFields = `
  uid
  is_deleted
  collection_name
  collection_image
  collection_icon
  background_image
  collection_type
  display_order
  sort_order
  layout_type
  parent  @filter(not eq(is_deleted, true) AND not eq(display_status, 1)){
    uid
    collection_name
  }
  children: ~parent @facets(orderasc: display_order) @filter(not eq(is_deleted, true) AND not eq(display_status, 1)) {
    uid
    collection_name
  }
  products: ~product.collection @facets(orderasc: display_order)  @filter(not eq(is_deleted, true) AND eq(display_status, 2)) {
    uid
    product_name
  }
  highlight.products @facets(orderasc: display_order) {
    uid
    product_name
  }
  highlight.collections @facets(orderasc: display_order) {
    uid
    collection_name
  }
  voucher.customers (orderasc: display_order) {
    uid
    expand(_all_)
  }
  ~highlight.collections {
    uid
    collection_name
  }
  brand_shop: ~brand_shop.highlight {
    uid
    brand_shop_name
  }
  type_highlight
  color_title
  color_price
  color_discount
  highlight_name
  image_highlight
  display_status
  reference_type
  is_temporary
  target_type
  condition_type
  condition_value
  voucher_type
  voucher_code
  voucher_value
  max_applied_value
  voucher_label
  start_at
  stop_at
  redeem
  is_internal
`

async function handleFormHighlight (request) {
  const brand_shop_uid = request?.params?.uid
  if(!request.isMultipart() || !brand_shop_uid) {
    throw new Error('multipart request only or not brand shop uid')
  }

  const { fields, files } = request.body

  const { del, set, childSort } = fields

  if(del) {
    const delData = JSON.parse(del)
    await db.mutate({ del: {
      uid: brand_shop_uid,
      "brand_shop.highlight": delData
    } })
  }
  if (childSort) {
    const sortData = JSON.parse(childSort)
    await db.mutate({ set: [...sortData] })
  }
  if(set) {
    const setData = JSON.parse(set)
    if(!setData?.uid || !setData?.uid.length) {
      setData['created_at'] = new Date().getTime();
    }else {
      setData['updated_at'] = new Date().getTime();
    }
    const uid = await db.mutate({
      set: {
        uid: brand_shop_uid,
        "brand_shop.highlight": {
          uid: "_:new_collection",
          "dgraph.type": "Collection",
          "collection_type": set.collection_type || 0,
          ...setData
        }
      }
    }).then(res => setData.uid || res.getUidsMap().get("new_collection"))
    if(Object.keys(files).length > 0) {
      const fields_files = saveMediaFiles(files, imageFolder)
      db.mutate({
        set: {
          uid,
          ...fields_files
        }
      })
    }

    return {
      statusCode: 200,
    }
  }
  return { statusCode: 200 }
}

async function getBrandShopHighlight ({ body }) {
  log.debug({ body })
  const { number = 20, page = 0, func = '', filter = '' } = body
  const funcStr = func !== '' ? `
      c as summary(${func}) @filter(type(Collection) AND has(~brand_shop.highlight)${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }` : `
      c as summary(func: type(Collection)) @filter(has(~brand_shop.highlight) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }`
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    result(func: uid(c), orderasc: display_order, first: $number, offset: $offset) @filter(type(Collection) AND has(~brand_shop.highlight) AND NOT eq(is_deleted, true)${filter})  {
      ${tableFields}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { data: result, summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page } }
}

async function getBrandShopHighlightDetail ({ query, params }) {
  const highlight_id = params?.uid;
  const { t, is_temp, reference_type, is_parent } = query
  let filter = reference_type ? `AND NOT eq(reference_type, ${reference_type})` : ''
  filter += is_temp && is_temp === "true" ? ` AND eq(is_temporary, true)` : is_temp && is_temp === "false" ? ` AND NOT eq(is_temporary, true)` : ''
  filter += t ? ` AND eq(collection_type, ${t})` : ''
  filter += (typeof is_parent === "undefined") ? '' : is_parent === "true" ? `AND NOT has(parent)` : 'AND NOT has(~parent)'
  const queryStr = `{
    result(func: uid(${highlight_id})) @filter(type(Collection) AND has(~brand_shop.highlight) AND NOT eq(is_deleted, true)${filter}) {
      ${tableFields}
    }
  }`
  const { result: [highlight] } = await db.query(queryStr)
  if(highlight)
    return db.parseArrayFacet(highlight)
  return {}

}


async function getProductsOptions ({ body }) {
  if(!body) {
    throw new Error('What are you doing here?')
  }
  const { fulltext_search, query_str, brand_shop_uid } = body

  if (!fulltext_search || fulltext_search === "") {
    throw new Error("fulltext_search is not allowed empty value!")
  }

  if(!brand_shop_uid) {
    return db.query(`
      {
        result(func: alloftext(fulltext_search, "${fulltext_search.replace(/[`'"\\]/g, '')}") , first: 50, offset: 0) @filter(${query_str ? query_str + ' AND ' : ''}NOT has(~brand_shop.product) AND type(Product) AND not eq(is_deleted, true) AND eq(display_status, 2)) {
          uid
          product_name
          sku_id
        }
      }`)
  } else
    return db.query(`
      {
        var(func: uid(${brand_shop_uid})) @filter(type(BrandShop)){
          uid
          brand_shop.product {
            c as uid
          }
        }
        result(func: alloftext(fulltext_search, "${fulltext_search.replace(/[`'"\\]/g, '')}") , first: 50, offset: 0) @filter(${query_str ? query_str + ' AND ' : ''}uid(c) AND type(Product) AND not eq(is_deleted, true) AND (eq(display_status, 2) OR eq(display_status, 1))) {
          uid
          product_name
          sku_id
        }
      }`)

}

module.exports = {
  highlight_route: [
    ["post", "/brand-shop/highlight-list", getBrandShopHighlight], // Lấy danh sách high light của brand shop
    ["get", "/brand-shop/highlight/:uid", getBrandShopHighlightDetail], //Lấy thông tin chi tiết highlight brand shop
    ["post", "/brand-shop/highlight/:uid", handleFormHighlight], // Cập nhật thông tin highlight brand shop,
    ["post", "/brand-shop/product-option", getProductsOptions], // Lấy danh sách sản phẩm
  ]
}