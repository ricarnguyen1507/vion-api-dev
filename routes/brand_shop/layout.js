"use strict"
const db = require("#/services/tvcdb")

const layoutField = `
  uid
  layout_name
  display_status
  created_at
  is_deleted
  is_default
  display_order
  target_type
  layout.layout_type
  brand_shop: ~brand_shop.layout {
    uid
    brand_shop_name
  }
  layout.layout_section @facets(orderasc: display_order) {
    uid
    expand(_all_)
  }
  layout.customers {
    uid
    expand(_all_)
  }
  layout.group_customers{
    uid
    expand(_all_)
  }
  layout.hybrid_layout {
    uid
    layout_name
  }
`

async function getBrandShopLayout ({ body }) {
  const { number = 20, page = 0, func = '', filter = '' } = body
  const funcStr = func !== '' ? `
      c as summary(${func}) @filter(type(Layout) AND has(~brand_shop.layout) AND not eq(is_deleted, true) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }` : `
      c as summary(func: type(Layout)) @filter(has(~brand_shop.layout) AND not eq(is_deleted, true) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }`
  const { result, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    result(func: uid(c), orderasc: display_order, first: $number, offset: $offset) {
      ${layoutField}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return {
    result: db.parseFacetArray(result),
    summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page }
  }
}
async function getLayoutDetail ({ params }) {
  const layout_id = params?.layout_id;
  const queryStr = `{
    result(func: uid(${layout_id})) @filter(type(Layout) AND has(~brand_shop.layout) AND NOT eq(is_deleted, true)) {
      ${layoutField}
    }
  }`
  const { result: [layout] } = await db.query(queryStr)
  return layout ?? {}
}


async function mutate (request) {
  const brand_shop_uid = request?.params?.brand_shop_uid
  let uid = request.params.uid
  const { fields } = request.body
  const res = await db.mutate(fields)
  uid = uid.startsWith('_:') ? res.getUidsMap().get('new_layout') : uid
  if(brand_shop_uid) {
    const brand_shop_product = {
      "dgraph.type": "BrandShop",
      "uid": brand_shop_uid,
      "brand_shop.layout": { uid: uid }
    }
    await db.mutate({ set: brand_shop_product })
  }
  return { statusCode: 200 }
}

module.exports = {
  layout_route: [
    ["post", "/brand-shop/layout-list", getBrandShopLayout], // Lấy danh sách layout của brand shop
    ["post", "/brand-shop/layout/:uid/:brand_shop_uid", mutate], // Cập nhật layout
    ["get", "/brand-shop/layout-detail/:layout_id", getLayoutDetail], // Cập nhật layout
  ]
}