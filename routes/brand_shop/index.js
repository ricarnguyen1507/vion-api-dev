"use strict"
const db = require("#/services/tvcdb")
const { highlight_route } = require("#/routes/brand_shop/highlight")
const { layout_route } = require("#/routes/brand_shop/layout")
const { product_route } = require("#/routes/brand_shop/products")
const { collection_route } = require("#/routes/brand_shop/collections")
const { video_stream_route } = require("#/routes/brand_shop/video-stream")
const { brandshop_group_route } = require("#/routes/brand_shop/brandshop_group")
const { saveMediaMD5 } = require("#/modules/uploadFileHandle")

async function getAll () {
  const { result } = await db.query(`{ result(func: type(BrandShop)){
    uid
    brand_shop_name
    brand_shop_description
    display_name_in_product
    display_name
    display_name_detail
    hotline
    certification_tag
    image_banner
    image_logo
    brand_shop.collection @facets(orderasc: display_order) {
      uid
      collection_name
    }
    description
    description_html
    display_status
    created_at
    is_deleted
    updated_at
  }}`)

  return {
    result: db.parseFacetArray(result)
  }
}
// const query =`
//   uid
//   brand_shop_name
//   display_name
//   display_name_detail
//   brand_shop.product{
//     uid
//     product_name
//   }
//   image_details{
//     uid
//     media_type
//     source
//   }
//   display_status
//   image_banner
//   image_logo
//   brand_shop_id
//   description_html
//   is_deleted
//   hotline
//   updated_at
//   created_at
// `
const query = `
  uid
  brand_shop_name
  display_name
  display_name_detail
  display_name_in_product
  brand_shop_description
  brand_shop.collection @filter(type(Collection) AND eq(collection_type, 400) AND NOT eq(is_deleted, true)) @facets(orderasc: display_order) {
    uid
    collection_name
  }
  display_status
  image_banner
  image_logo
  brand_shop_id
  description_html
  is_deleted
  hotline
  updated_at
  created_at
  skin_name
  skin_image
  skin_display_at_home
  skin_display_at_brandshop
  certification_tag
  brandShop.partner @filter(type(Partner)){
    uid
    dgraph.type
  }
  brandShop.group_payment @filter(type(GroupPayment)){
    uid
    dgraph.type
  }
  short_description_1 @filter(type(ShortDescription)){
    uid
    display_name
  }
  short_description_2 @filter(type(ShortDescription)){
    uid
    display_name
  }
  short_description_3 @filter(type(ShortDescription)){
    uid
    display_name
  }
  short_description_4 @filter(type(ShortDescription)){
    uid
    display_name
  }
  brandShop.manufacturer @filter(type(Manufacturer)){
    uid
    dgraph.type
  }
`
async function getADetail (request) {
  const uid = request.params.uid
  const { result: [data] } = await db.query(`{ result(func: uid(${uid})) @filter(type(BrandShop)) {
     ${query}
  }}`)
  if(!data) {
    throw { statusCode: 404, message: `BrandShop ${uid} not exist` }
  }
  return {
    status: 200,
    result: data
  }
}

async function mutate (request) {
  // let uid = request.params.uid
  const { fields, files } = request.body
  await db.mutate(fields)
  // uid = uid.startsWith('_:') ? res.getUidsMap().get('new_brand_shop') : uid
  if(Object.keys(files).length > 0) {
    for(const file of Object.entries(files)) {
      saveMediaMD5(file, 'brand_shop')
    }
  }
  return {
    status: 200
  }
}

async function brandShopFilter ({ body }) {
  const { number = 20, page = 0, func = '', filter = '' } = body
  const funcStr = func !== '' ? `
      c as summary(${func}) @filter(type(BrandShop) ${filter?.length ? 'AND ' + filter : ''}) {
        total: count(uid)
      }` : `
      c as summary(func: type(BrandShop)) @filter(${filter}) {
        total: count(uid)
      }`
  const { data, summary } = await db.query(`query result($number: int, $offset: int) {
    ${funcStr}
    data(func: uid(c), first: $number, offset: $offset)  {
      ${query}
    }
  }`, {
    $number: String(number),
    $offset: String(page * number)
  })
  return { data: db.parseFacetArray(data), summary: { totalCount: summary?.[0]?.total ?? 0, offset: number * page, page: page } }
}
async function getBrandShopProduct ({ query }) {
  const { number = -1, page = 0, filter = '', brand_shop_id, func = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`

  const { result, summary } = brand_shop_id.startsWith('_:') ? [] : await db.query(`{
    var(func: uid(${brand_shop_id})) @filter(type(BrandShop)) {
      uid
      products as brand_shop.product
    }
    brand_product as summary(func:uid(products)){
      totalCount: count(uid)
    }
    result(func: uid(brand_product)${paginate})@filter(not eq(is_deleted, true)${func}${filter}){
      uid
      product_name
      sku_id
      display_status
      product.pricing (first: -1) {
        uid
        price_with_vat
      }
    }
  }`)
  return { result, summary }
}
async function getOptions (request) {
  if(!request.body) {
    throw new Error('What are you doing here?')
  }
  const { brand_shop_name } = request.body
  const query = `query q($text: string) {
    result(func: alloftext(brand_shop_name, $text) , first: 50, offset: 0) @filter(type(BrandShop) AND not eq(is_deleted, true) AND (eq(display_status, 2) OR eq(display_status, 3))) {
      uid
      brand_shop_name
    }
  }`
  return db.query(query, { $text: brand_shop_name })
}
async function getSelected (request) {
  if(!request.body) {
    throw new Error('What are you doing here?')
  }
  const { brand_shop_name } = request.body
  const query = `query q($text: string) {
    result(func: eq(brand_shop_name, $text) , first: 50, offset: 0) @filter(type(BrandShop) AND not eq(is_deleted, true) AND (eq(display_status, 2) OR eq(display_status, 3))) {
      uid
      brand_shop_name
    }
  }`
  return db.query(query, { $text: brand_shop_name })
}

async function getOptionAvailable () {
  const query = `{
    result(func: type(BrandShop) , first: 50, offset: 0) @filter(NOT eq(is_deleted, true) AND eq(display_status, 2)) {
      uid
      brand_shop_name
    }
  }`
  return db.query(query)
}
module.exports = [
  ["get", "/brand-shop", getAll], // Lấy danh sách brand shop
  ["post", "/brand-shop:uid", mutate], // Cập nhật thông tin brand shop
  ["get", "/brand-shop/:uid", getADetail], //Lấy thông tin chi tiết brand shop
  ["post", "/brand-shop-filter", brandShopFilter], //Lấy dánh sách brand shop có filter
  ["get", "/brand-shop/product", getBrandShopProduct],
  ['post', '/list/brandShop-option', getOptions],
  ['post', '/list/brandShop-select', getSelected],
  ['get', '/list/brandShop-option/available', getOptionAvailable],
  ...highlight_route, //Highlight Brand Shop
  ...layout_route, // Layout Brand Shop
  ...product_route, // Sản phẩm
  ...collection_route,
  ...video_stream_route,
  ...brandshop_group_route
]
