"use strict"
const db = require("#/services/tvcdb")

async function getProductNoHasBrandShopOptions (request) {
  if(!request.body) {
    throw new Error('What are you doing here?')
  }
  const { fulltext_search, collection_uid } = request.body
  return db.query(collection_uid ? `{
    var(func: uid(${collection_uid})) {
      c as uid
      ~parent {
        cc as uid
      }
    }
    var(func: uid(c,cc)) @filter(type(Collection) AND not eq(is_deleted, true)) {
      ~product.collection {
        p as uid
      }
    }
    result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(uid(p) AND NOT has(~brand_shop.product) AND type(Product) AND not eq(is_deleted, true)){
      uid
      product_name
      sku_id
    }
  }` : `{
    result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(type(Product) AND NOT has(~brand_shop.product) AND not eq(is_deleted, true)){
      uid
      product_name
      sku_id
    }
  }`)
}


async function getProductsOptions ({ query, params }) {
  const brand_shop_uid = params?.brand_shop_uid;
  const { fulltext_search = '', collection_uid } = query;

  if(brand_shop_uid === 'all') {
    return db.query(collection_uid ? `
    {
      var(func: uid(${collection_uid})) {
        c as uid
        ~parent {
            cc as uid
        }
      }
      var(func: uid(c,cc)) @filter(type(Collection) AND eq(display_status, 2) AND not eq(is_deleted, true)) {
        ~product.collection {
            p as uid
        }
      }
      result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(uid(p) AND has(~brand_shop.product) AND type(Product) AND not eq(is_deleted, true)) {
        uid
        product_name
        sku_id
      }
    }` : `{
      result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(type(Product) AND has(~brand_shop.product) AND not eq(is_deleted, true)) {
        uid
        product_name
        sku_id
      }
    }`)
  } else {
    return db.query(collection_uid ? `{
        var(func:uid(${brand_shop_uid})) {
            brand_shop.product {
                p as uid
            }
        }
        var(func: uid(${collection_uid})) {
            c as uid
            ~parent {
                cc as uid
            }
        }
        var(func: uid(c,cc)) @filter(type(Collection) AND eq(display_status, 2) AND not eq(is_deleted, true)) {
            ~product.collection {
                p as uid
            }
        }
        result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(uid(p) AND uid(p) AND type(Product) AND not eq(is_deleted, true)) {
            uid
            product_name
            sku_id
        }
    }` : `{
        var(func:uid(${brand_shop_uid})) {
            brand_shop.product {
                p as uid
            }
        }
        result(func: alloftext(fulltext_search, "${fulltext_search}") , first: 50, offset: 0) @filter(uid(p) AND type(Product) AND not eq(is_deleted, true)) {
            uid
            product_name
            sku_id
        }
    }`)
  }
}
async function getProductByUids ({ body, params }) {
  const brand_shop_uid = params?.brand_shop_uid;
  return brand_shop_uid != null ? db.query(`{
    var(func: uid(${brand_shop_uid})){
      c as brand_shop.product @filter(not eq(is_deleted, true) AND (eq(display_status, 2) OR eq(display_status, 1)))
    }
    result(func: uid(${body.fields.uid})) @filter(uid(c)) {
        uid
        product_name
        sku_id
      }
    }`) : {}
}
module.exports = {
  product_route: [
    ["get", "/brand-shop/products-list/:brand_shop_uid", getProductsOptions],
    ['post', '/brand-shop/list/product-option', getProductNoHasBrandShopOptions], // Lấy danh sách layout của brand shop
    ['post', '/brand-shop/list/product-by-uids/:brand_shop_uid', getProductByUids], // Lấy danh sách layout của brand shop
  ]
}