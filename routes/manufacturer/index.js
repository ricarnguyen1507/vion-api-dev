"use strict"
const db = require("#/services/tvcdb")

async function mutate ({ body }) {
  await db.mutate(body)
  return { statusCode: 200 }
}
async function getAll () {
  return db.query(`{result(func: type(Manufacturer)) @filter(NOT eq(is_deleted, true)){uid,expand(_all_)}}`)
}

async function getTable ({ body }) {
  const { page = 0, pageSize = 20 } = body
  const { primaryFilters, filters } = db.parseFilters(body.filters)

  const str1 = primaryFilters.length ? ["type(Manufacturer)", ...primaryFilters].join(', ') : "type(Manufacturer)"
  const str2 = filters.length ? `@filter(${[...filters].join(" AND ")})` : ''

  const { summary, data } = await db.query(`query result($number: string, $offset: string) {
    manufacturers as summary(func: ${str1}) ${str2}{
      total: count(uid)
    }
    data(func: uid(manufacturers), first: $number, offset: $offset) {
      uid,
      expand(_all_)
    }
  }`, {
    $number: String(pageSize),
    $offset: String(page ? page * pageSize : 0)
  })
  return {
    totalCount: summary[0]?.total || 0,
    data
  }
}
async function loadMore () {
  const { result, summary } = await db.query(`
    summary(func: type(Manufacturer)) {
      totalCount: count(uid)
    }
    result(func: type(Manufacturer), first: $number, offset: $offset){uid,expand(_all_)}
    `)
  return { result, summary }
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Manufacturer)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['post', '/list/manufacturer', getTable],
  ['get', '/list/manufacturer', getAll],
  ['get', '/list/manufacturer/:number/:page', loadMore],
  ['get', '/manufacturer/:uid', getByUid],
  ['post', '/manufacturer', mutate]
]