"use strict"
const db = require("#/services/tvcdb")

const fields = `
  uid
  expand(_all_) {
    uid
    expand(_all_) {
      uid
      expand(_all_)
    }
  }
`

function getByUid(request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Address)) {${fields}}
  }`, { $uid })
}

module.exports = [
  ['get', '/address/:uid', getByUid],
]