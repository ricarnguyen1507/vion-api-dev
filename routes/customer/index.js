"use strict"
const db = require("#/services/tvcdb")

async function mutate ({ body }) {

  if(body.set) {
    const { address, alt_info } = body.set

    if(address) {
      db.addType("Address", address)
    }
    if(alt_info) {
      alt_info.forEach(addr => {
        db.addType("ContactInfo", addr);
        if(addr?.address) {
          db.addType("Address", addr?.address)
        }
      })
    }
    db.addType("Customer", body.set)
    if (body.del?.uid) {
      await db.mutate({ del: body.del })
    }
    const result = await db.mutate({ set: body.set })
    // không hiểu sao truyền cả body vào thì lại chạy ko đúng :)))
    // const uid = result.getUidsMap().get("new_customer") || (body.set && body.set.uid)

    // process.emit('sync', uid, "customer")

    return db.getUids(result, { statusCode: 200 })
  }
}

async function deleteCustomer ({ params: { uid } }) {
  if(!uid) {
    throw new Error("What are you doing here?")
  }
  await db.mutate({
    set: { uid, is_deleted: true }
  })
  // process.emit('sync', uid, "customer")
  return "success"
}

const fields = `
  uid
  expand(_all_) {
    uid
    expand(_all_) {
      uid
      expand(_all_)
    }
  }
`

// function getAll() {
//   return db.query(`{
//     result(func: type(Customer)) {${fields}}
//   }`)
// }
async function getAll ({ query }) {
  const { pageSize = -1, page = 0, filter = '' } = query
  const paginate = pageSize == -1 ? '' : `, first: ${String(pageSize)}, offset: ${String(pageSize * page)}`
  const { result, summary } = await db.query(`{
    customers as summary(func: type(Customer)) @filter(NOT eq(is_deleted, true) ${filter}) {
      totalCount: count(uid)
    }
    result(func: uid(customers)${paginate}){${fields}}
  }`)
  return { result, summary }
}
// function getLoadMoreTable({ body }) {
//   const { pageSize, page, filter = ''} = body
//   return db.query(`{
//     customers as summary(func: type(Customer)) @filter(not eq(is_deleted, true)${filter}) {
//       totalCount: count(uid)
//     }
//     data(func: uid(customers), first: ${pageSize}, offset: ${pageSize*page}) {
//       uid
//       expand(_all_){
//         uid
//         expand(_all_){
//            uid
//            expand(_all_){
//              uid
//               expand(_all_)
//            }
//         }
//       }
//     }
//   }`)
// }

function loadMore (request) {
  const { $offset = 20, $number = 0 } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(Customer), first: $number, offset: $offset) {${fields}}
  }`, { $number, $offset })
}

function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(Customer)) {${fields}}
  }`, { $uid })
}
const getOptions = {
  schema: {
    body: {
      phone_number: { type: 'string' },
      customer_name: { type: 'string' }
    }
  },
  async handler (request) {
    if(!request.body) {
      throw new Error("What are you doing here")
    }
    const { phone_number, customer_name } = request.body

    if(3 > (phone_number || customer_name)?.length) {
      return { result: [] }
    }

    let primary_filter
    if(phone_number) {
      primary_filter = `regexp(phone_number, /${phone_number}/)`
    } else if(customer_name) {
      primary_filter = `anyoftext(customer_name, "${customer_name}")`
    }
    if(primary_filter) {
      return db.query(`{
        result(func: ${primary_filter}) @filter(type(Customer) AND not eq(is_deleted, true)) {
          uid
          phone_number
          customer_name
        }
      }`)
    }
  }
}

async function getOptionsText (request) {
  if (!request.body) {
    throw new Error("What are you doing here")
  }
  const { fulltext_search } = request.body;
  const queryPhone = `phone(func: regexp(phone_number, /${fulltext_search}/)) @filter(type(Customer) AND not eq(is_deleted, true)) {
        uid
        phone_number
        customer_name
      }`
  const query = `{
      ${fulltext_search.length > 2 ? queryPhone : ''}
      name(func: anyoftext(customer_name, "${fulltext_search}")) @filter(type(Customer) AND not eq(is_deleted, true)) {
        uid
        phone_number
        customer_name
      }
    }`
  const { name = [], phone = [] } = await db.query(query)
  return { result: [...new Set([...name, ...phone].map(item => JSON.stringify(item)))].map(item => JSON.parse(item)) };
}
// Get 200 row for notify option
function getListOption () {
  const number = 50
  return db.query(`query result($number: string){
    result(func: type(Customer), orderdesc: phone_number, first: $number) {
      uid
      phone_number
    }
  }`, { $number: String(number) })
}

module.exports = [
  ['delete', '/customer/:uid', deleteCustomer],
  ['get', '/list/customer', getAll],
  // ['post', '/list/customer', getLoadMoreTable],
  ['get', '/list/customer-option/notify', getListOption],
  ['get', '/list/customer/:offset/:number', loadMore],
  ['get', '/customer/:uid', getByUid],
  ['post', '/customer', mutate],
  ['post', '/list/customer-options', getOptions],
  ['post', '/list/customer-options-text', getOptionsText]
]