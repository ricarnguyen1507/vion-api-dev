"use strict"

const db = require("#/services/tvcdb")

async function productFilter () {
  // const { number = 20, page = 0, is_temporary = null, filter = '', func = '' } = body
  return await db.query(`{
    result(func: type(LogImport)) {
      date_import
      updatedBy{
        user_name
      }
      display_status
      type_import
      path
      pathErr
      import_status
      item_volume
    }
  }`)
}
module.exports = [
  ['get', '/list/dialog', productFilter],
]