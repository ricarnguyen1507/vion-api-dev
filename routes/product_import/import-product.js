const db = require("#/services/tvcdb")
const fs = require("fs")

const productXlsFieldNames = [
  "uid",
  "sku_id",
  "partner_uid",
  "manufacturer_uid",
  "brand_uid",
  "collection_uid",
  "UOM_uid",
  "UOM_name",
  "group_payment",
  "region_uid",
  "short_description_1",
  "short_description_2",
  "short_description_3",
  "short_description_4",
  "product_tag_1",
  "product_tag_2",
  "product_tag_3",
  "product_tag_4",
  "brand_shop_uid",
  "brand_shop_collection_uid",
  "specs_type_uid_1",
  "specs_type_uid_2",
]
async function prepareBrandShop (products) {
  return products.filter(p => 'brand_shop.product' in p).map(p => ({
    "dgraph.type": "BrandShop",
    "uid": p['brand_shop.product'],
    "brand_shop.product": { uid: p.uid }
  }))
}
function parseRow (fields, rowData) {
  // PREPARE DEFAULT DATA FOR ROW DATA

  if (rowData?.region_uid) {
    rowData.region_uid = rowData?.region_uid.map(a => a).join(",")
  }
  if (rowData?.ott_uid) {
    rowData.ott_uid = rowData?.ott_uid.map(a => a).join(",")
  }
  // END PREPARE REGION DATA

  // PREPARE COOLECTION DATA
  if (rowData?.collection_uid) {
    /**try to fix this */
    rowData.collection_uid = rowData?.collection_uid?.map(a => a).join(",")

  }
  if (rowData?.manufacturer_uid) {
    /**try to fix this */
    rowData.manufacturer_uid = rowData?.manufacturer_uid?.map(a => a).join(",")

  }
  if (rowData?.brand_uid) {
    /**try to fix this */
    rowData.brand_uid = rowData?.brand_uid?.map(a => a).join(",")

  }
  if (rowData?.partner_uid) {
    /**try to fix this */
    rowData.partner_uid = rowData?.partner_uid?.map(a => a).join(",")

  }
  if (rowData?.UOM_uid) {
    /**try to fix this */
    rowData.UOM_uid = rowData?.UOM_uid?.map(a => a).join(",")
  }
  if (rowData?.specs_type_uid_1) {
    /**try to fix this */
    rowData.specs_type_uid_1 = rowData?.specs_type_uid_1?.map(a => a).join(",")
  }
  if (rowData?.specs_type_uid_2) {
    /**try to fix this */
    rowData.specs_type_uid_2 = rowData?.specs_type_uid_2?.map(a => a).join(",")
  }
  if (rowData?.short_description_1) {
    /**try to fix this */
    rowData.short_description_1 = rowData?.short_description_1?.map(a => a).join(",")
  }
  if (rowData?.short_description_2) {
    /**try to fix this */
    rowData.short_description_2 = rowData?.short_description_2?.map(a => a).join(",")
  }
  if (rowData?.short_description_3) {
    /**try to fix this */
    rowData.short_description_3 = rowData?.short_description_3?.map(a => a).join(",")
  }
  if (rowData?.short_description_4) {
    /**try to fix this */
    rowData.short_description_4 = rowData?.short_description_4?.map(a => a).join(",")
  }
  if (rowData?.productTag1) {
    /**try to fix this */
    rowData.productTag1 = rowData?.productTag1?.map(a => a).join(",")
  }
  if (rowData?.productTag2) {
    /**try to fix this */
    rowData.productTag2 = rowData?.productTag2?.map(a => a).join(",")
  }
  if (rowData?.productTag3) {
    /**try to fix this */
    rowData.productTag3 = rowData?.productTag3?.map(a => a).join(",")
  }
  if (rowData?.productTag4) {
    /**try to fix this */
    rowData.productTag4 = rowData?.productTag4?.map(a => a).join(",")
  }
  // if(rowData?.brand_shop?.length) {
  //   rowData.brand_shop_uid = rowData?.brand_shop[0].uid
  //   let brand_shop_collection = rowData?.brand_shop[0]?.brand_shop_collection ?? []
  //   if(brand_shop_collection?.length) {
  //     let collection_product = brand_shop_collection?.filter(col => col?.["highlight.products"]?.find(prod => prod?.uid === rowData.uid)) ?? []
  //     if(collection_product.length) {
  //       rowData.brand_shop_collection_uid = collection_product[0]?.uid
  //     }
  //   }
  // }
  // PREPARE ROW DATA TO EXPORT
  return buildRowData(fields, rowData, '')
}
function uidExists(uid, result) {
  return result.some(function(el) {
    return el.uid == uid;
  });
}
async function checkNodeUID(data) {
  const errors = {}
  if(Object.keys(data)?.length) {
    for(const key in data) {
      const getError = []
      if(data[key]?.values?.length) {
        for(const uid of data[key].values) {
          if(!db.isUid(uid)) {
            data = data[key]?.values?.filter(function(e) { return e !== uid })
            getError.push(uid)
          }
        }
      }
      const { result } = await db.query(`{
        result(func: type(${data[key].type})) ${data[key]?.filter != '' ? data[key]?.filter : ''}{
            uid
            dgraph.type
          }
        }`)
      if(result?.length && data[key]?.values?.length) {
        for(const value of data[key].values) {
          const checkUid = uidExists(value, result)
          if(!checkUid) {
            getError.push(value)
          }
        }
        if(getError?.length) {
          errors[key] = getError
          // for(let value of getError) {
          //   errors.push(value)
          // }
        }
      }
    }
  }
  return errors
}
function buildRowData (fields, data, defaultValue) {
  const entries = fields.map(field => [field, data[field] ?? defaultValue])
  return Object.fromEntries(entries)
}

async function checkNodes(data) {
  let errObj = {}
  const arrManufacturer = new Set()
  let manuUID = []
  const arrPartner = new Set()
  let partnerUID = []
  const arrBrand = new Set()
  let brandUID = []
  const arrCollection = new Set()
  let colUID = []
  const arrUom = new Set()
  let uomUID = []
  const arrGroupPayment = new Set()
  let paymentUID = []
  const arrShortDes1 = new Set()
  let shortDes1UID = []
  const arrShortDes2 = new Set()
  let shortDes2UID = []
  const arrShortDes3 = new Set()
  let shortDes3UID = []
  const arrShortDes4 = new Set()
  let shortDes4UID = []
  const arrProdTag1 = new Set()
  let tag1UID = []
  const arrProdTag2 = new Set()
  let tag2UID = []
  const arrProdTag3 = new Set()
  let tag3UID = []
  const arrProdTag4 = new Set()
  let tag4UID = []
  const arrSpecs1 = new Set()
  let spec1UID = []
  const arrSpecs2 = new Set()
  let spec2UID = []
  const arrRegion = new Set()
  let regionUID = []
  const arrOtt = new Set()
  let ottUID = []
  // FILTER REMOVE DUPLICATE UID
  for (const obj of data) {
    obj?.region_uid?.trim()
    obj?.ott_uid?.trim()
    obj?.manufacturer_uid?.trim()
    obj?.brand_uid?.trim()
    obj?.partner_uid?.trim()
    obj?.collection_uid?.trim()
    obj?.UOM_uid?.trim()
    obj?.group_payment?.trim()

    // let checkManuExist = arrManufacturer?.some(m => obj.manufacturer_uid == m)
    arrManufacturer.add(obj.manufacturer_uid)
    manuUID = Array.from(arrManufacturer)

    arrPartner.add(obj.partner_uid)
    partnerUID = Array.from(arrPartner)

    arrBrand.add(obj.brand_uid)
    brandUID = Array.from(arrBrand)

    arrCollection.add(obj.collection_uid)
    colUID = Array.from(arrCollection)

    arrUom.add(obj.UOM_uid)
    uomUID = Array.from(arrUom)

    arrGroupPayment.add(obj.group_payment)
    paymentUID = Array.from(arrGroupPayment)

    if(obj?.short_description_1) {
      arrShortDes1.add(obj.short_description_1)
      shortDes1UID = Array.from(arrShortDes1)
    }

    if(obj?.short_description_2) {
      arrShortDes2.add(obj.short_description_2)
      shortDes2UID = Array.from(arrShortDes2)
    }

    if(obj?.short_description_3) {
      arrShortDes3.add(obj.short_description_3)
      shortDes3UID = Array.from(arrShortDes3)
    }

    if(obj?.short_description_4) {
      arrShortDes4.add(obj.short_description_4)
      shortDes4UID = Array.from(arrShortDes4)
    }

    if(obj?.productTag1) {
      arrProdTag1.add(obj.productTag1)
      tag1UID = Array.from(arrProdTag1)
    }

    if(obj?.productTag2) {
      arrProdTag2.add(obj.productTag2)
      tag2UID = Array.from(arrProdTag2)
    }

    if(obj?.productTag3) {
      arrProdTag3.add(obj.productTag3)
      tag3UID = Array.from(arrProdTag3)
    }
    if(obj?.productTag4) {
      arrProdTag4.add(obj.productTag4)
      tag4UID = Array.from(arrProdTag4)
    }

    if(obj?.specs_type_uid_1) {
      arrSpecs1.add(obj.specs_type_uid_1)
      spec1UID = Array.from(arrSpecs1)
    }

    if(obj?.specs_type_uid_2) {
      arrSpecs2.add(obj.specs_type_uid_2)
      spec2UID = Array.from(arrSpecs2)
    }

    const regionArr = obj?.region_uid.split(",");
    if(regionArr.length) {
      for(const region of regionArr) {
        arrRegion.add(region)
      }
      regionUID = Array.from(arrRegion)
    }

    const ottArr = obj?.ott_uid.split(",");
    if(ottArr.length) {
      for(const ott of ottArr) {
        arrOtt.add(ott)
      }
      ottUID = Array.from(arrOtt)
    }
  }
  const ObjCheck = {
    manufacturer_uid: { values: manuUID, type: 'Manufacturer', filter: '' },
    partner_uid: { values: partnerUID, type: 'Partner', filter: '' },
    brand_uid: { values: brandUID, type: 'Brand', filter: '' },
    collection_uid: { values: colUID, type: 'Collection', filter: '' },
    UOM_uid: { values: uomUID, type: 'UOM', filter: '' },
    group_payment: { values: paymentUID, type: 'GroupPayment', filter: '' },
    short_description_1: { values: shortDes1UID, type: 'ShortDescription', filter: '@filter(has(display_name) AND NOT eq(display_name, ""))' },
    short_description_2: { values: shortDes2UID, type: 'ShortDescription', filter: '@filter(has(display_name) AND NOT eq(display_name, ""))' },
    short_description_3: { values: shortDes3UID, type: 'ShortDescription', filter: '@filter(has(display_name) AND NOT eq(display_name, ""))' },
    short_description_4: { values: shortDes4UID, type: 'ShortDescription', filter: '@filter(has(display_name) AND NOT eq(display_name, ""))' },
    productTag1: { values: tag1UID, type: 'Tag', filter: '@filter(has(tag_title) AND NOT eq(tag_title, ""))' },
    productTag2: { values: tag2UID, type: 'Tag', filter: '@filter(has(tag_title) AND NOT eq(tag_title, ""))' },
    productTag3: { values: tag3UID, type: 'Tag', filter: '@filter(has(tag_title) AND NOT eq(tag_title, ""))' },
    productTag4: { values: tag4UID, type: 'Tag', filter: '@filter(has(tag_title) AND NOT eq(tag_title, ""))' },
    specs_type_uid_1: { values: spec1UID, type: 'SpecsType', filter: '@filter(has(specs_type_name) AND NOT eq(specs_type_name, ""))' },
    specs_type_uid_2: { values: spec2UID, type: 'SpecsType', filter: '@filter(has(specs_type_name) AND NOT eq(specs_type_name, ""))' },
    region_uid: { values: regionUID, type: 'Region', filter: '' },
    ott_uid: { values: ottUID, type: 'OTT', filter: '' },
  }
  errObj = await checkNodeUID(ObjCheck)

  return errObj
}
// PREPARE EDGE FOR IMPORT FILE
async function prepareEdges (basicFields) {
  if(basicFields.length > 0) {
    const checkNodeEgdesErr = await checkNodes(basicFields)
    return checkNodeEgdesErr
  }
}

async function prepareErrorImport (fileName, products) {
  if(Object.keys(products).length) {
    console.log("-- Create file Import Error --")
    const ws = fs.createWriteStream(`/tmp/${fileName}`)
    ws.write('\uFEFF' + productXlsFieldNames.join(",") + "\n")
    const csvRow = Object.values(parseRow(productXlsFieldNames, products)).map(cell => {
      if (typeof cell === 'string') {
        cell = cell.trim()
        if(cell.length > 0) {
          if(cell.replace(/ /g, '').match(/[\s,"]/)) {
            cell = cell.replace(/"/g, '""')
          }
          return '"' + cell + '"'
        }
      }
      return cell;
    }).join(',')
    ws.write(csvRow)
    ws.write("\n")
    ws.end()
    fs.copyFileSync(`/tmp/${fileName}`, `/public/export/${fileName}`)
    fs.rmSync(`/tmp/${fileName}`)
  } else {
    throw new Error("nothing to write")
  }
}

module.exports = {
  prepareBrandShop,
  prepareEdges,
  prepareErrorImport
}