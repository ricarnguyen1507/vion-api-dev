const fs = require('fs')
const path = require('path')
const db = require("#/services/tvcdb")
const session = require("#/modules/userSession")
const { mutate, mutateUpdate } = require("#/modules/db-import-product")
const {
  prepareEdges,
  prepareErrorImport
} = require("./import-product")

// FUNCTIONS EXECUTE IMPORT DATA FROM XLSX FILE
async function productImport (request, reply) {
  const user = await session.getUser(request);
  const date_import = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  const products = JSON.parse(request.body.fields.dataValid)

  const dataOrigin = request.body.files.file
  if (dataOrigin) {
    try {

      const userImportDir = path.join(config.import_dir, `product/${user.uid}`)

      if(!fs.existsSync(userImportDir)) {
        fs.mkdirSync(userImportDir, { recursive: true })
      }
      const filePath = path.join(userImportDir, `import_${(new Date()).getTime()}.xlsx`)
      fs.writeFileSync(filePath, fs.readFileSync(dataOrigin.filepath));


      // CREATE DIALOG INFORMATION
      const uploadInformation = {
        uid: '_:new_log',
        date_import: date_import,
        updatedBy: { uid: user.uid },
        display_status: 2,
        type_import: 1,
        path: filePath,
        item_volume: products.length,
        import_status: 'Pending'
      }
      db.addType("LogImport", uploadInformation)
      const res = await db.mutate({ set: uploadInformation })
      const logUID = res.getUidsMap().get('new_log')
      const errorLog = await prepareEdges(products)
      if(Object.keys(errorLog).length) {
        try {
          const fileName = `product_${Date.now()}.csv`
          prepareErrorImport(fileName, errorLog)
            .catch(err => {
              log.error("Export product", err)
            })
          const updateFileStatus = {
            uid: logUID,
            pathErr: `/export/${fileName}`,
            import_status: 'Error',
            "dgraph.type": "LogImport"
          }
          await db.mutate({ set: updateFileStatus })
          return {
            statusCode: 400,
            message: `File Import error`
          }
        } catch(err) {
          reply.send(err)
        }
      }else{
        //addition price_with_vat to order_by products
        if(products.length) {
          products.map(bf => {
            bf['price_with_vat'] = bf?.['product.pricing']?.price_with_vat || 0
          })
          mutate(products, logUID).catch(err => {
            log.error("Export product", err)
          });
          return {
            statusCode: 200,
            message: `Import successfully`
          }
        }
      }
      // END CREATE DIALOG INFORMATION
    } catch(err) {
      reply.send(err)
    }
  }
}

async function productImportUpdate (request, reply) {
  const user = await session.getUser(request);
  const date_import = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  // let errorObjList = []
  const products = JSON.parse(request.body.fields.dataUpdateValid)
  const dataOrigin = request.body.files.file
  if (dataOrigin) {
    try {
      const userImportDir = path.join(config.import_dir, `product/update/${user.uid}`)

      if(!fs.existsSync(userImportDir)) {
        fs.mkdirSync(userImportDir, { recursive: true })
      }
      const filePath = path.join(userImportDir, `import_${(new Date()).getTime()}.xlsx`)
      fs.writeFileSync(filePath, fs.readFileSync(dataOrigin.filepath));

      // ADD PRODUCT TO BRAND SHOP
      // let brandShop = await prepareBrandShop(products)
      // if (brandShop.length) {
      //   await db.mutate({ set: brandShop })
      // }
      // END ADD PRODUCT TO BRAND SHOP

      // REMOVE FIELD BRAND SHOP OUT OF THE DATA PRODUCT
      for (const obj of products) {
        if('brand_shop.product' in obj) {
          delete obj['brand_shop.product']
        }
      }
      // END REMOVE FIELD BRAND SHOP OUT OF THE DATA PRODUCT

      // CREATE DIALOG INFORMATION
      const uploadInformation = {
        uid: '_:new_log_update',
        date_import: date_import,
        updatedBy: { uid: user.uid },
        display_status: 2,
        type_import: 1,
        item_volume: products.length,
        path: filePath,
        import_status: 'Pending'
      }
      db.addType("LogImport", uploadInformation)
      const res = await db.mutate({ set: uploadInformation })
      const logUID = res.getUidsMap().get('new_log_update')
      const errorLog = await prepareEdges(products)
      if(Object.keys(errorLog).length) {
        try {
          const fileName = `product_${Date.now()}.csv`
          prepareErrorImport(fileName, errorLog)
            .catch(err => {
              log.error("Import product", err)
            })
          const updateFileStatus = {
            uid: logUID,
            pathErr: `/export/${fileName}`,
            import_status: 'Error',
            "dgraph.type": "LogImport"
          }
          await db.mutate({ set: updateFileStatus })
          return {
            statusCode: 400,
            message: `File Import Update error`
          }
        } catch(err) {
          reply.send(err)
        }
      }else{
      //addition price_with_vat to order_by products
        if(products.length) {
          // products.map(bf => {
          //   bf['price_with_vat'] = bf?.['product.pricing']?.price_with_vat || 0
          // })
          mutateUpdate(products, logUID).catch(err => {
            log.error("Import product", err)
          });
          return {
            statusCode: 200,
            message: `Import Update Successfully`
          }
        }
      }
    }catch(err) {
      reply.send(err)
    }
    // END CREATE DIALOG INFORMATION
  }
}

// CHECK SKU ID PRODUCT BETWEEN IMPORT EXCEL AND DATABASE (CHECK WITH CLIENT)
async function checkImportSKU (request) {
  const basicFields = JSON.parse(request.body.fields.checkSku);
  let arrError = []
  let limit = 0;
  if (basicFields.length > 0) {
    while (basicFields.length > 0) {
      limit = (basicFields.length > 200) ? 199 : basicFields.length;
      const tmp = basicFields.splice(0, limit);
      try {
        const objSku = await db.query(`{
          result(func: eq(sku_id, ${tmp}))@filter(type(Product)){
            sku_id
          }
        }`)
        if (objSku.result.length > 0) {
          arrError = objSku.result;
        }
      }
      catch (err) {
        log.error(err);
      }
    }
  }
  log.info('End check SKU', arrError.length, new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }))
  return arrError;
}

// END CHECK SKU ID PRODUCT BETWEEN IMPORT EXCEL AND DATABASE

// CHECK UID PRODUCT EXIST OR NOT
async function checkImportUpdateUID (request) {
  const basicFields = JSON.parse(request.body.fields.checkUID);
  const availableUID = [];
  let limit = 0;
  if (basicFields.length > 0) {
    while (basicFields.length > 0) {
      limit = (basicFields.length > 200) ? 199 : basicFields.length;
      const tmp = basicFields.splice(0, limit);
      try {
        const objUid = await db.query(`{
          result(func: uid(${tmp}))@filter(type(Product)){
            uid
          }
        }`)
        if (objUid.result.length > 0) {
          for (let i = 0; i < objUid.result.length; i++) {
            availableUID.push(objUid.result[i].uid)
          }
        }
      }
      catch (err) {
        log.error(err);
      }
    }
  }
  log.info('End check UID', availableUID.length, new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }))
  return availableUID;
}
// END CHECK UID PRODUCT EXIST OR NOT

// CHECK UID PRODUCT AND UID BRAND SHOP EXIST OR NOT
async function checkBrandShopUID (request) {
  const basicFields = JSON.parse(request.body.fields.checkBrandShopUID);
  const errBrandUID = [];
  let limit = 0;
  if (basicFields.length > 0) {
    while (basicFields.length > 0) {
      limit = (basicFields.length > 200) ? 199 : basicFields.length;
      const tmp = basicFields.splice(0, limit);
      try {
        for (const obj of tmp) {
          const { brandShop: [info] } = await db.query(`{
            brandShop(func: uid(${obj['brand_shop.product']}))@filter(type(BrandShop)){
              uid
              brand_shop.product @filter(type(Product)){
              uid
              product_name
            }
            }
          }`)
          // xài ? như hạch vậy tưng, ko rảnh thì đọc kỹ đi rồi xài, tấu hài quá ko tốt nha
          // if (info && info['brand_shop.product']?.length) {
          if (info?.['brand_shop.product']?.length) {
            for (const objProduct of info['brand_shop.product']) {
              if (obj.uid == objProduct.uid) {
                errBrandUID.push(obj.uid)
              }
            }
          } else {
            errBrandUID.push(obj.uid)
          }
        }
      }
      catch (err) {
        log.error(err);
      }
    }
  }
  return errBrandUID;
}
// END CHECK UID PRODUCT AND UID BRAND SHOP EXIST OR NOT

// CHECK EDGE FOR PRODUCT BEFORE IMPORT
// async function checkNodeEdge (request) {
//   console.log("start check edge")
//   let basicFields = JSON.parse(request.body.fields.dataNodeEdge);
//   if (basicFields.length > 0) {
//     let errorLog = await prepareEdges(basicFields)
//     return errorLog;
//   }
//   return {
//     statusCode: 200,
//     data: 'No data to check'
//   }
// }
module.exports = [
  ['post', '/product/import', productImport],
  ['post', '/product/check_sku', checkImportSKU],
  ['post', '/product/check_uid', checkImportUpdateUID],
  ['post', '/product/importUpdate', productImportUpdate],
  ['post', '/product/check/brandShop_uid', checkBrandShopUID],
  // ['post', '/product/check/nodeEdge', checkNodeEdge]
]