"use strict"
const db = require("#/services/tvcdb")

async function getAll ({ query }) {
  const { number = -1, page = 0, filter = '' } = query
  const paginate = number == -1 ? '' : `, first: ${String(number)}, offset: ${String(number * page)}`
  const { result, summary } = await db.query(`{
      channels as summary(func: type(Channel))  @filter(NOT eq(is_deleted, true)${filter}) {
        totalCount: count(uid)
      }
      result(func: uid(channels)${paginate}) {
        uid
        expand(_all_)
      }
    }`)
  return { result, summary }
}

async function mutate (request) {
  await db.mutate(request.body)
  return { statusCode: 200 }
}

async function getOptions (request) {
  if(!request.body) {
    throw new Error('What are you doing here?')
  }
  const { fulltext_search } = request.body
  return db.query(`{
    result(func: alloftext(channel.name, "${fulltext_search}") , first: 50, offset: 0) @filter(type(Channel)) {
      uid
      expand(_all_)
    }
  }`)
}

module.exports = [
  ["get", "/stream-channel", getAll],
  ["post", "/stream-channel-option", getOptions],
  ["post", "/stream-channel", mutate]
]
