"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}
async function getAll ({ body }) {
  const { number = 20, page = 0 } = body
  const { result, summary } = await db.query(`query result($number: int, $offset: int){
      platforms as summary(func: eq(collection_type, 100)) @filter(type(Collection)) {
        totalCount: count(uid)
      }
      result(func: uid(platforms), first: $number, offset: $offset){
        uid,
        expand(_all_)
      }
    }`, {
    $number: String(number),
    $offset: String(number * page)
  })
  return { result, summary }
}

module.exports = [
  ['post', '/list/platform', getAll],
  ['post', '/platform', mutate]
]