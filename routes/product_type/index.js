"use strict"
const db = require("#/services/tvcdb")

async function mutate (request) {
  await db.mutate(request.body.fields)
  return { statusCode: 200 }
}
function getAll () {
  return db.query(`{data(func: type(ProductType)){uid,expand(_all_)}}`)
}
async function getDataTable ({ body }) {
  const { number, page } = body
  const { result, summary } = await db.query(`query result($number: int, $offset: int){
      productTypes as summary(func: type(ProductType)) {
        totalCount: count(uid)
      }
      result(func: uid(productTypes), first: $number, offset: $offset){uid,expand(_all_)}
    }`, {
    $number: String(number),
    $offset: String(number * page)
  })
  return { result, summary }
}
function loadMore (request) {
  const { offset, number } = request.params
  return db.query(`query result($number: string, offset: string) {
    result(func: type(ProductType), first: $number, offset: $offset){uid,expand(_all_)}
  }`, {
    $number: number || 20,
    $offset: offset || 0
  })
}
function getByUid (request) {
  const $uid = request.params.uid
  return db.query(`query result($uid: string) {
    result(func: uid($uid)) @filter(type(ProductType)){
      uid
      expand(_all_)
    }
  }`, { $uid })
}

module.exports = [
  ['get', '/list/product_type', getAll],
  ['post', '/list/product_type', getDataTable],
  ['get', '/list/product_type/:offset/:number', loadMore],
  ['get', '/product_type/:uid', getByUid],
  ['post', '/product_type', mutate]
]