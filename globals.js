/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Global variables
 */

global.app_env = process.env.ENV || "local_dev"
global.rootDir = __dirname
global.config = require('./config')

global.log = (logLevel => {
  const
    dir_length = __dirname.length,
    stack_line = /at\s?.*\s\(?(\S+:\d+:\d+)\)?$/
  const colors = { blue: '\x1b[34m', yellow: '\x1b[33m', cyan: '\x1b[36m', dim: '\x1b[2m', reset: '\x1b[0m' }
  const get_call_line = ({ stack }) => {
    try {
      return '.' + stack.split("\n", 3).pop().match(stack_line).pop().slice(dir_length)
    } catch(err) {
      console.log(err.message)
      console.log("can not parse:", stack)
    }
    return "???"
  }
  const txt_color = (color, txt) => `${colors[color]}${txt}${colors.reset}`

  const gen_log_fn = (name, level) => (...args) => {
    if(logLevel < level) {
      const err = new Error()
      const call_line = get_call_line(err)
      console.log(txt_color('yellow', `(${name})`), ...args, txt_color('cyan', `- at ${call_line}`))
    }
  }
  return Object.fromEntries(["system", "debug", "info", "warn", "error"].map((level, i) => [level, gen_log_fn(level, i)]))
})(process.env.LOGLEVEL ?? 1)

// Patch require
const Mod = require('module')
Mod.prototype.req_mod = Mod.prototype.require
const req_main = require.main.require
Mod.prototype.require = function (path) {
  if(path.startsWith('#/')) {
    return req_main(path.replace('#', '.'))
  } else {
    return this.req_mod(path)
  }
}