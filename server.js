/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Server
 */

require('./globals')

const Fastify = require("fastify")
const fastifyCors = require("fastify-cors")
const fastifyErorrHandle = require('./modules/fastify/errorHandler')
const fastifyAuth = require("./modules/fastify/fastify-auth")
const fatifyMultipartUpload = require("./modules/fastify/fastify-multipart-upload")
const fastifyStatic = require('fastify-static')
const fastifyHttpProxy = require('fastify-http-proxy')
const nlpnRouter = require('./modules/nhalpn-router')
// const imageScale = require("./routes/image_scale")

function init_notify() {
  config.tracking && process.on('track', require('./services/tracking'))
  config.sync_partner && process.on('sync', require('./services/sync-partner'))
  config.notify_mailer && process.on('send_notify_mail', require('./services/notify_mailer'))
}

module.exports = function () {
  const {
    fastify_options,
    apiPrefix,
    tmp_dir: uploadDir
  } = config

  const fastify = Fastify(fastify_options)

  // Chỉ dùng cho bản chạy trên localhost
  if(app_env === 'local_dev') {
    fastify.register(fastifyCors, {
      credentials: true,
      origin: true,
      allowedHeaders: ['Content-Type', 'Authorization'],
      methods: ['GET', 'POST', 'PUT', 'DELETE, OPTIONS']
    })
  }

  fastify.setErrorHandler(fastifyErorrHandle)

  fastifyAuth(fastify)

  fatifyMultipartUpload(fastify, {
    hashAlgorithm: 'md5',
    multiples: true,
    uploadDir
  })

  for(const option of config.static_routes) {
    fastify.register(fastifyStatic, option)
  }

  if(app_env === 'local_dev') {
    // Chỉ local_dev mới xài package: fastify-http-proxy
    for(const option of config.proxy_routes) {
      fastify.register(fastifyHttpProxy, option)
    }
  }

  // fastify.route(imageScale)
  fastify.get('/health', () => "OK")

  fastify.register(nlpnRouter(rootDir + '/routes'), { prefix: apiPrefix })

  fastify.ready().then(init_notify)

  return fastify
}