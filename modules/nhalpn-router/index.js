/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Extend my own flexible router functions for omishop
 */

const fs = require('fs')

const type = o => o.constructor.name

async function getRoutes(dir) {
  const tmpRoutes = []
  const schemas = {}

  const buildRoutes = items => {
    const objRoutes = []
    const asyncRoutes = []

    // Function recursive để biến các router dạng function, async_function về thành array hoặc promise
    const preProcess = route => {
      switch (type(route)) {
        case "Array":
          objRoutes.push(route); break
        case "Promise":
          asyncRoutes.push(route); break
        case "Function":
        case "AsyncFunction":
          preProcess(route()); break
        default:
          throw new Error("unknow route type: " + type(route))
      }
    }

    // Biến các thể loại cấu trúc option tự chế về option chuẩn của fastify
    const parseRouters = routes => routes.map(route => {
      switch (type(route)) {
        case "Array": {
          route[0] = route[0].toUpperCase()
          const [method, url, handler, opts = {}] = route
          if(typeof handler === 'object') {
            return { method, url, ...handler }
          } else {
            return { method, url, handler, ...opts }
          }
        }
        case "Object":
          return route
      }
      throw new Error("unknow route type:" + type(route))
    })

    items.forEach(preProcess)

    // Chuyển thể tất cả promise về router cho fastify
    // và mở rộng thêm tính năng router của nhalpn
    return Promise.all(asyncRoutes)
      .then(result => result.concat(objRoutes).flat())
      .then(parseRouters)
  }

  // Tự động load tất cả route có trong thư mục routers
  const collectRouters = (dir, deep = 0) => {
    deep++
    fs.readdirSync(dir).forEach(function (fileName) {
      const fullPath = `${dir}/${fileName}`
      if(fs.lstatSync(fullPath).isDirectory()) {
        if(deep <= 1 || fileName.startsWith('@')) collectRouters(fullPath, deep)
      } else {
        // Tìm các thư mục có chứa file tên index.js hoặc tên có kú tự @ mở đầu
        if(fileName.startsWith('@') || fileName === "index.js") {
          // Lấy các route được export trong index.js đưa vào fastify
          tmpRoutes.push(require(fullPath))
        } else if(fileName === "schema.js") {
          Object.assign(schemas, require(fullPath))
        }
      }
    })
  }

  collectRouters(dir)

  return buildRoutes(tmpRoutes)
}

module.exports = dir => async fastify => getRoutes(dir).then(routes => routes.forEach(route => { fastify.route(route) }))