"use strict"

function buildRowData (fields, data, defaultValue) {
  const entries = fields.map(field => [field, data[field] ?? defaultValue])
  return Object.fromEntries(entries)
}

function prepareDataExportError (data) {
  const formatDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })

  const xlsFieldsLabel = [
    "Ngày export",
    "SKU ID",
    "Mã tham chiếu với GHN",
    "Tên sản phẩm",
    "Tên hiển thị",
    "Tên hiển thị chi tiết",
    "Barcode",
    "ID Nhà cung cấp (*)",
    "ID Nhà sản xuất",
    "Tên thương hiệu",
    "Ngành Hàng 2 (*)",
    "Xuất sứ",
    "Đơn vị tính",
    "Đơn vị con",
    "Số lượng đơn vị con",
    "Quy cách đóng gói",
    "Số lượng đơn vị con trong package",
    "Mô tả ngắn",
    "Keywords",
    "Đơn vị đo",
    "Khối lượng",
    "Dài(cm)",
    "Rộng(cm)",
    "Cao(cm)",
    "Giá mua từ nhà cung cấp (No VAT)",
    "Giá mua từ NCC (Có VAT)",
    "Giá niêm yết (NO VAT)",
    "Giá niêm yết (Có VAT)",
    "Giá bán (No VAT)",
    "Giá bán (Có VAT)",
    "Front Margin",
    "Điều Kiện Bảo Hành ",
    "Điều khoản thanh toán",
    "Chương trình khuyến mãi",
    "Chi tiết khuyến mãi",
    "Còn hàng",
    "Số lượng tồn kho",
    "COD",
    "Khu vực",
    "Dòng 1 mô tả",
    "Dòng 2 mô tả",
    "Dòng 3 mô tả",
    "Dòng 4 mô tả",
    "Mô tả html"
  ]
  const xlsFieldsName = [
    "dateCreated",
    "sku_id",
    "ghn_code",
    "product_name",
    "display_name",
    "display_name_detail",
    "barcode",
    "partner_uid",
    "partner_name",
    "manufacturer_uid",
    "manufacturer_name",
    "brand_uid",
    "brand_name",
    "collection_uid",
    "collection_name",
    "original",
    "unit",
    "sub_unit",
    "sub_unit_quantity",
    "packaging_unit",
    "packaging_unit_quantity",
    "short_desc",
    "keywords",
    "UOM_uid",
    "UOM_name",
    "uom_quantity",
    "long",
    "width",
    "height",
    "cost_price_without_vat",
    "cost_price_with_vat",
    "listed_price_without_vat",
    "listed_price_with_vat",
    "price_without_vat",
    "price_with_vat",
    "front_margin",
    "return_terms",
    "payment_terms",
    "promotion_desc",
    "promotion_detail",
    "instock",
    "stock",
    "is_disabled_cod",
    "region_uid",
    "name",
    "short_description_1",
    "display_name",
    "short_description_2",
    "display_name",
    "short_description_3",
    "display_name",
    "short_description_4",
    "display_name",
    "description_html"
  ]

  const rows = data.map(dataDetail => buildRowData(xlsFieldsName, dataDetail, ''))
  rows[0].dateCreated = formatDate

  return {
    xlsFieldsName,
    xlsFieldsLabel,
    rows
  }
}
function prepareDataExport (data) {
  const formatDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  const xlsFieldsLabel = [
    "Ngày export",
    "ID",
    "SKU ID",
    "Mã tham chiếu với GHN",
    "Tên sản phẩm",
    "Tên hiển thị",
    "Tên hiển thị chi tiết",
    "Tên hiển thị trên hoá đơn",
    "Barcode",
    "ID Nhà cung cấp (*)",
    "ID Nhà sản xuất",
    "Tên thương hiệu",
    "Ngành Hàng 2 (*)",
    "Xuất sứ",
    "Đơn vị tính",
    "Đơn vị con",
    "Số lượng đơn vị con",
    "Quy cách đóng gói",
    "Số lượng đơn vị con trong package",
    "Mô tả ngắn",
    "Keywords",
    "Đơn vị đo",
    "Khối lượng",
    "Dài(cm)",
    "Rộng(cm)",
    "Cao(cm)",
    "Giá mua từ nhà cung cấp (No VAT)",
    "Giá mua từ NCC (Có VAT)",
    "Giá niêm yết (NO VAT)",
    "Giá niêm yết (Có VAT)",
    "Giá bán (No VAT)",
    "Giá bán (Có VAT)",
    "Front Margin",
    "Điều Kiện Bảo Hành ",
    "Điều khoản thanh toán",
    "Chương trình khuyến mãi",
    "Chi tiết khuyến mãi",
    "Còn hàng",
    "Số lượng tồn kho",
    "COD",
    "Điều kiện bảo hành",
    "Thời gian bảo hành",
    "Khu vực",
    "Dòng 1 mô tả",
    "Dòng 2 mô tả",
    "Dòng 3 mô tả",
    "Dòng 4 mô tả",
    "Mô tả html",
    "ID nhóm phân loại 1",
    "Giá trị phân loại 1",
    "ID nhóm phân loại 2",
    "Giá trị phân loại 2",
    "Trạng thái"
  ]
  const xlsFieldsName = [
    "dateCreated",
    "uid",
    "sku_id",
    "ghn_code",
    "product_name",
    "display_name",
    "display_name_detail",
    "display_bill_name",
    "barcode",
    "partner_uid",
    "partner_name",
    "manufacturer_uid",
    "manufacturer_name",
    "brand_uid",
    "brand_name",
    "collection_uid",
    "collection_name",
    "original",
    "unit",
    "sub_unit",
    "sub_unit_quantity",
    "packaging_unit",
    "packaging_unit_quantity",
    "short_desc",
    "keywords",
    "UOM_uid",
    "UOM_name",
    "uom_quantity",
    "long",
    "width",
    "height",
    "cost_price_without_vat",
    "cost_price_with_vat",
    "listed_price_without_vat",
    "listed_price_with_vat",
    "price_without_vat",
    "price_with_vat",
    "front_margin",
    "return_terms",
    "payment_terms",
    "promotion_desc",
    "promotion_detail",
    "instock",
    "stock",
    "is_disabled_cod",
    "warranty_policy",
    "shelf_life",
    "region_uid",
    "name",
    "short_description_1",
    "display_name_1",
    "short_description_2",
    "display_name_2",
    "short_description_3",
    "display_name_3",
    "short_description_4",
    "display_name_4",
    "description_html",
    "product_tag_1",
    "product_tag_name_1",
    "product_tag_2",
    "product_tag_name_2",
    "product_tag_3",
    "product_tag_name_3",
    "product_tag_4",
    "product_tag_name_4",
    "specs_type_uid_1",
    "spec_type_value_1",
    "specs_type_uid_2",
    "spec_type_value_2",
    "display_status"
  ]

  const rows = data.map(dataImport => {

    // PREPARE DEFAULT DATA FOR ROW DATA
    // let areaList = ''
    // let collectionList = ''

    if (dataImport.areas) {
      // let areaListTemp = []
      // for (let area of dataImport.areas) {
      //   area && areaListTemp.push(area.uid)
      // }
      // dataImport.areas = areaListTemp.toString();

      /**try to fix this */
      const areaListTemp = dataImport.areas
      dataImport.name = areaListTemp?.map(a => a.name).join(",")
      dataImport.areas = areaListTemp?.map(a => a.uid).join(",")

    }
    // END PREPARE REGION DATA

    // PREPARE COOLECTION DATA
    if (dataImport['product.collection']) {
      // let collectionListTemp = []
      // for (let collection of dataImport['product.collection']) {
      //   collectionListTemp.push(collection.uid)
      // }
      // dataImport['product.collection'] = collectionListTemp.toString()

      /**try to fix this */
      const collectionListTemp = dataImport['product.collection']
      dataImport['collection_name'] = collectionListTemp?.map(a => a.collection_name).join(",")
      dataImport['product.collection'] = collectionListTemp?.map(a => a.uid).join(",")

    }
    // PARSE NODE EDGE FOR PRODUCT
    dataImport['manufacturer_name'] = dataImport['product.manufacturer']?.manufacturer_name
    dataImport['product.manufacturer'] = dataImport['product.manufacturer']?.uid

    dataImport['brand_name'] = dataImport['product.brand']?.brand_name
    dataImport['product.brand'] = dataImport['product.brand']?.uid

    dataImport['UOM_name'] = dataImport['product.uom']?.UOM_name
    dataImport['product.uom'] = dataImport['product.uom']?.uid

    dataImport['partner_name'] = dataImport['product.partner']?.partner_name
    dataImport['product.partner'] = dataImport['product.partner']?.uid


    // SET PRICING PRODUCT DATA
    dataImport.cost_price_without_vat = dataImport['product.pricing']?.cost_price_without_vat
    dataImport.cost_price_with_vat = dataImport['product.pricing']?.cost_price_with_vat
    dataImport.listed_price_without_vat = dataImport['product.pricing']?.listed_price_without_vat
    dataImport.listed_price_with_vat = dataImport['product.pricing']?.listed_price_with_vat
    dataImport.price_without_vat = dataImport['product.pricing']?.price_without_vat
    dataImport.price_with_vat = dataImport['product.pricing']?.price_with_vat
    dataImport.front_margin = dataImport['product.pricing']?.front_margin
    // END SET PRICING PRODUCT DATA

    // SET SHORT DESCRIPTION PRODUCT DATA
    dataImport.display_name_1 = dataImport?.short_description_1?.display_name
    dataImport.short_description_1 = dataImport?.short_description_1?.uid

    dataImport.display_name_2 = dataImport?.short_description_2?.display_name
    dataImport.short_description_2 = dataImport?.short_description_2?.uid

    dataImport.display_name_3 = dataImport?.short_description_3?.display_name
    dataImport.short_description_3 = dataImport?.short_description_3?.uid

    dataImport.display_name_4 = dataImport?.short_description_4?.display_name
    dataImport.short_description_4 = dataImport?.short_description_4?.uid

    // END SET SHORT DESCRIPTION PRODUCT DATA

    // SET PRODUCT TAG
    if (dataImport['product.tag']) {
      for (const objTag of dataImport['product.tag']) {
        if (objTag.tag_category == "assess") {
          dataImport.product_tag_1 = objTag.uid
          dataImport.product_tag_name_1 = objTag.tag_title
        } else if (objTag.tag_category == "promotion") {
          dataImport.product_tag_2 = objTag.uid
          dataImport.product_tag_name_2 = objTag.tag_title
        } else if (objTag.tag_category == "shipping") {
          dataImport.product_tag_3 = objTag.uid
          dataImport.product_tag_name_3 = objTag.tag_title
        } else if (objTag.tag_category == "name_tag") {
          dataImport.product_tag_4 = objTag.uid
          dataImport.product_tag_name_4 = objTag.tag_title
        }
      }
    }

    // END SET PRODUCT TAG

    // PARSE DISPLAY STATUS PRODUCT
    if (dataImport.display_status == 0) {
      dataImport.display_status = "PENDING"
    } else if (dataImport.display_status == 1) {
      dataImport.display_status = "REJECTED"
    } else if (dataImport.display_status == 2) {
      dataImport.display_status = "APPROVED"
    } else if (dataImport.display_status == 3) {
      dataImport.display_status = "IMPORT"
    }
    // END PARSE DISPLAY STATUS PRODUCT

    let str = JSON.stringify(dataImport);
    str = str.replace(/"areas":/g, "\"region_uid\":");
    str = str.replace(/"product.manufacturer":/g, "\"manufacturer_uid\":");
    str = str.replace(/"product.brand":/g, "\"brand_uid\":");
    str = str.replace(/"product.partner":/g, "\"partner_uid\":");
    str = str.replace(/"product.collection":/g, "\"collection_uid\":");
    str = str.replace(/"product.uom":/g, "\"UOM_uid\":");

    dataImport = JSON.parse(str);

    // PREPARE ROW DATA TO EXPORT
    return buildRowData(xlsFieldsName, dataImport, '')
  })
  if (rows.length > 0) {
    rows[0].dateCreated = formatDate
  }


  return {
    xlsFieldsName,
    xlsFieldsLabel,
    rows
  }
}
function prepareErrorRow (data) {
  const formatDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
  const xlsFieldsLabel = [
    "Ngày export",
    "Dòng",
    "Thông tin",
    "Lí do"
  ]
  const xlsFieldsName = [
    "dateCreated",
    "stt",
    "field_name",
    "error_value"
  ]

  // PREPARE DEFAULT DATA FOR ROW DATA
  // PREPARE ROW DATA TO EXPORT
  const rows = data.map(dataDetail => buildRowData(xlsFieldsName, dataDetail, ''))
  rows[0].dateCreated = formatDate

  return {
    xlsFieldsName,
    xlsFieldsLabel,
    rows
  }
}
function prepareErrorImport (data) {
  const formatDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })

  const xlsFieldsName = [
    "dateCreated",
    "manufacturer_uid",
    "region_uid",
    "brand_uid",
    "partner_uid",
    "collection_uid",
    "UOM_uid",
    "short_description_1",
    "short_description_2",
    "short_description_3",
    "short_description_4",
    "product_tag_1",
    "product_tag_2",
    "product_tag_3",
    "product_tag_4"
  ]

  const rows = data.map(dataDetail => buildRowData(xlsFieldsName, dataDetail, '-'))
  rows[0].dateCreated = formatDate

  return {
    xlsFieldsName,
    rows
  }
}

module.exports = { prepareDataExportError, prepareDataExport, prepareErrorRow, prepareErrorImport }