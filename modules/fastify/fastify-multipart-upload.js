/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Handle upload file function for whole project
 */

const formidable = require("formidable")

module.exports = function(fastify, opts = {}) {
  fastify.decorateRequest('isMultipart', function() {
    return this.headers['content-type'].startsWith('multipart/form-data')
  })
  fastify.addContentTypeParser('multipart/form-data', function (req, payload, done) {
    formidable(opts).parse(payload, function(err, fields, files) {
      done(err, { fields, files })
    })
  })
}