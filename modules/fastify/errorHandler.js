/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Log error
 */

function parseBody (body, length) {
  if(length > 1024) {
    if(typeof body === 'object') {
      return JSON.stringify(body).slice(0, 1024) + '...'
    } else if(typeof body === 'string') {
      return body.slice(0, 1024)
    }
  }
  return body
}

module.exports = function (error, { method, url, params, query, body, headers }, reply) {
  console.log('>>> Request:', method, url)

  const level = reply.statusCode < 500 ? 'Warn' : 'Error'

  if(error instanceof Error) {
    console.log(`-   ${level}|`, error.stack)
  } else {
    console.log(`-   ${level}|`, error)
  }
  if(params && Object.keys(params).length) {
    console.log('-   Params:', params)
  }
  if(query && Object.keys(query).length) {
    console.log('-   Query:', query)
  }
  if(body) {
    const length = +headers['content-length']
    console.log(`-   Body (${length} bytes):`, parseBody(body, length))
  }

  // Không in thông tin lỗi validation ra
  // khỏi lộ format data, dễ bị exploxit
  error.validation ? reply.status(400).send(new Error('validation failed!')) : reply.send(error)
}