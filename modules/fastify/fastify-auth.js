/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Check cookies and tokens
 */

const fs = require('fs')
const path = require('path')
const tokens = require(`#/config/${app_env}/tokens`)

module.exports = function (fastify) {
  fastify.register(require('fastify-secure-session'), {
    key: fs.readFileSync(path.join(rootDir, 'config', app_env, 'secret-key')),
    cookie: { path: '/' }
  })

  const auth = req => req.session.get('login') || req.headers['x-api-key'] in tokens

  fastify.addHook('preHandler', (request, reply, done) => {
    if(auth(request)) {
      done()
    } else {
      const url = request.raw.url
      if(url.startsWith('/health') || url.startsWith('/api/user/login') || url.startsWith('/login') || url === '/favicon.webp' || url === '/manifest.json') {
        done()
      } else {
        reply.redirect('/login/index.html')
      }
    }
  })
}