"use strict"

const db = require("#/services/tvcdb")

// FUNCTION USE TO CHANGE DATA EDGES
function createEdgesData (productObj) {
  if (Object.keys(productObj).length != 0) {
    let str = JSON.stringify(productObj);
    str = str.replace(/"region_uid":/g, "\"product.areas\":");
    str = str.replace(/"ott_uid":/g, "\"product.ott\":");
    str = str.replace(/"manufacturer_uid":/g, "\"product.manufacturer\":");
    str = str.replace(/"brand_uid":/g, "\"product.brand\":");
    str = str.replace(/"partner_uid":/g, "\"product.partner\":");
    str = str.replace(/"collection_uid":/g, "\"product.collection\":");
    str = str.replace(/"UOM_uid":/g, "\"product.uom\":");
    str = str.replace(/"group_payment":/g, "\"product.group_payment\":");
    productObj = JSON.parse(str);
    const regionArr = productObj['product.areas'].split(',')
    if (regionArr.length > 0) {
      const tmp = []
      for (let i = 0; i < regionArr.length; i++) {
        tmp.push({ uid: regionArr[i] })
      }
      productObj['product.areas'] = tmp;
    }
    const ottArr = productObj['product.ott'].split(',')
    if (ottArr.length > 0) {
      const tmpOtt = []
      for (let i = 0; i < ottArr.length; i++) {
        tmpOtt.push({ uid: ottArr[i] })
      }
      productObj['product.ott'] = tmpOtt;
    }
    productObj['product.manufacturer'] = { uid: productObj['product.manufacturer'] }
    productObj['product.brand'] = { uid: productObj['product.brand'] }
    productObj['product.partner'] = { uid: productObj['product.partner'] }
    const collectionArr = productObj['product.collection'].split(',')
    if (collectionArr.length > 0) {
      const tmp = []
      for (let i = 0; i < collectionArr.length; i++) {
        tmp.push({ uid: collectionArr[i] })
      }
      productObj['product.collection'] = tmp
    }
    productObj['product.uom'] = { uid: productObj['product.uom'] }
    productObj['product.group_payment'] = { uid: productObj['product.group_payment'] }
    return productObj;
  }
}
// END FUNCTION USE TO CHANGE DATA EDGES

// PREPARE ALIAS FOR PRODUCT
function change_alias (str) {
  return !str ? '' : str.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/đ/g, 'd').replace(/Đ/g, 'D');
}
function executeProductTag (tag) {
  const arrProductTag = []
  if (tag.productTag1) {
    arrProductTag.push({ uid: tag.productTag1 })
  }
  if (tag.productTag2) {
    arrProductTag.push({ uid: tag.productTag2 })
  }
  if (tag.productTag3) {
    arrProductTag.push({ uid: tag.productTag3 })
  }
  if (tag.productTag4) {
    arrProductTag.push({ uid: tag.productTag4 })
  }
  return arrProductTag
}
async function executeUpdateProductSpecsValue (uidSpec, specValue, arrSpecs) {
  const { find_exist } = await db.query(`{
    find_exist(func: type(SpecsValue)) @filter(eq(specs_value_name, "${String(specValue)}")) {
      uid
      specs_value_name
      specs_type: ~specs_type.values {
        uid
        specs_type_name
      }
    }
  }`)
  const checkSpecValue = arrSpecs?.filter(spec => spec?.['specs_type.values']?.specs_value_name === specValue) ?? [];
  let specsTypeData = {}
  if (find_exist?.length === 0 && checkSpecValue.length === 0) {
    //tạo mới lưu vào
    specsTypeData = {
      uid: uidSpec,
      "specs_type.values": {
        uid: genUid('new_specs_value'),
        "dgraph.type": "SpecsValue",
        specs_value_name: specValue
      }
    }
  } else {
    if (find_exist[0] && find_exist[0].uid) {
      specsTypeData = {
        uid: uidSpec,
        "specs_type.values": {
          uid: find_exist[0].uid,
          "dgraph.type": "SpecsValue",
          specs_type_name: specValue
        }
      }
    }
  }
  return Object.keys(specsTypeData).length ? specsTypeData : null
}
async function executeProductSpecs (specs, arrSpecs) {
  const arrProductSpecs = []
  const specsTypeData = []
  if (specs.specs_type_uid_1) {
    const { specType } = await db.query(`{
      specType(func: uid(${specs.specs_type_uid_1})) @filter(type(SpecsType) AND eq(display_status,2)) {
        uid
        specs_type_name
      }
    }`)
    if(specType.length) {
      arrProductSpecs.push({
        uid: genUid('new_specs_section'),
        'dgraph.type': "SpecsSection",
        section_name: specType[0].specs_type_name,
        section_ref: specType[0].uid,
        section_ref_value: specs.spec_type_value_1,
        section_value: specType[0].uid,
        display_order: 0
      })
    }
    const spec = await executeUpdateProductSpecsValue(specs.specs_type_uid_1, specs.spec_type_value_1, arrSpecs)
    if(spec != null) {
      specsTypeData.push(spec)
    }
  }
  if (specs.specs_type_uid_2) {
    const { specType } = await db.query(`{
      specType(func: uid(${specs.specs_type_uid_2})) @filter(type(SpecsType) AND eq(display_status,2)) {
        uid
        specs_type_name
      }
    }`)
    if(specType.length) {
      arrProductSpecs.push({
        uid: genUid('new_specs_section'),
        'dgraph.type': "SpecsSection",
        section_name: specType[0].specs_type_name,
        section_ref: specType[0].uid,
        section_ref_value: specs.spec_type_value_2,
        section_value: specType[0].uid,
        display_order: 1
      })
    }
    const spec = await executeUpdateProductSpecsValue(specs.specs_type_uid_2, specs.spec_type_value_2, arrSpecs)
    if(spec != null) {
      specsTypeData.push(spec)
    }
  }
  return { arrProductSpecs, specsTypeData }
}
let count = 0
function genUid (prefix) {
  return `_:${prefix}_${(new Date()).getTime() + count++}`
}

// END PREPARE ALIAS FOR PRODUCT
/**
 *
 * @param {*} products
 * @param {*} logUID
 * @returns {Promise}
 */
async function mutate (products, logUID) {
  console.log('Start import data into system.')
  /* const productParser = (prod) => {
    let arrProductTag = executeProductTag(prod)
    prod['product.tag'] = arrProductTag ?? []
    let dataChange = createEdgesData(prod)
    dataChange.uid = genUid('new_product')
    dataChange['dgraph.type'] = "Product"
    dataChange.display_status = 3
    // CREATE FULLTEXT SEARCH FOR PRODUCT IMPORT.
    dataChange.fulltext_search = [
      dataChange.product_name, change_alias(dataChange.product_name),
      dataChange.display_name, change_alias(dataChange.display_name),
      dataChange.display_name_detail, change_alias(dataChange.display_name_detail),
      dataChange.keywords, change_alias(dataChange.keywords),
      dataChange.sku_id
    ].join(' ')
    // END CREATE FULLTEXT SEARCH FOR PRODUCT IMPORT.

    if ("brand_shop.product" in prod) { // Đè luôn vào dataChange để mutate, không cần check 2 lần, không cần để riêng ra
      dataChange['brand_shop.product'] = {
        uid: prod['brand_shop.product'],
        'brand_shop.product': { uid: dataChange.uid },
        'dgraph.type': "BrandShop"
      }
    }
    return dataChange
  } */

  const txn = db.txn()
  let chunk
  try {
    while(chunk = products.splice(0, 200), chunk.length > 0) {
      const dataImport = []
      const brandShops = []
      let arrSpecs = []
      for(const prod of chunk) {
        /* PREPARE PRODUCT SPECS DATA */
        const arrProductTag = executeProductTag(prod)
        if(prod.productTag1) {
          delete prod.productTag1
        }
        if(prod.productTag2) {
          delete prod.productTag2
        }
        if(prod.productTag3) {
          delete prod.productTag3
        }
        if(prod.productTag4) {
          delete prod.productTag4
        }
        prod['product.tag'] = arrProductTag ?? []
        /* END PREPARE PRODUCT SPECS DATA */

        /* PREPARE PRODUCT SPECS DATA */
        const { arrProductSpecs, specsTypeData } = await executeProductSpecs(prod, arrSpecs)
        if(specsTypeData.length) {
          arrSpecs = arrSpecs.concat(specsTypeData)
        }
        if(prod.specs_type_uid_1) {
          delete prod.specs_type_uid_1
          delete prod.spec_type_value_1
        }
        if(prod.specs_type_uid_2) {
          delete prod.specs_type_uid_2
          delete prod.spec_type_value_2
        }
        prod['product.specs_sections'] = arrProductSpecs ?? []

        /* END PREPARE PRODUCT SPECS DATA */
        /* UPDATE PRODUCT SPECS VALUE */
        // let specsTypeData = await executeUpdateProductSpecsValue(prod)
        /* END UPDATE PRODUCT SPECS VALUE */
        const dataChange = createEdgesData(prod)
        dataChange.uid = genUid('new_product')


        if ("brand_shop.product" in prod) { // Đè luôn vào dataChange để mutate, không cần check 2 lần, không cần để riêng ra
          brandShops.push({
            uid: prod['brand_shop.product'],
            'brand_shop.product': { uid: dataChange.uid },
            'dgraph.type': "BrandShop"
          })
          delete dataChange['brand_shop.product']
        }
        dataChange['dgraph.type'] = "Product"
        dataChange.display_status = 3
        dataChange.short_description_1 = prod.short_description_1 ? { uid: prod.short_description_1 } : null
        dataChange.short_description_2 = prod.short_description_2 ? { uid: prod.short_description_2 } : null
        dataChange.short_description_3 = prod.short_description_3 ? { uid: prod.short_description_3 } : null
        dataChange.short_description_4 = prod.short_description_4 ? { uid: prod.short_description_4 } : null
        // CREATE FULLTEXT SEARCH FOR PRODUCT IMPORT.
        dataChange.fulltext_search = [
          dataChange.product_name, change_alias(dataChange.product_name),
          dataChange.display_name, change_alias(dataChange.display_name),
          dataChange.display_name_detail, change_alias(dataChange.display_name_detail),
          dataChange.keywords, change_alias(dataChange.keywords),
          dataChange.sku_id
        ].join(' ')
        // END CREATE FULLTEXT SEARCH FOR PRODUCT IMPORT.

        dataImport.push(dataChange)
      }
      await db.mutate({ set: [...dataImport, ...brandShops, ...arrSpecs] }, txn)
    }
    console.log('Finalize import !!!!!', products.length)
    await txn.commit()
    // UPDATE STATUS OF LOG IMPORT IF FILE IMPORT DONE....
    await db.mutate({
      set: {
        uid: logUID,
        import_status: 'Done',
        "dgraph.type": "LogImport"
      }
    })
  } catch (error) {
    log.error("Import product", error)
    await db.mutate({
      set: {
        uid: logUID,
        import_status: 'Error',
        "dgraph.type": "LogImport"
      }
    })
  }
}

async function mutateUpdate (products, logUID) {
  console.log('Start import update data into system.')
  const txn = db.txn()
  let chunk
  try {
    while(chunk = products.splice(0, 200), chunk.length > 0) {
      const dataImport = []
      let arrSpecs = []
      for(const prod of chunk) {
        // DELETE PREDICATE EMPTY BEFORE UPDATE.
        // await db.upsert({
        //   query: `{
        //         q(func: uid(${prod.uid})) @filter(type(Product)){
        //             p as uid
        //           }
        //         }`,
        //   del: {
        //     uid: "uid(p)",

        //   }
        // }, txn)
        // END DELETE PREDICATE EMPTY BEFORE UPDATE.
        // DELETE NODE EDGE BEFORE UPDATE.
        await db.upsert({
          query: `{
              q(func: uid(${prod.uid})) @filter(type(Product)){
                  p as uid
                }
              }`,
          del: {
            uid: "uid(p)",
            'ghn_code': null,
            'barcode': null,
            'sub_unit': null,
            'sub_unit_quantity': null,
            'packaging_unit': null,
            'packaging_unit_quantity': null,
            'short_desc': null,
            'return_terms': null,
            'payment_terms': null,
            'promotion_desc': null,
            'promotion_detail': null,
            'instock': null,
            'stock': null,
            'is_disabled_cod': null,
            'description_html': null,
            'product.manufacturer': null,
            'product.brand': null,
            'product.partner': null,
            'product.uom': null,
            'product.collection': null,
            'product.areas': null,
            'product.ott': null,
            'product.group_payment': null,
            'short_description_1': null,
            'short_description_2': null,
            'short_description_3': null,
            'short_description_4': null,
            'product.tag': null,
            'product.specs_sections': null
          }
        }, txn)

        // UPDATE FULL TEXT SEARCH
        const arrProductTag = executeProductTag(prod)
        if(prod.productTag1) {
          delete prod.productTag1
        }
        if(prod.productTag2) {
          delete prod.productTag2
        }
        if(prod.productTag3) {
          delete prod.productTag3
        }
        if(prod.productTag4) {
          delete prod.productTag4
        }
        prod['product.tag'] = arrProductTag ?? []

        /* PREPARE PRODUCT SPECS DATA */
        const { arrProductSpecs, specsTypeData } = await executeProductSpecs(prod, arrSpecs)
        if(specsTypeData.length) {
          arrSpecs = arrSpecs.concat(specsTypeData)
        }
        if(prod.specs_type_uid_1) {
          delete prod.specs_type_uid_1
          delete prod.spec_type_value_1
        }
        if(prod.specs_type_uid_2) {
          delete prod.specs_type_uid_2
          delete prod.spec_type_value_2
        }
        prod['product.specs_sections'] = arrProductSpecs ?? []
        const dataChange = createEdgesData(prod)

        dataChange.short_description_1 = prod.short_description_1 ? { uid: prod.short_description_1 } : null
        dataChange.short_description_2 = prod.short_description_2 ? { uid: prod.short_description_2 } : null
        dataChange.short_description_3 = prod.short_description_3 ? { uid: prod.short_description_3 } : null
        dataChange.short_description_4 = prod.short_description_4 ? { uid: prod.short_description_4 } : null
        dataChange.fulltext_search = [
          dataChange.product_name, change_alias(dataChange.product_name),
          dataChange.display_name, change_alias(dataChange.display_name),
          dataChange.display_name_detail, change_alias(dataChange.display_name_detail),
          dataChange.keywords, change_alias(dataChange?.keywords?.toString())
        ].join(' ')
        // END UPDATE FULL TEXT SEARCH
        dataImport.push(dataChange)
      }
      await db.mutate({ set: [...dataImport, ...arrSpecs] }, txn)
    }
    console.log('Finalize import !!!!!', products.length)
    // FINALIZE COMMIT THE TRANSACTION
    await txn.commit()
    // Import xong thì mới ghi nhận là xong chứ
    await db.mutate({
      set: {
        uid: logUID,
        import_status: 'Done',
        "dgraph.type": "LogImport"
      }
    })
  } catch (error) {
    log.error("Import product", error)
    await db.mutate({
      set: {
        uid: logUID,
        import_status: 'Error',
        "dgraph.type": "LogImport"
      }
    })
  }
}
module.exports = {
  mutate,
  mutateUpdate
}