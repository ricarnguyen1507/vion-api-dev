/**
 * @author LPN Nhã <nhalpn@fpt.com.vn>
 * Handle upload file for whole project
 */

const fs = require('fs')
const path = require('path')
const mime = require("mime-types")

function saveFile (file, fileDir, fileName) {
  if(!fs.existsSync(fileDir)) {
    fs.mkdirSync(fileDir, { recursive: true })
  }
  const filePath = path.join(fileDir, fileName)
  fs.writeFileSync(filePath, fs.readFileSync(file.filepath));
  fs.unlinkSync(file.filepath)
  return filePath
}

function saveMediaMD5 ([md5, file], folder) {
  if(md5 === file.hash) {
    const [media_type] = file.mimetype.split('/')
    const fileDir = path.join(config.image_dir, `${media_type}s`, folder)
    const fileName = `${file.hash}.${mime.extension(file.mimetype)}`
    saveFile(file, fileDir, fileName)
  } else {
    throw new Error(`MD5 not match ${md5} ${file.hash}`)
  }
}

function saveMediaFile (file, prefix, folder) {
  const ext = mime.extension(file.mimetype)

  const [media_type] = file.mimetype.split('/')

  let savePath = ''

  if(media_type === 'video') {
    savePath = path.join(config.video_dir, `${media_type}s`, folder)
  } else if(media_type === 'image') {
    savePath = path.join(config.image_dir, `${media_type}s`, folder)
  }
  if(savePath === '') {
    throw new Error('unknow media type')
  }

  const fileName = `${prefix}_${(new Date()).getTime()}.${ext}`

  saveFile(file, savePath, fileName)

  return [`${media_type}s/${folder}/${fileName}`, media_type]
}

function saveMediaFiles (files, folder) {
  const result = {}
  for(const field in files) {
    const [path] = saveMediaFile(files[field], field, folder)
    result[field] = path
  }
  return result
}

function saveMediaImage ([file_name, file], folder, repFileName) {
  if(file_name) {
    const [media_type] = file.mimetype.split('/')
    const fileDir = path.join(config.image_dir, `${media_type}s`, folder)
    const fileName = `${file_name ?? repFileName ?? file.hash}.${mime.extension(file.mimetype)}`
    saveFile(file, fileDir, fileName)
  }
  else {
    throw new Error(`File name does not exist !`)
  }
}

module.exports = { saveFile, saveMediaMD5, saveMediaFile, saveMediaFiles, saveMediaImage }