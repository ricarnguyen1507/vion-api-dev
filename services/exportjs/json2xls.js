const Excel = require("exceljs")

function exportProduct ({ fields = [], rows = [] }) {
  log.info(`Start export products`)
  if(fields.length === 0 || rows.length === 0) {
    throw new Error("No data to export")
  }
  var workbook = new Excel.Workbook();
  var worksheet = workbook.addWorksheet();
  // PREPARE DATA FOR COLUMNS
  worksheet.columns = fields.map(dataField => ({ header: dataField, key: dataField, width: 10 }))
  // END PREPARE DATA FOR COLUMNS
  // ADD DATA FOR ROW
  worksheet.addRows(rows)
  // END ADD ROW DATA
  log.info(`End export products`)
  return workbook.xlsx.writeBuffer()
}
module.exports = {
  exportProduct
}