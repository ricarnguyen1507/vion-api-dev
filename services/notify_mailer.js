const axios = require("axios").create(config.notify_mailer)

module.exports = function (mailPayload) {
  axios.post("/", mailPayload).then(res => {
    log.info("Send mail exported product", res.data)
  }).catch(err => {
    log.error("Send mail exported product", err.response?.data || err.message)
  })
}