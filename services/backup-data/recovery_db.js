"use strict"

const fs = require('fs')
const path = require('path')
const Types = require('./types')

async function RecoveryDB(...types) {
  const data = {}
  types = types.length > 0 ? types : Types
  for(const type of types) {
    data[type] = await readDataBackup(type)
  }
  return data
}

function readDataBackup(type) {
  return new Promise(function(resolve, reject) {
    try {
      const urlF = path.join(__dirname, 'data', `${type}.json`)
      if (fs.existsSync(urlF)) {
        const r = fs.createReadStream(urlF, { falgs: "?", autoClose: false, encoding: 'utf8' })
        r.on('data', (chunk) => {
          if (chunk.toString() !== "") {
            log.info(`Reading type: ${type}`, JSON.parse(chunk.toString()))
          }
        })
        r.on('close', resolve)
      }
    } catch(err) {
      reject(err)
    }

  })
}

module.exports = RecoveryDB