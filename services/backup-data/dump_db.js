"use strict"
const db = require("#/services/tvcdb")
const fs = require('fs')
const path = require('path')
const Types = require('./types')

// Ví dụ: DumpDB('Address', 'AddressType', 'Brand')
async function DumpDB(...types) {
  if(types.length === 0) {
    types = Types
  }
  const data = {}
  for(const type of types) {
    data[type] = await writeDataBackup(type)
  }
  return data
}

async function writeDataBackup(type) {
  let { result: [{ count }] } = await db.query(`{
        result(func: type(${type})) {
            count(uid)
        }
    }`)

  const limit = 200
  let offset = 0

  let typeTable = 0
  const urlF = path.join(__dirname, 'data', `${type}.json`)
  const f = fs.createWriteStream(urlF, { autoClose: false, encoding: 'utf8' })
  if (count === 0) {
    f.end()
  } else {
    while(count > 0) {
      const { result } = await db.query(`{
                result(func: type(${type}), first: ${limit}, offset: ${offset}) {
                    uid
                    dgraph_type: dgraph.type
                    expand(_all_) {
                        uid
                    }
                }
            }`)

      const strWrite = []
      result.forEach(r => {

        r['drgaph.type'] = r.dgraph_type[0]
        delete r.dgraph_type
        ++typeTable

        const json_str = JSON.stringify({ ...r })
        strWrite.push(json_str)
      })

      f.write(`${offset === 0 ? '[' : ','}${strWrite.join(',')}`, (err) => {
        if (err) throw err
      })

      if (count > limit) {
        count -= limit
        offset += limit
      } else {
        count -= count
      }
    }
    f.write(']', (err) => {
      if (err) throw err
    })
    f.end()
    log.info(`Dumped table - ${type}: ${typeTable} rows`)
  }

  return typeTable
}

module.exports = DumpDB