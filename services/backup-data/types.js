'use strict'

module.exports = ['Address', 'AddressType', 'Brand', 'CartItem', 'Collection', 'ContactInfo', 'Customer', 'Detail', 'Device', 'Distributor',
  'Icon', 'LayoutType', 'Manufacturer', 'Media', 'Notify', 'NotifyPlatform', 'NotifyUser', 'Order', 'OrderItem', 'Partner',
  'Pricing', 'PrimaryOrder', 'Product', 'ProductType', 'Region', 'Role', 'Status', 'UOM', 'CmsUser', 'VoucherUsage'
]