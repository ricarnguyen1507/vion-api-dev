module.exports = {
  parseProduct(product) {
    const { id: uid, title: product_name, images, variants } = product
    const [{ src: image_cover }, { src: image_banner }, { src: image_promotion }, { src: image_highlight }] = images
    const [{ price, sku }] = variants
    return {
      uid,
      sku,
      price,
      product_name,
      image_cover,
      image_banner,
      image_promotion,
      image_highlight
    }
  }
}