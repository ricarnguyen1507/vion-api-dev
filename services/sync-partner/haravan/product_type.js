const db = require("#/services/tvcdb")
const Api = require('./api')
const { calcPay, parseUrlImg } = require('./general')

/**
 * Create, Update a product, in haravan
 * Just One on time
 * This function get product by productUid, then if:
 * haravan_id is null it will call commit to create new record in haravan service and save haravanId into local database
 * haravan_id is {int} it will call commit to update this product in haravan service
 * @param productUid
 * @Crime => CA
 */
async function startSync(uid) {
  const { result: [product] } = await getOne(uid)

  if(!product) {
    throw { statusCode: 404, message: `Product ${uid} not exist` }
  }

  const type = product.haravan_id ? "update" : "create"
  const hrvProduct = await commit(product, type)
  return rewriteData(product, hrvProduct.id)
}

/**
  *
  * @param {*} uid
  * @param {*} callback
  * @Crime => CA
  */
function getOne(uid) {
  return db.query(`{
    result(func: uid(${uid})) @filter(type(Product)) {
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_)
        }
      }
    }
  }`)
}

/**
  * This function used to update haravanId into the product pass as first parameter
  * @param {*} product
  * @Crime => CA
  */
function rewriteData(product, haravan_id) {
  const mutate_time = (new Date()).getTime()
  const mutateData = {
    "dgraph.type": "Product",
    "updated_at": mutate_time,
    "uid": product.uid,
    "haravan_id": haravan_id
  }
  return db.mutate({ set: mutateData })
}

/**
  * This function include post and put data in haravan. If type = create and type = update
  * @param {*} product
  * @param {*} type
  * @param {*} callback
  */
async function commit(product, type) {
  let listDetail = ""
  if(product.details && product.details.length) {
    listDetail += product.details.map((detail) => `<li><p>${detail.title}: ${detail.content}</p></li>`)
  }

  let product_type = ""
  if (product['product.category'] && product['product.category'].length > 0) {
    product_type = product['product.category'][0].category_name
  }

  let vendor = ""
  if (product['product.brand_name']) {
    vendor = product['product.brand'].brand_name
  }

  const postProd = {
    ...(product.haravan_id && { id: product.haravan_id }),
    "product_type": product_type,
    "title": product.product_name,
    "body_html": listDetail,
    "images": [
      { "src": parseUrlImg(product.image_cover) }
    ],
    "vendor": vendor,
    "variants": [
      {
        "title": product.product_name,
        "price": calcPay(product.sell_price, product.discount),
        "option1": product.product_name,
        "sku": ""
      }
    ]
  }

  if (type === "create") {
    const { data } = await Api.post("/products.json", {
      "product": postProd
    })
    return data.product
  } else if (type === "update") {
    const { data } = Api.put(`/products/${product.haravan_id}.json`, {
      "product": postProd
    })
    return data.product
  }
}

module.exports = startSync