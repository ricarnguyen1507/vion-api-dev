const db = require("#/services/tvcdb")
const Api = require('./api')
const { calcPay, parseUrlImg } = require('./general')

/**
 * Create, Update a product, in haravan
 * Just One on time
 * This function get product by productUid, then if:
 * haravan_id is null it will call post to create new record in haravan service and save haravanId into local database
 * haravan_id is {int} it will call put to update this product in haravan service
 * and if product.is_deleted === true call del to delete this product in haravan service
 * @param productUid
 * @Crime => CA
 */
async function startSync(uid) {
  const { result: [data] } = await db.query(`{
    result(func: uid(${uid})) @filter(type(Product)) {
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_)
        }
      }
    }
  }`)

  if(!data) {
    throw { statusCode: 404, message: `Customer ${uid} not exist` }
  }
  if(data.is_deleted !== true) {
    // update : add
    const hrvData = parseData(data)
    const hrvRes = await (data.haravan_id ? put : post)(hrvData)
    await rewriteLocalData(data, hrvRes)
  } else if(data.haravan_id) {
    // Delete
    await del(data.haravan_id)
  }
  return uid
}

function parseData(product) {
  let listDetail = ""
  if(product.details && product.details.length) {
    listDetail += product.details.map((detail) => `<li><p>${detail.title}: ${detail.content}</p></li>`)
  }

  let product_type = "New"
  if (product['product.type']) {
    product_type = product['product.type'].type_name
  }

  let distributor = ""
  if (product['product.distributor']) {
    distributor = product['product.distributor'].distributor_name
  }
  return {
    "product": {
      ...(product.haravan_id && { id: product.haravan_id }),
      "title": product.product_name,
      "body_html": listDetail,
      "vendor": distributor,
      "product_type": product_type,
      "images": [
        { "src": parseUrlImg(product.image_cover) }
      ],
      "variants": [
        {
          "option1": product.product_name,
          "price": calcPay(product),
          "sku": product.uid,
          "requires_shipping": true
        }
      ]
    }
  }
}

/**
 * This function used to update haravanId into the product pass as first parameter
 * @param {*} product
 * @param {*} hrvRes
 * @Crime => CA
 */
function rewriteLocalData(product, hrvRes) {
  const mutateData = {
    "dgraph.type": "Product",
    "uid": product.uid,
    "haravan_id": hrvRes.data.product.id,
    "haravan_variant_id": hrvRes.data.product.variants[0].id
  }
  return db.mutate({ set: mutateData })
}

function post(hrvData) {
  return Api.post('/products.json', hrvData)
}

function put(hrvData) {
  return Api.put(`/products/${hrvData.product.id}.json`, hrvData)
}

function del(id) {
  return Api.delete(`/products/${id}.json`)
}

module.exports = startSync