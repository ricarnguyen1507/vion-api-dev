const db = require("#/services/tvcdb")
const Api = require('./api')

/**
 * Create, Update a customer, in haravan
 * Just One on time
 * This function get customer by customerUid, then if:
 * haravan_id is null it will call post to create new record in haravan service and save haravanId into local database
 * haravan_id is {int} it will call put to update this customer in haravan service
 * and if customer.is_deleted === true call del to delete this customer in haravan service
 * @param customerUid
 * @Crime => CA
 */

async function startSync(uid) {
  const { result: [data] } = await db.query(`{
    result(func: uid(${uid})) @filter(type(Customer)) {
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_)
        }
      }
    }
  }`)

  if(!data) {
    throw { statusCode: 404, message: `Customer ${uid} not exist` }
  }

  if(data.is_deleted !== true) {
    // update : add
    const hrvData = parseData(data)
    await (data.haravan_id ? put : post)(hrvData)
      .then(hrvRes => rewriteLocalData(data, hrvRes))
  } else if(data.haravan_id) {
    // Delete
    await del(data)
  }
  return uid
}

function parseData(customer) {
  return {
    "customer": {
      ...(customer.haravan_id && { id: customer.haravan_id }),
      "first_name": customer.customer_name,
      "email": customer.email,
      "phone": customer.phone_number,
      "verified_email": true,
      "addresses": [],
      "note": customer.address.address_des || null
    }
  }
}

/**
 * This function used to update haravanId into the customer pass as first parameter
 * @param {*} customer
 * @param {*} hrvRes
 * @Crime => CA
 */
function rewriteLocalData(customer, hrvRes) {
  const mutateData = {
    "uid": customer.uid,
    "haravan_id": hrvRes.data.customer.id
  }
  return db.mutate({
    set: mutateData
  })
}

function post(hrvData) {
  return Api.post('/customers.json', hrvData)
}

function put(hrvData) {
  return Api.put(`/customers/${hrvData.customer.id}.json`, hrvData)
}

function del(id) {
  return Api.delete(`/customers/${id}.json`)
}

module.exports = startSync