"use strict"

const product = require('./product')
const productType = require('./product_type')
const order = require('./order')
const customer = require('./customer')

/*
const types = { product, productType, order, customer }

function eventHandler(uid, type) {
  if(typeof types[type] === 'function') {
    types[type](uid).then(uid => {
      console.log("sync success haravan", type, uid)
    }).catch(err => {
      console.log("sync failed haravan", , type, uid, err)
    })
  }
}

module.exports = {
  hrvTypes: types,
  eventHandler
}
*/

module.exports = { product, productType, order, customer }