const db = require("#/services/tvcdb")
const Api = require('./api')
// const {calcPay, parseUrlImg} = require('./general')

/**
  * Create, Update a order, in haravan
  * Just One on time
  * This function get order by orderUid, then if:
  * haravan_id is null it will call commit to create new record in haravan service and save haravanId into local database
  * haravan_id is {int} it will call commit to update this order in haravan service
  * @param orderUid
  * @Crime => CA
  */
async function startSync(uid) {
  const { result: [data] } = await db.query(`{
    result(func: uid(${uid})) @filter(type(PrimaryOrder)){
      uid
      expand(_all_) {
        uid
        expand(_all_) {
          uid
          expand(_all_) {
            uid
            expand(_all_)
          }
        }
      }
    }
  }`)

  if(!data) {
    throw { statusCode: 404, message: `Order ${uid} not exist` }
  }

  if(data.haravan_id) {
    if (data.order_status === 2) { // Đã đang lấy hàng
      await createFulfillment(data)
    }
    if (data.order_status === 4) { // Đã giao
      await completeFulfillment(data)
    }
    if (data.order_status === 6) { //Đã huỷ
      await cancelFulfillment(data)
    }
    if (data.pay_status === 1) { // Payment success, mark done transaction
      await successOrderTransaction(data)
    }
  } else {
    const hrvRes = await post(data)
    await rewriteLocalData(data, hrvRes)
  }
  return uid
}

/**
  * This function used to update haravanId into the order pass as first parameter
  * @param {*} order
  * @param {*} hrvRes
  * @Crime => CA
  */
function rewriteLocalData(order, hrvRes) {
  const mutateData = {
    "uid": order.uid,
    "haravan_id": hrvRes.data.order.id
  }
  return db.mutate({
    set: mutateData
  })
}

function post(order) {
  const { sub_orders } = order
  const orderItems = Array.prototype.concat(...sub_orders.map(so => so['order_items']))
  const line_items = orderItems.map(oi => ({
    "variant_id": oi['order.product'].haravan_variant_id,
    "quantity": oi.quantity
  }))
  const hrvData = {
    "order": {
      "line_items": line_items,
      "note": order.uid,
    }
  }

  return Api.post('/orders.json', hrvData)
}

function successOrderTransaction({ haravan_id, total_pay }) {
  return Api.post(`/orders/${haravan_id}/transactions.json`, {
    "transaction": {
      "kind": "capture",
      "amount": total_pay
    }
  })
}

function createFulfillment(haravan_id) {
  Api.get(`/orders/${haravan_id}.json?fields=line_items`).then(() =>
    // let line_item_ids = order.line_items.map(item => {
    //   return {id: item['id'], quantity: item.quantity}
    // })
    Api.post(`/orders/${haravan_id}/fulfillments.json`, {
      "fulfillment": {
        "cod_amount": 0
      }
    })
  )
}

/**
  * readytopick :          Chờ lấy hàng
  * picking :              Đang đi lấy
  * delivering :           Đang giao hàng
  * delivered :            Đã giao hàng
  * cancel :               Hủy giao hàng
  * return :               Chuyển hoàn
  * pending :              Chờ xử lý
  * notmeetcustomer:       Không gặp khách
  * waitingforreturn:      Chờ chuyển hoàn
  * @param {*} order
  * @param {*} callback
  */

function updateFulfilmentStatus(order_id) {
  return Api.get(`/orders/${order_id}.json?fields=fulfillments`).then(({ data: { order } }) => {
    const fulfillment_id = order.fulfillments[0].id
    return Api.put(`/orders/${order_id}/fulfillments/${fulfillment_id}.json`, {
      "fulfillment": {
        "carrier_status_code": "cancel"
      }
    })
  })
}

function completeFulfillment(order) {
  return updateFulfilmentStatus(order.haravan_id, "delivered")
}

function cancelFulfillment(order) {
  return updateFulfilmentStatus(order.haravan_id, "cancel")
}

module.exports = startSync