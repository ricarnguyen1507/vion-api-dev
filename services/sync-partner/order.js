const db = require("#/services/tvcdb")

async function updatePartnerToSubOrder() {
  const { subs, partners } = await db.query(`{
    subs(func: type(Order)) {
      uid
      partner_id
      order.partner {
        uid
        id
      }
    }
    partners(func: type(Partner)) {
      uid
      id
    }
  }`).then(res => ({ subs: res.subs, partners: res.partners }))

  const mutateData = []
  if (subs && subs.length > 0) {
    for(let i = 0; i < subs.length; i++) {
      let findP = {}
      partners.forEach(p => {
        if (p.id === subs[i].partner_id) {
          return findP = p
        }
      })

      if (findP) {
        mutateData.push({
          del: {
            uid: subs[i].uid,
            'order.partner': { uid: subs[i]['order.partner'] && subs[i]['order.partner'].uid }
          }
        })
        mutateData.push({
          set: {
            uid: subs[i].uid,
            'order.partner': { uid: findP.uid }
          }
        })
      }
    }
  }

  return await db.mutates(mutateData)
}

module.exports = updatePartnerToSubOrder