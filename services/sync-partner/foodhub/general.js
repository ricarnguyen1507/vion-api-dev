"use strict"
const { image_url } = config

// function calcPay({sell_price = 0, discount = 0}) {
//     return discount === 0 ? sell_price : (sell_price * (1 - discount/100))
//   }
function calcPay({ sell_price = 0, discount = 0 }, quantity = 1) {
  return quantity * (discount === 0 ? sell_price : (sell_price * (1 - discount / 100)))
}

function parseUrlImg(imgSrc) {
  return `${image_url}/${imgSrc}`
}

module.exports = {
  calcPay,
  parseUrlImg
}