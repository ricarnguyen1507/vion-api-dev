// const supplier_id = "100026"
const { calcPay } = require("./general")
const FormData = require("form-data")
const { baseURL, appKey } = config.sync_partner.foodhub
const axios = require("axios").create({ baseURL })
/**
 * Create, Update a order, in haravan
 * Just One on time
 * This function get order by orderUid, then if:
 * haravan_id is null it will call commit to create new record in haravan service and save haravanId into local database
 * haravan_id is {int} it will call commit to update this order in haravan service
 * @param primary_order
 * @param foodhub_order
 * @Crime => CA
 */
function startSync(primary_order, foodhub_order) {
  if (foodhub_order && (foodhub_order.order_status === 5 || foodhub_order.order_status === 6)) {
    return post(primary_order, foodhub_order, 0)
    //Yeu cau huy don
  } else if (foodhub_order && foodhub_order.order_status === 1) {
    //tao don hang
    return post(primary_order, foodhub_order)
  }
  throw new Error("FoodHub: Not match status to sync order")
}

function post(primary_order, foodhub_order, status_post = 1) {
  //default 1 = post & put, 0 = cancel
  const orderItem = []
  let totalPay = 0
  let total = 0
  if (foodhub_order.order_items && foodhub_order.order_items.length > 0) {
    foodhub_order.order_items.map((oi) => {
      totalPay += calcPay(oi)
      total += oi.sell_price
      orderItem.push({
        code: (oi["order.product"] && oi["order.product"].sku_id) || "",
        quantity: oi.quantity,
        price: calcPay(oi),
        priceBeforeDiscount: oi.sell_price,
        discount: oi.discount
      })
    })
  }

  const order = {
    code: foodhub_order.order_id,
    status: status_post,
    storeCode: "FH1",
    customerName: primary_order.customer_name,
    phone: primary_order.phone_number,
    address: primary_order.address_des + ((primary_order.district && primary_order.district.name) || "") + ((primary_order.province && primary_order.province.name) || ""),
    note: foodhub_order.notes || "",
    total: total,
    // shipfee:"20000",
    payment: primary_order.pay_gateway == "cod" ? 1 : 3,
    totalPay: totalPay,
    orderItem: orderItem
  }

  const formData = new FormData()
  formData.append("app_key", appKey)
  formData.append("order", JSON.stringify(order))

  return axios.post("/createOrder", formData, { headers: formData.getHeaders() })
}

module.exports = startSync
