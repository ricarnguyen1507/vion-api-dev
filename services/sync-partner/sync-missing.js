const db = require("#/services/tvcdb")
const FPTShop = require('./fpt_shop')

async function syncMissingHandle (brand_shop_id, type) {
  log.info("Sync Missing Handle is called")
  if (type === "order") {
    const { result } = await db.query(`{
      var(func: type(OrderItem)) @filter(eq(brand_shop_id, ${brand_shop_id})){
        sub_o as ~order_items @filter(NOT eq(had_sent_partner, true)) {
          pri_o as ~sub_orders
        }
      }

      result(func: uid(pri_o)) {
        uid
        order_id
        customer_name
        phone_number
        address_des
        pay_gateway
        order_status
        total_pay
        pay_status
        voucher_code
        voucher_value
        created_at
        sub_orders @filter(uid(sub_o)) {
          uid
          order_id
          partner_id
          notes
          had_sent_partner
          order_status
          pay_status
          order_items @normalize {
            uid
            brand_shop_id: brand_shop_id
            quantity: quantity
            product_name: product_name
            sell_price: sell_price
            discount: discount
            order.product {
              sku_id: sku_id
              partner_sku: partner_sku
              partner_product_id: partner_product_id
            }
          }
        }
        district {
          uid
          name
        }
        province {
          uid
          name
        }
      }
    }`)

    /**
     * fowarding những đơn bị miss cho FPTSHOP
     */
    if (result && result.length) {
      result.forEach(data => {
        if (data && data.sub_orders && Array.isArray(data.sub_orders)) {
          for(const sub of data.sub_orders) {
            /**
             * Đoạn này để check trong order_items miễn có sản phẩm nào có brand_shop_id là FPTSHOP thì gửi sub_order đó qua cho FPTSHOP
             */
            const brand_shop_ids = sub.order_items?.map(oi => oi.brand_shop_id)
            if (brand_shop_ids.includes(FPTShop.id)) {
              /** send sub_order cho FPTSHOP */
              FPTShop[type](data, sub)
            }
          }
        }
      })
    }
  }
}

module.exports = syncMissingHandle