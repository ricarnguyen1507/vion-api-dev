// const supplier_id = "100026"
const { calcPay } = require("./general")
const db = require("#/services/tvcdb")

const { baseURL, appKey } = config.sync_partner.fptshop

const axios = require("axios").create({ baseURL })

/**
 * Create, Update a order, in haravan
 * Just One on time
 * This function get order by orderUid, then if:
 * haravan_id is null it will call commit to create new record in haravan service and save haravanId into local database
 * haravan_id is {int} it will call commit to update this order in haravan service
 * @param primary_order
 * @param fptShop_order
 * @Crime => CA
 */
function startSync (primary_order, fptShop_order) {
  if (fptShop_order && fptShop_order.had_sent_partner !== true) {
    //tao don hang
    return post(primary_order, fptShop_order)
  } else {
    log.info("FPTShop: Đơn hàng đã được gửi cho FPT Shop rồi")
  }
}

function post (primary_order, fptShop_order) {
  //default 1 = post & put, 0 = cancel
  let totalPay = 0
  const FRTOrderDetails = []
  let fptShopPartnerId = ""
  if (fptShop_order.order_items && fptShop_order.order_items.length > 0) {
    fptShop_order.order_items.map((oi) => {
      totalPay += calcPay(oi)
      if (oi.brand_shop_id) {
        fptShopPartnerId = oi.brand_shop_id
      }

      FRTOrderDetails.push({
        product_id: oi.partner_product_id || "",
        partner_sku: oi.partner_sku || "",
        sku_id: "",
        product_name: oi.product_name || "",
        price: oi.sell_price || "",
        quantity: oi.quantity || ""
      })
    })

    const payGetway = {
      'momo': "Momo",
      'quickpay': "Quickpay",
      'vnpay': "VNPay",
      'moca': "Moca",
      'cod': "COD"
    }

    const fptShopSubOrder = {
      // key_omnishop: appKey,
      sub_order_id: fptShop_order.order_id || "",
      pri_order_id: primary_order.order_id || "",
      partner_id: fptShopPartnerId,
      recipient_name: primary_order.customer_name || "",
      recipient_phone: primary_order.phone_number || "",
      created_phone_number: primary_order.created_phone_number || '',
      recipient_address: primary_order.address_des || "",
      recipient_district: primary_order.district?.name || "",
      recipient_city: primary_order.province?.name || "",
      sub_order_total_price: totalPay,
      pri_order_total_price: primary_order.total_pay,
      is_voucher_applied: !!primary_order.voucher_value,
      voucher_code: primary_order.voucher_value ? primary_order.voucher_code : "",
      voucher_discounted: primary_order.voucher_value ?? 0, // bug sync here
      payment_status_id: primary_order.pay_status,
      payment_status: primary_order.pay_status === 1 ? "Success" : primary_order.pay_status === 2 ? "Failed" : "Pending",
      payment_type_id: primary_order.pay_gateway,
      payment_type: payGetway[primary_order.pay_gateway],
      created_date_timestamp: primary_order.created_at,
      updated_date_timestamp: primary_order.created_at,
      order_details: FRTOrderDetails,
    }

    // console.log("--- Sync fptshop:", JSON.stringify(fptShopSubOrder))

    axios.post("/api-tvshopping-createorder/TVShoppingCreateOrder/InsertOrderTVShopping", fptShopSubOrder).then(res => {
      // const {status, statusText, headers, config} = res
      const { statusCode, message } = res.data
      log.info("Sync order To FRT response: ", { statusCode, message, order_id: primary_order.order_id })
      // log.info("Sync order To FRT config: ", {status, statusText, headers, configUrl: config.url, configData: config.data, configMethod: config.method})

      if (statusCode === 200 && fptShop_order.uid) {
        db.mutate({ set: {
          uid: fptShop_order.uid,
          had_sent_partner: true,
          order_status: 2
        } }).catch(log.error) // bug crash here
      } else {
        log.error("Sync failed: ", { order_id: primary_order.order_id, sub_order_id: fptShop_order.order_id })
      }
    }).catch(err => { // bug crash here
      // nhắc catch error cho promise hơn 1k lần ko lọt lỗ  tai, thứ gì đâu
      log.error("fptshop: sync order failed", err.message, err.response?.data)
    })
  }
}

module.exports = startSync