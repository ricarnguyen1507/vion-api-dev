const db = require("#/services/tvcdb")

// const haravan = require('./haravan')
const Foodhub = require('./foodhub')
const FPTShop = require('./fpt_shop')
// const Ahamove = () => {}//require('./ahamove')
// const partners = [
//   ["haravan", require('./haravan')],
//   ["foodhub", require('./foodhub')]
// ]

async function syncHandle (uid, type) {
  if (type === "order") {
    const { result: [data] } = await db.query(`{
      result(func: uid(${uid})) @filter(type(PrimaryOrder)){
        uid
        order_id
        customer_name
        phone_number
        address_des
        pay_gateway
        order_status
        total_pay
        pay_status
        voucher_code
        voucher_value
        created_at
        sub_orders {
          uid
          order_id
          partner_id
          notes
          had_sent_partner
          order_status
          pay_status
          order_items @normalize {
            uid
            brand_shop_id: brand_shop_id
            quantity: quantity
            product_name: product_name
            sell_price: sell_price
            discount: discount
            order.product {
              sku_id: sku_id
              partner_sku: partner_sku
              partner_product_id: partner_product_id
            }
          }
        }
        district {
          uid
          name
        }
        province {
          uid
          name
        }
      }
    }`)

    if (data && data.order_status >= 2 && data.sub_orders && Array.isArray(data.sub_orders)) {
      for(const sub of data.sub_orders) {
        let shipping_method = 0
        if (sub['order.partner']) {
          shipping_method = sub['order.partner'].shipping_method
        }
        if (shipping_method === 0 && !sub.had_sent_shipping) { //Forwarding
          forwardingOrder(data, sub)
        }
        // if (shipping_method === 1 && !sub.had_sent_shipping) { //Stocking
        // }
        // if (shipping_method === 2 && !sub.had_sent_shipping) { //Stock At Sup
        // stockAtSupOrder(data, sub)
        // }
      }
    }

    /**
     * fowarding đơn cho FPTSHOP
     */
    if (data && data.sub_orders && Array.isArray(data.sub_orders)) {
      for(const sub of data.sub_orders) {
        /**
         * Đoạn này để check trong order_items miễn có sản phẩm nào có brand_shop_id là FPTSHOP thì gửi sub_order đó qua cho FPTSHOP
         */
        const brand_shop_ids = sub.order_items?.map(oi => oi.brand_shop_id)
        if (brand_shop_ids.includes(FPTShop.id)) {
          /** send sub_order cho FPTSHOP */
          FPTShop[type](data, sub)
        }
      }
    }
  }
  return "Sync partner success"
}

async function forwardingOrder (primaryOrder, sub_order, type = 'order') {
  if (sub_order.partner_id === Foodhub.id && typeof Foodhub[type] === 'function') {
    try {
      await Foodhub[type](primaryOrder, sub_order)
      log.info(`sync Foodhub success type(order), order_id(${sub_order['order_id']})`)
    } catch(err) {
      log.warn(`sync Foodhub failed type(order), order_id(${sub_order['order_id']})`)
    }
  }
}

/* async function stockAtSupOrder(primaryOrder, sub_order, type = 'order') {
  try {
    const res = await Ahamove[type](primaryOrder, sub_order)
    console.log(res.data)
  } catch(err) {
    log.error(err)
  }
} */

module.exports = function (uid, type) {
  syncHandle(uid, type)
    .then(message => {
      log.info(message)
    })
    .catch(err => {
      log.error("sync partner", err)
    })
}