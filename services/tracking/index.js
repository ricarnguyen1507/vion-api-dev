"use strict"

const api = require('axios').create(config.tracking)

async function sendTracking ({ pages, dataOrigin = null, dataEdit = null, actionType, userData }) {
  let params = []

  if(pages === 'product') {
    params = [{
      track_name: "product",
      action: actionType.toString().toUpperCase(),
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      uid: dataEdit?.uid ?? null,
      changes: { dataOrigin, dataEdit }
    }]
  }

  if(pages === 'order') {
    params = [{
      track_name: "primary_order",
      action: actionType.toString().toUpperCase(),
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      uid: dataEdit?.uid ?? null,
      changes: { dataOrigin, dataEdit }
    }]
  }

  if(pages === 'order_sub') {
    params = [{
      track_name: "sub_order",
      action: actionType.toString().toUpperCase(),
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      uid: dataEdit?.uid ?? null,
      changes: { dataOrigin, dataEdit }
    }]
  }

  if(pages === 'widget') {
    params = [{
      track_name: "widget_fptplay",
      action: actionType.toString().toUpperCase(),
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      uid: dataEdit?.uid ?? null,
      changes: { dataOrigin, dataEdit }
    }]
  }

  await api.post(`/log/cms`, params)
  log.debug('Gui log thanh cong !', params)
}

module.exports = trackData => {
  sendTracking(trackData).catch(err => { log.error(err.response?.data || err.message) })
}