const { getService } = require("#/modules/dgraph-client")
const { alpha_grpc } = config

const tvcdb = getService('tvcdb', alpha_grpc)

const IsUid = /^0[xX][0-9A-F]+$/i
const text_esc = /["\\]/g
const regexp_esc = /[-[\]{}()*+?.,/\\^$|#\s]/g
const number_pattern = /^\d+$/

function addType (type, data) {
  if(data) {
    if(Array.isArray(data)) {
      data.forEach(o => {
        o['dgraph.type'] = type
      })
    } else if(typeof data === 'object') {
      data['dgraph.type'] = type
    }
  }
  return data
}

function isNumber (v) {
  if(typeof v === 'number') {
    return true
  }
  if(typeof v === 'string') {
    return number_pattern.test(v)
  }
  return false
}

function escapeText (text) {
  return text.replace(text_esc, '\\$&');
}

function escapeRegExp (text) {
  return text.replace(regexp_esc, '\\$&');
}

function createFilters (f) {
  if(f.operator === 'regexp') {
    return `regexp(${f.field}, /${escapeRegExp(f.value)}/i)`
  }
  if(isNumber(f.value) || typeof f.value === 'boolean') {
    return `${f.operator}(${f.field}, ${f.value})`
  }
  return `${f.operator}(${f.field}, "${escapeText(f.value)}")`
}

function parseFilters (filters) {
  const newFilter = filters.filter(e => {
    if(typeof e.value === 'string' || Array.isArray(e.value)) {
      return e.value.length > 0
    }
    return e.value != null
  })
  return {
    primaryFilters: newFilter.filter(f => f['order'] !== '').map(({ order, field }) => `order${order}: ${field}`),
    filters: newFilter.length ? newFilter.map(f => {
      if(Array.isArray(f.value)) {
        const arrStr = []
        let allStr = ''
        f.value.map(i => arrStr.push(createFilters({ ...f, value: i })))
        allStr = arrStr.join(' OR ')
        return arrStr.length > 1 ? `(${allStr})` : allStr
      }
      else{
        return createFilters(f)
      }
    }) : []
  }
}

/**
 * Chuyển đổi cục dữ liệu query có facets ( Thống nhất convert về dữ liệu bản stag )
 * Support cho dữ liệu query facets cũ có fields chứa |
 */
function parseFacetArray (arrData = []) {
  if(arrData?.length) {
    for(const data of arrData) {
      if(Object.keys(data).find((v) => v.includes('|'))) {
        parseFacetObject(data)
      }
    }
    return arrData
  }
  return []
}
function parseFacetObject (data = {}) {
  if(Object.keys(data)?.length) {
    const checkPredFacet = Object.keys(data).filter((v) => v.includes('|')) || []
    if(checkPredFacet.length) {
      checkPredFacet.map(e => {
        const arrPred = e.split('|')
        const data_field_facet = data?.[e] || {}
        if(arrPred.length === 2 && Array.isArray(data?.[arrPred[0]]) && data?.[arrPred[0]]?.length && Object.keys(data?.[e]).length) {
          data?.[arrPred[0]]?.map((obj, i) => {
            obj[arrPred[1]] = data_field_facet?.[i] || i
            obj[e] = data_field_facet?.[i] || i
          })
          delete data[e]
        }
      })
      return data
    }
    return data
  }
  return data
}


module.exports = {
  ...tvcdb,
  getUids (result, initData = { statusCode: 200 }) {
    const uid_arr = result.getUidsMap().arr_
    if(uid_arr.length) {
      const uids = {}
      uid_arr.forEach(([k, v]) => {
        uids[k] = v
      })
      return {
        ...initData,
        ...(uid_arr.length && { uids })
      }
    }
    return initData
  },
  isUid (txt) {
    return IsUid.test(txt)
  },
  addType,
  addTypeSet (type, data) {
    if(data.set) {
      addType(type, data.set)
    }
    return data
  },
  escapeText,
  escapeRegExp,
  parseFilters,
  parseFacetArray
}